package net.khwan.commons.file;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;

/**
 * Created by kkh on 2017-03-14.
 */
public final class FileIOCommon {
    private FileIOCommon() {
    }

    // private final Logger logger = LoggerFactory.getLogger(this.getClass());
    public static String getRootPath() {
        return System.getProperty("user.dir");
    }

    // File 존재 여부
    public static boolean isFileExists(String path) {
        File file = new File(path);
        return file.exists();
    }

    // Directory List
    public static List<File> getFileList(String path) {
        File file = new File(path);
        File[] files = file.listFiles();

        return (files != null) ? Arrays.asList(files) : null;
    }

    // Directory 생성
    public static void makeDir(String path) {
        File file = new File(path);
        if (!file.exists()) file.mkdirs();
    }

    // Directory 삭제
    public static void deleteDir(String path) {
        File file = new File(path);
        File[] childFileList = file.listFiles();
        for (File childFile : childFileList) {
            if (childFile.isDirectory()) {
                deleteDir(childFile.getAbsolutePath());
            } else {
                childFile.delete();
            }
        }
        file.delete();
    }

    public static File makeTempDir(String directoryName) {
        directoryName = directoryName + "_";
        File tmpdir = new File(System.getProperty("java.io.tmpdir"));
        File file;
        for (int i = 0; i < 1000; i++) {
            file = new File(tmpdir, directoryName + System.nanoTime());
            if (file.mkdir()) {
                return file;
            }
        }
        return null;
    }

    // Directory 검색
    public static String findDirectory(String path, String directoryName) {
        if (directoryName == null) return null;
        else if (directoryName.trim().length() == 0) return null;

        String findDirName = null;
        File file = new File(path);
        File[] fileArray = file.listFiles();
        if (fileArray == null) return null;
        for (File _file : fileArray) {
            if (_file.isDirectory()) {
                if (directoryName.equals(_file.getName())) {
                    findDirName = _file.getAbsolutePath();
                    break;
                }
            }
        }
        return findDirName;
    }

    // 파일 검색
    public static String findFile(String path, String fileName) {
        if (fileName == null) return null;
        else if (fileName.trim().length() == 0) return null;

        String findFileName = null;
        File file = new File(path);
        File[] fileArray = file.listFiles();
        if (fileArray == null) return null;
        for (File _file : fileArray) {
            if (_file.isFile()) {
                if (fileName.equals(_file.getName())) {
                    findFileName = _file.getAbsolutePath();
                    break;
                }
            }
        }
        return findFileName;
    }

    public static String stringFromFileSize(long fileSize) {
        return stringFromFileSize(fileSize, true, 1);
    }

    public static String stringFromFileSize(long fileSize, boolean si) {
        return stringFromFileSize(fileSize, si, 1);
    }

    public static String stringFromFileSize(long fileSize, boolean si, int decimalPlaces) {
        int unit = si ? 1000 : 1024;
        if (fileSize < unit) return fileSize + " B";
        int exp = (int) (Math.log(fileSize) / Math.log(unit));
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
        StringBuilder sb = new StringBuilder();
        if (decimalPlaces < 0) {
            decimalPlaces = 0;
        }
        sb.append(BigDecimal.valueOf(((double) fileSize / Math.pow(unit, exp))).setScale(decimalPlaces, RoundingMode.HALF_UP).toString());
        sb.append(" ");
        sb.append(pre);
        sb.append("B");
//        return String.format("%.1f %sB", fileSize / Math.pow(unit, exp), pre);
        return sb.toString();
    }

    public static String searchDirPath(String path, String dirName) {
        List<File> fileList = getFileList(path);
        File searchFile = null;
        if (fileList != null) {
            for (File file : fileList) {
                if (file.isDirectory()) {
                    String name = file.getName();
                    if (name.toLowerCase().contains(dirName)) {
                        searchFile = file;
                        break;
                    }
                }
            }
        }
        return (searchFile != null) ? searchFile.getAbsolutePath() : null;
    }

    public interface Listener {
        void progress(int length);
    }

    public static void copy(String fromFilePath, String toFilePath) throws IOException {
        copy(new FileInputStream(new File(fromFilePath)), new FileOutputStream(new File(toFilePath)));
    }

    public static void copy(InputStream in, OutputStream out) throws IOException {
        copy(in, out, 8192, null);
    }

    public static void copy(InputStream in, OutputStream out, Listener listener) throws IOException {
        copy(in, out, 8192, listener);
    }

    public static void copy(InputStream in, OutputStream out, int bufferSize, Listener listener) throws IOException {
        byte[] buffer = new byte[bufferSize];
        int length;

        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(in);
            bos = new BufferedOutputStream(out);

            while ((length = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, length);
                if (listener != null) {
                    listener.progress(length);
                }
            }
            bos.flush();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                    bos = null;
                } catch (IOException e) {
                }
            }
            if (bis != null) {
                try {
                    bis.close();
                    bis = null;
                } catch (IOException e) {
                }
            }
        }
    }

    public static String readAllString(String path) throws IOException {
        return readAllString(path, "UTF-8");
    }

    public static String readAllString(String path, String charsetName) throws IOException {
        return readAllString(new FileInputStream(new File(path)), charsetName);
    }

    public static String readAllString(InputStream is) throws IOException {
        return readAllString(is, "UTF-8");
    }

    public static String readAllString(InputStream in, String charsetName) throws IOException {
        byte[] buffer = new byte[8192];
        BufferedInputStream bis = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            bis = new BufferedInputStream(in);
            int size;
            while ((size = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, size);
            }

        } finally {
            if (bis != null) {
                try {
                    bis.close();
                    bis = null;
                } catch (IOException e) {
                }
            }

            try {
                buffer = bos.toByteArray();
                bos.close();
                bos = null;
            } catch (IOException e) {
            }
        }
        return (new String(buffer, charsetName));
    }

    public static void write(String path, String content) throws IOException {
        write(path, content, "UTF-8");
    }

    public static void write(String path, String content, String charsetName) throws IOException {
        OutputStream out = new FileOutputStream(new File(path), false);
        write(out, content, charsetName);
    }

    public static void write(OutputStream out, String content, String charsetName) throws IOException {
        write(out, content.getBytes(charsetName));
    }

    public static void writeAppend(String path, String content) throws IOException {
        writeAppend(path, content, "UTF-8");
    }

    public static void writeAppend(String path, String content, String charsetName) throws IOException {
        OutputStream out = new FileOutputStream(new File(path), true);
        write(out, content, charsetName);
    }

    public static void write(OutputStream out, byte[] buffer) throws IOException {
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(out);
            bos.write(buffer);
            bos.flush();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                    bos = null;
                } catch (IOException e) {
                }
            }
        }
    }
}
