package net.khwan.commons.file;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.FileVisitResult.SKIP_SUBTREE;

/**
 * Created by kkh on 2017-03-14.
 */
public final class FileNIOCommon {
    private FileNIOCommon() {
    }

    private static final CopyOption[] options = new CopyOption[]{
            StandardCopyOption.REPLACE_EXISTING,    // 복사대상파일이 존재하면 덮어쓰기를 함.
            StandardCopyOption.COPY_ATTRIBUTES,    // 원본파일의 속성까지 모두 복사.
    };

    // NIO 파일 이동
    public static void move(String fromFilePath, String toFilePath) throws IOException {
        Path fromPath = Paths.get(fromFilePath);
        Path toPath = Paths.get(toFilePath);

        if (Files.exists(fromPath, LinkOption.NOFOLLOW_LINKS)) {
            Files.move(fromPath, toPath, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    // NIO 파일 복사
    public static void copy(String fromFilePath, String toFilePath) throws IOException {
        Path fromPath = Paths.get(fromFilePath);
        Path toPath = Paths.get(toFilePath);

        if (Files.exists(fromPath, LinkOption.NOFOLLOW_LINKS)) {
            Files.copy(fromPath, toPath, options);
        }
    }

    // NIO 파일 복사
    public static void copy(Path fromPath, Path toPath) throws IOException {
        if (Files.exists(fromPath, LinkOption.NOFOLLOW_LINKS)) {
            Files.copy(fromPath, toPath, options);
        }
    }

    // NIO 디렉토리 복사
    public static void copyDir(String fromFilePath, String toFilePath) throws IOException {
        final Path fromPath = Paths.get(fromFilePath);
        final Path toPath = Paths.get(toFilePath);
        if (Files.exists(fromPath, LinkOption.NOFOLLOW_LINKS)) {
            Files.copy(fromPath, toPath, options);
            Files.walkFileTree(fromPath, new FileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                    // before visiting entries in a directory we copy the directory
                    // (okay if directory already exists).
                    //CopyOption[] options = new CopyOption[] { StandardCopyOption.COPY_ATTRIBUTES };

                    Path newdir = toPath.resolve(fromPath.relativize(dir));
                    try {
                        Files.copy(dir, newdir, options);
                    } catch (FileAlreadyExistsException x) {
                        // ignore
                    } catch (IOException x) {
                        System.err.format("Unable to create: %s: %s%n", newdir, x);
                        return SKIP_SUBTREE;
                    }
                    return CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    try {
                        copy(file, toPath.resolve(fromPath.relativize(file)));
                    } catch (IOException e) {
                        System.err.format("Error %s", e);
                    }
                    return CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    if (exc instanceof FileSystemLoopException) {
                        System.err.println("cycle detected: " + file);
                    } else {
                        System.err.format("Unable to copy: %s: %s%n", file, exc);
                    }
                    return CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    // fix up modification time of directory when done
                    if (exc == null) {
                        Path newdir = toPath.resolve(fromPath.relativize(dir));
                        try {
                            FileTime time = Files.getLastModifiedTime(dir);
                            Files.setLastModifiedTime(newdir, time);
                        } catch (IOException x) {
                            System.err.format("Unable to copy all attributes to: %s: %s%n", newdir, x);
                        }
                    }
                    return CONTINUE;
                }
            });
        }
    }

    public static List<String> readAllLines(String rootPath, String fileName) throws IOException {
        // NIO Code
        String filePath = rootPath + "/" + fileName;
        return Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);
    }

    public static void write(String path, String data) throws IOException {
        write(path, null, "UTF-8", data, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    public static void write(String rootPath, String fileName, String data) throws IOException {
        write(rootPath, fileName, "UTF-8", data, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    public static void writeAppend(String rootPath, String fileName, String data) throws IOException {
        write(rootPath, fileName, "UTF-8", data, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
    }

    public static void writeAppend(String path, String data) throws IOException {
        write(path, null, "UTF-8", data, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
    }

    public static void write(String rootPath, String fileName, String charsetStr, String data, OpenOption... openOptions) throws IOException {
        Path location = Paths.get(rootPath + ((fileName != null) ? ("/" + fileName) : ""));
        FileChannel fileChannel = FileChannel.open(location, openOptions);
        Charset charset = Charset.forName(charsetStr);
        ByteBuffer byteBuffer = charset.encode(data);
        fileChannel.write(byteBuffer);
        fileChannel.close();
    }
}
