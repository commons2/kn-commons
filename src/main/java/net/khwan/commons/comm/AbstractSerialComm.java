package net.khwan.commons.comm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import purejavacomm.*;

import java.io.IOException;

public abstract class AbstractSerialComm extends AbstractComm {
    private final Logger logger;

    private SerialPort serialPort;

    public AbstractSerialComm(String className) {
        this(className, 2048);
    }

    public AbstractSerialComm(String className, int bufferMaxSize) {
        super(className, bufferMaxSize);
        logger = LoggerFactory.getLogger(className);
    }

    public boolean connect(String portName, int baudRate) {
        return connect(portName, baudRate, 3000);
    }

    public boolean connect(String portName, int baudRate, int connectTimeout) {
        CommState state = getState();
        if (state == CommState.CONNECTING || state == CommState.CONNECTED) {
            logger.info("Already connected.({})", state);
            return false;
        }

        setState(CommState.CONNECTING);

        try {
            logger.info("Try Tcp Serial Connection({}, {})", portName, baudRate);
            CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);

            CommPort commPort = portIdentifier.open(this.className, connectTimeout);

            if (commPort instanceof SerialPort) {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(baudRate  // 통신속도
                        , SerialPort.DATABITS_8 // 데이터 비트
                        , SerialPort.STOPBITS_1 // stop 비트
                        , SerialPort.PARITY_NONE // 패리티  SerialPort.PARITY_NONE
                );
//                logger.debug("Before enableReadTimeout: " + serialPort.isReceiveTimeoutEnabled());
//                if (receiveTimeoutMillis > 0) serialPort.enableReceiveTimeout(receiveTimeoutMillis);
//                logger.debug("Timeout           : {}", receiveTimeoutMillis);
//                logger.debug("EnableReadTimeout : {}", serialPort.isReceiveTimeoutEnabled());
                logger.debug("BaudRate          : {}", serialPort.getBaudRate());
                logger.debug("DataBIts          : {}", serialPort.getDataBits());
                logger.debug("StopBits          : {}", serialPort.getStopBits());
                logger.debug("Parity            : {}", serialPort.getParity());
                logger.debug("FlowControl       : {}", serialPort.getFlowControlMode());
                inputStream = serialPort.getInputStream();
                outputStream = serialPort.getOutputStream();

                this.serialPort = serialPort;

                setState(CommState.CONNECTED);
                logger.info("Serial({}) Connected.", portName);

                if (convertProtocol != null) convertProtocol.clearBuffer();
                startReceiveThread();
                connected();
                return true;
            } else {
                logger.error("Only serial ports are handled by this module.");
            }
        } catch (NoSuchPortException e) {
            logger.info("NoSuchPort({})", portName);
        } catch (UnsupportedCommOperationException e) {
            logger.info("UnsupportedCommOperationException({})", portName);
        } catch (PortInUseException e) {
            logger.info("PortInUse({})", portName);
        } catch (IOException e) {
            logger.error("Exception", e);
        }
        setState(CommState.DISCONNECTED);
        return false;
    }

    @Override
    public void disconnect() {
        if (serialPort != null) {
            try {
                serialPort.close();
            } catch (Exception e) {
                exceptionCaught(e);
            }
            serialPort = null;
        }

        super.disconnect();
    }

    @Override
    public boolean send(byte[] buffer) {
        return super.send(buffer);
    }
}
