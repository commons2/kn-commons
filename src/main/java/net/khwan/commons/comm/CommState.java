package net.khwan.commons.comm;

public enum CommState {
    DISCONNECTING, DISCONNECTED, CONNECTING, CONNECTED
}
