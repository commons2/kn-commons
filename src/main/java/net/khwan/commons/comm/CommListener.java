package net.khwan.commons.comm;

public interface CommListener {
    void onNewData(Object data);

    void connected();

    void disconnected();

    void exceptionCaught(Exception e);
}
