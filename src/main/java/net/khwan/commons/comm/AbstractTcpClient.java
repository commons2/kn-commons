package net.khwan.commons.comm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.*;
import java.security.cert.X509Certificate;

public abstract class AbstractTcpClient extends AbstractComm {
    private final Logger logger;

    private SSLSocketFactory sslSocketFactory = null;
    private Socket socket;

    public AbstractTcpClient(String className) {
        this(className, 2048, false);
    }

    public AbstractTcpClient(String className, boolean enableSSL) {
        this(className, 2048, enableSSL);
    }

    public AbstractTcpClient(String className, int bufferMaxSize) {
        this(className, bufferMaxSize, false);
    }

    public AbstractTcpClient(String className, int bufferMaxSize, boolean enableSSL) {
        super(className, bufferMaxSize);
        logger = LoggerFactory.getLogger(className);
        if (enableSSL) {
            createSslSessionFactory();
        }
    }

    public boolean connect(String ipAddress, int port) {
        return connect(ipAddress, port, 5000);
    }

    public boolean connect(String ipAddress, int port, int timeout) {
        CommState state = getState();
        if (state == CommState.CONNECTING || state == CommState.CONNECTED) {
            logger.info("Already connected.({})", state);
            return false;
        }

        setState(CommState.CONNECTING);
        try {
            logger.info("Try tcp socket connection, ip({}) port({})", ipAddress, port);
            InetAddress inetAddress = InetAddress.getByName(ipAddress);

            if (sslSocketFactory != null) {
                socket = sslSocketFactory.createSocket();
            } else {
                socket = new Socket();
            }
            socket.connect(new InetSocketAddress(inetAddress, port), timeout);
            socket.setSoLinger(true, 0);

            inputStream = new BufferedInputStream(socket.getInputStream());
            outputStream = new BufferedOutputStream(socket.getOutputStream());

            setState(CommState.CONNECTED);
            logger.info("Tcp Socket Connected. ip({}) port({})", ipAddress, port);

            if (convertProtocol != null) convertProtocol.clearBuffer();
            startReceiveThread();
            connected();
            return true;
        } catch (UnknownHostException e) {
            logger.warn("UnknownHostException ", e);
        } catch (NoRouteToHostException e) {
            logger.warn("No route to host (Host unreachable)");
        } catch (SocketTimeoutException e) {
            logger.warn("Connect timed out");
        } catch (ConnectException e) {
            logger.warn("Connection refused");
        } catch (Exception e) {
            logger.error("Exception ", e);
        }
        setState(CommState.DISCONNECTED);
        return false;
    }

    @Override
    public void disconnect() {
        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                exceptionCaught(e);
            }
            socket = null;
        }

        super.disconnect();
    }

    @Override
    public boolean send(byte[] buffer) {
        return super.send(buffer);
    }

    @SuppressWarnings("Duplicates")
    private void createSslSessionFactory() {
        if (sslSocketFactory == null) {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });
                HttpsURLConnection.setFollowRedirects(true);

                sslSocketFactory = sc.getSocketFactory();
                //System.out.println("new create ssl socket factory");
            } catch (Exception e) {
                logger.error("Exception", e);
            }
        }
    }
}
