package net.khwan.commons.comm;

import net.khwan.commons.protocol.converter.ConvertProtocol;
import net.khwan.commons.protocol.model.ReceiveResult;
import net.khwan.commons.utils.ConvertUtils;
import net.khwan.commons.utils.HexBin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class AbstractComm implements Runnable {
    private final Logger logger;

    private Thread thread = null;

    protected String className;

    protected ConvertProtocol convertProtocol;

    protected List<CommListener> listeners = new ArrayList<>();

    protected InputStream inputStream;
    protected OutputStream outputStream;

    public final int BUFFER_MAX_SIZE;

    // 통신 연결 상태
    private CommState state = CommState.DISCONNECTED;
    private final ReadWriteLock stateReadWriteLock = new ReentrantReadWriteLock();

    public CommState getState() {
        stateReadWriteLock.readLock().lock();
        try {
            return state;
        } finally {
            stateReadWriteLock.readLock().unlock();
        }
    }

    protected void setState(CommState state) {
        logger.debug("change state {} -> {}", this.state, state);
        stateReadWriteLock.writeLock().lock();
        this.state = state;
        stateReadWriteLock.writeLock().unlock();
    }

    public AbstractComm(String className, int bufferMaxSize) {
        this.className = className;
        this.logger = LoggerFactory.getLogger(className);
        this.BUFFER_MAX_SIZE = bufferMaxSize;
    }

    public synchronized boolean send(byte[] buffer) {
        if (outputStream != null) {
            try {
                if (convertProtocol != null) {
                    buffer = convertProtocol.sendPacket(buffer);
                }
                outputStream.write(buffer);

                if (outputStream instanceof BufferedOutputStream) {
                    outputStream.flush();
                }

                printBytes("send", buffer);
                return true;
            } catch (IOException e) {
                exceptionCaught(e);
                disconnect();
            }
        } else if (getState() != CommState.CONNECTED) {
            logger.warn("is not connected.");
        } else {
            logger.error("state : {}, outputStream is null", getState());
        }
        return false;
    }

    protected synchronized boolean startReceiveThread() {
        if (thread != null) {
            logger.warn("Could not start thread. The thread is already running.");
            return false;
        } else {
            thread = new Thread(this);
            thread.start();
            return true;
        }
    }

    public synchronized void disconnect() {
        CommState state = getState();
        if (state == CommState.DISCONNECTED || state == CommState.DISCONNECTING) {
            return;
        }
        setState(CommState.DISCONNECTING);

        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (Exception e) {
                exceptionCaught(e);
            }
            inputStream = null;
        }

        if (outputStream != null) {
            try {
                outputStream.close();
            } catch (Exception e) {
                exceptionCaught(e);
            }
            outputStream = null;
        }

        if (thread != null) {
            thread.interrupt();
            thread = null;
        }

        setState(CommState.DISCONNECTED);
        disconnected();
    }

    protected void broadcastNewData(Object data) {
        for (CommListener listener : listeners) {
            listener.onNewData(data);
        }
    }

    protected void broadcastConnected() {
        for (CommListener listener : listeners) {
            listener.connected();
        }
    }

    protected void broadcastDisconnected() {
        for (CommListener listener : listeners) {
            listener.disconnected();
        }
    }

    protected void broadcastExceptionCaught(Exception e) {
        for (CommListener listener : listeners) {
            listener.exceptionCaught(e);
        }
    }

    public void addListener(CommListener listener) {
        if (listener != null) {
            this.listeners.add(listener);
        }
    }

    public void removeListener(CommListener listener) {
        if (listener != null) {
            this.listeners.remove(listener);
        }
    }

    @Override
    public void run() {
        logger.debug("running packet receive thread.");

        byte[] buffer = new byte[BUFFER_MAX_SIZE];
        int lengthZeroCount = 0;
        int length = -1;

        final InputStream in = inputStream;
        final ConvertProtocol convertProtocol = this.convertProtocol;

        try {
            while ((length = in.read(buffer)) > -1) {
                if (length > 0) {
                    try {
                        lengthZeroCount = 0;
                        if (convertProtocol == null) {
                            try {
                                byte[] receiveBuffer = new byte[length];
                                ConvertUtils.byteArrayCopy(buffer, receiveBuffer, 0, length);
                                printBytes("receive", receiveBuffer);
                                onNewData(receiveBuffer);
                            } catch (Exception e) {
                                exceptionCaught(e);
                            }
                        } else {
                            Queue<ReceiveResult> resultQueue = convertProtocol.receivePacket(buffer, length);

                            while (!resultQueue.isEmpty()) {
                                ReceiveResult receiveResult = resultQueue.remove();
                                if (receiveResult.isResult()) {
                                    printBytes("receive", receiveResult.getReceiveBuffer());
                                    try {
                                        onNewData(receiveResult.getReceiveBuffer());
                                    } catch (Exception e) {
                                        exceptionCaught(e);
                                    }
                                } else {
                                    logger.warn("{}", receiveResult.toString());
                                    logger.warn("packet    : {}", HexBin.encode(receiveResult.getReceiveBuffer()));
                                }
                            }
                        }
                    } catch (Exception e) {
                        exceptionCaught(e);
                    }
                } else {
                    lengthZeroCount++;
                    if (lengthZeroCount >= 10000) {
                        disconnect();
                        exceptionCaught(new Exception("receive length zero count full(" + lengthZeroCount + ")"));
                    }
                }
            }
        } catch (Exception e) {
            exceptionCaught(e);
            disconnect();
        }

        disconnect();
        logger.debug("interrupt packet receive thread.");
    }

    protected void printBytes(String title, byte[] buffer) {
        if (logger.isDebugEnabled()) {
            logger.debug("{}({}): {}", title, buffer.length, HexBin.encode(buffer));
        }
    }

    protected abstract void onNewData(Object data);

    protected abstract void connected();

    protected abstract void disconnected();

    protected abstract void exceptionCaught(Exception e);
}
