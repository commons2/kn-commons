package net.khwan.commons.model;

import java.util.ArrayList;

/**
 * @author 김경환
 * @version 1.0
 * @since 2020-10-16
 */
public class LocationContains {
    private final ArrayList<Coordinate> pointList = new ArrayList<>();
    private int size;

    private double minLat = Double.MAX_VALUE;
    private double minLng = Double.MAX_VALUE;

    private double maxLat = Double.MIN_VALUE;
    private double maxLng = Double.MIN_VALUE;

    public LocationContains() {
    }

    public void addCoordinate(double latitude, double longitude) {
        pointList.add(new Coordinate(latitude, longitude));
        if (minLat > latitude) {
            minLat = latitude;
        }
        if (minLng > longitude) {
            minLng = longitude;
        }

        if (maxLat < latitude) {
            maxLat = latitude;
        }
        if (maxLng < longitude) {
            maxLng = longitude;
        }
        size = pointList.size();
    }

    public boolean contains(double latitude, double longitude) {
        if (size < 3) {
            return false;
        }

        // 위경도 값 범위 내 포함여부 확인
        if (!(minLat <= latitude && latitude <= maxLat && minLng <= longitude && longitude <= maxLng)) {
            return false;
        }

        int followIndex = size - 1;
        boolean isOddNodes = false;

        /**
         * 아래 알고리즘은 "Point in Polygon" 알고리즘이다.
         * 다만 좌우 양 방향을 체크하는 것이 아니라 왼쪽 방향만을 체크한다.
         * xPos = longitude
         * yPos = latitude
         */
        for (int frontIndex = 0; frontIndex < size; frontIndex++) {
            Coordinate frontPoint = pointList.get(frontIndex);
            Coordinate followPoint = pointList.get(followIndex);

            if (frontPoint.getLatitude() < latitude && followPoint.getLatitude() >= latitude || followPoint.getLatitude() < latitude && frontPoint.getLatitude() >= latitude) {
                /**
                 * "직선의 기울기 m을 갖는 yPos에 해당하는 x" < xPos 인지 체크
                 * 두 점을 지나는 직선의 방정식 참고.
                 *      y - y1 = M * (x - x1)
                 *      M = (y2 - y1) / (x2 - x1)
                 */
                if (frontPoint.getLongitude() + (latitude - frontPoint.getLatitude()) / (followPoint.getLatitude() - frontPoint.getLatitude())
                        * (followPoint.getLongitude() - frontPoint.getLongitude()) < longitude) {
                    isOddNodes = !isOddNodes;
                }
            }

            followIndex = frontIndex;
        }

        /**
         * "기울기 m을 갖는 yPosf에 해당하는 x" < xPos의 개수가 홀수이면
         * 다각형안에 포함된 점이다.
         */
        return isOddNodes;
    }

    public double getMinLat() {
        return minLat;
    }

    public double getMinLng() {
        return minLng;
    }

    public double getMaxLat() {
        return maxLat;
    }

    public double getMaxLng() {
        return maxLng;
    }

    @Override
    public String toString() {
        return "LocationContains{" +
                "pointList=" + pointList +
                ", size=" + size +
                ", minLat=" + minLat +
                ", minLng=" + minLng +
                ", maxLat=" + maxLat +
                ", maxLng=" + maxLng +
                '}';
    }
}
