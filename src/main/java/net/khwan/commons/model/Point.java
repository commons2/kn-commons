package net.khwan.commons.model;

import java.util.Objects;

/**
 * @author 김경환
 * @version 1.0
 * @since 2019-10-06
 */
public class Point {
    private double x;
    private double y;

    public Point() {
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public static PointBuilder builder() {
        return new PointBuilder();
    }

    public static final class PointBuilder {
        private Point point;

        private PointBuilder() {
            point = new Point();
        }

        public PointBuilder x(double x) {
            point.setX(x);
            return this;
        }

        public PointBuilder y(double y) {
            point.setY(y);
            return this;
        }

        public Point build() {
            return point;
        }
    }
}
