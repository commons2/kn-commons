package net.khwan.commons.model;

import java.util.Objects;

/**
 * @author 김경환
 * @version 1.0
 * @since 2020-10-16
 */
public class MinMaxCoordinate {
    private final Coordinate min;
    private final Coordinate max;

    public MinMaxCoordinate(Coordinate min, Coordinate max) {
        this.min = min;
        this.max = max;
    }

    public Coordinate getMin() {
        return min;
    }

    public Coordinate getMax() {
        return max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MinMaxCoordinate that = (MinMaxCoordinate) o;
        return Objects.equals(min, that.min) &&
                Objects.equals(max, that.max);
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }

    @Override
    public String toString() {
        return "MinMaxCoordinate{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }

    public static MinMaxCoordinateBuilder builder() {
        return new MinMaxCoordinateBuilder();
    }

    public static final class MinMaxCoordinateBuilder {
        private Coordinate min;
        private Coordinate max;

        private MinMaxCoordinateBuilder() {
        }

        public MinMaxCoordinateBuilder min(Coordinate min) {
            this.min = min;
            return this;
        }

        public MinMaxCoordinateBuilder max(Coordinate max) {
            this.max = max;
            return this;
        }

        public MinMaxCoordinate build() {
            return new MinMaxCoordinate(min, max);
        }
    }
}
