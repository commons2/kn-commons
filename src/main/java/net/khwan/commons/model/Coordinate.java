package net.khwan.commons.model;

import java.util.Objects;

/**
 * @author 김경환
 * @version 1.0
 * @since 2020-10-16
 */
public class Coordinate {
    private Double latitude;
    private Double longitude;

    public Coordinate() {
    }

    public Coordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Coordinate(String latitude, String longitude) throws NumberFormatException {
        this.latitude = Double.parseDouble(latitude);
        this.longitude = Double.parseDouble(longitude);
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public String toString() {
        return "Coordinate{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    public static CoordinateBuilder builder() {
        return new CoordinateBuilder();
    }


    public static final class CoordinateBuilder {
        private Coordinate coordinate;

        private CoordinateBuilder() {
            coordinate = new Coordinate();
        }

        public CoordinateBuilder latitude(Double latitude) {
            coordinate.setLatitude(latitude);
            return this;
        }

        public CoordinateBuilder longitude(Double longitude) {
            coordinate.setLongitude(longitude);
            return this;
        }

        public Coordinate build() {
            return coordinate;
        }
    }
}
