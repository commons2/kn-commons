package net.khwan.commons.thread;

import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class AbstractThread implements Runnable {
    protected final Object lock = new Object();
    private ExecutorService singleExecutorService;
    protected ExecutorService executorService;
    private final boolean useCachedExecutorService;
    private final boolean useFixedExecutorService;
    private final int fixedExecutorThreads;

    public AbstractThread() {
        this(false);
    }

    public AbstractThread(boolean useCachedExecutorService) {
        this.useCachedExecutorService = useCachedExecutorService;
        this.useFixedExecutorService = false;
        this.fixedExecutorThreads = 0;
    }

    public AbstractThread(boolean useFixedExecutorService, int nThreads) {
        this.useFixedExecutorService = useFixedExecutorService;
        this.fixedExecutorThreads = nThreads;
        this.useCachedExecutorService = false;
    }

    public void start() throws RuntimeException {
        synchronized (lock) {
            if (singleExecutorService == null) {
                singleExecutorService = Executors.newSingleThreadExecutor();
                singleExecutorService.execute(this);

                if (useCachedExecutorService) {
                    executorService = Executors.newCachedThreadPool();
                } else if (useFixedExecutorService) {
                    executorService = Executors.newFixedThreadPool(fixedExecutorThreads);
                }
            } else {
                throw new RuntimeException("Thread already started");
            }
        }
    }

    @PreDestroy
    public void stop() {
        synchronized (lock) {
            if (singleExecutorService != null) {
                singleExecutorService.shutdownNow();
                singleExecutorService = null;
            }

            if (executorService != null) {
                executorService.shutdownNow();
                executorService = null;
            }
        }
    }
}
