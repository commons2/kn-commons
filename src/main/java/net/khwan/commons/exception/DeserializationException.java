package net.khwan.commons.exception;

public class DeserializationException extends Exception {
    public DeserializationException(String message) {
        super(message);
    }

    public static void newThrow(final Class clazz, byte[] byteArray, int bytesLength) throws DeserializationException {
        newThrow(clazz, byteArray, 0, bytesLength);
    }

    public static void newThrow(final Class clazz, byte[] byteArray, int startPos, int bytesLength) throws DeserializationException {
        StringBuilder sb = new StringBuilder(160);
        String name = clazz.getSimpleName();
        sb.append(name)
                .append(".setBytes(byte byteArray[").append(byteArray.length - startPos).append("]). The byte length of the parameter is less than the current class byte length. [")
                .append(name)
                .append(".getBytesLength () = ").append(bytesLength).append("].");
        throw new DeserializationException(sb.toString());
    }
}
