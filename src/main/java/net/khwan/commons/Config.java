package net.khwan.commons;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import ch.qos.logback.core.util.StatusPrinter;
import net.khwan.commons.utils.ConvertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Config {
    private static Logger logger;

    // conf/application.properties To Map
    protected final static HashMap<String, String> map = new HashMap<>();

    public final static String appRunDir = System.getProperty("user.dir");

    public static String profile;
    public static String configLocation;

    public static int versionCode = 0;
    public static String versionName = "";

    public enum ArgumentOption {
        PROFILE("profile"),
        CONFIG_LOCATION("config.location"),
        UNKNOWN("UNKNOWN");

        final String key;

        ArgumentOption(String key) {
            this.key = key;
        }

        /**
         * static
         */
        public static final Map<String, ArgumentOption> keyValues = new HashMap<>(values().length);

        static {
            for (ArgumentOption type : values()) {
                keyValues.put(type.key, type);
            }
        }

        static public ArgumentOption get(String key) {
            if (keyValues.containsKey(key)) {
                return keyValues.get(key);
            }
            return UNKNOWN;
        }
    }

    public static InputStream getResourceAsStream(String name) {
        return Config.class.getClassLoader().getResourceAsStream(name);
    }

    public static void init() {
        init(false, false, null);
    }

    public static void init(String[] args) {
        init(true, true, args);
    }

    public static void init(boolean loadLoggingConfig, boolean loadProperties, String[] args) {
        System.setProperty("file.encoding", "UTF-8");

        HashMap<ArgumentOption, String> argMap = new HashMap<>();
        if (args != null) {
            for (String arg : args) {
                if (!isEmpty(arg)) {
                    String[] splitArray = arg.split("=");
                    if (splitArray.length == 2) {
                        ArgumentOption argumentOption = ArgumentOption.get(splitArray[0]);
                        if (argumentOption != ArgumentOption.UNKNOWN) {
                            argMap.put(argumentOption, splitArray[1]);
                        }
                    }
                }
            }
        }

        try {
            if (loadLoggingConfig) {
                logger = loadLoggingConfig(new ByteArrayInputStream(DEFAULT_LOGGING_CONFIG_XML.getBytes("UTF-8")));
            } else {
                logger = LoggerFactory.getLogger(Config.class);
            }

            if (loadProperties) {
                logger.debug("arg : {}", argMap);

                // 기본 Properties 정보 로드
                String propertiesName = "application.properties";
                onLoadApplicationProperties(propertiesName);

                // Resources Profile properties 정보 메모리 로드
                String profile = argMap.get(ArgumentOption.PROFILE);
                if (profile != null) {
                    propertiesName = "application-" + profile + ".properties";
                    onLoadApplicationProperties(propertiesName);
                } else {
                    profile = "default";
                }
                Config.profile = profile;
                logger.debug("active profile : {}", profile);

                // 외부 properties 정보 메모리 로드
                String configLocation = argMap.get(ArgumentOption.CONFIG_LOCATION);
                if (configLocation != null) {
                    try {
                        File file = new File(configLocation);
                        if (file.exists()) {
                            loadApplicationProperties(new FileInputStream(configLocation));
                        } else {
                            logger.warn("not found config file({})", configLocation);
                        }
                    } catch (FileNotFoundException e) {
                        logger.error("Exception", e);
                    }
                    //logger.debug("map : {}", map);
                }
                Config.configLocation = configLocation;

                String loggingConfigPath = map.get("logging.config");
                if (loggingConfigPath != null) {
                    logger.debug("loggingConfigPath : {}", loggingConfigPath);
                    try {
                        String classPathStr = "classpath:";
                        int pos = loggingConfigPath.indexOf(classPathStr);
                        if (pos > -1) {
                            loggingConfigPath = loggingConfigPath.substring(pos + classPathStr.length(), loggingConfigPath.length());
                            logger = loadLoggingConfig(getResourceAsStream(loggingConfigPath));
                        } else {
                            logger = loadLoggingConfig(new FileInputStream(new File(loggingConfigPath)));
                        }
                    } catch (FileNotFoundException e) {
                        logger.error("Exception", e);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception", e);
        }

        try {
            Map<String, String> versionMap = new HashMap<>();
            if (loadProperties(getResourceAsStream("version.properties"), versionMap)) {
                String temp;

                try {
                    temp = versionMap.get("version-code");
                    Config.versionCode = Integer.parseInt(temp);
                    Config.versionName = versionMap.get("version-name");
                } catch (Exception e) {
                    logger.error("Exception", e);
                }
                logger.debug("versionCode : {}", versionCode);
                logger.debug("versionName : {}", versionName);
            } else {
                StringBuilder sb = new StringBuilder();
                logger.debug("Can not find version.properties file in resources");
                sb.append("\nversion.properties\n");
                sb.append("ex)\n");
                sb.append("version-code=1\n");
                sb.append("version-name=0.0.1\n");
                logger.warn("{}", sb.toString());
            }
        } catch (Exception e) {
            logger.error("Exception", e);
        }
    }

    private static void onLoadApplicationProperties(String propertiesName) {
        try {
            // Resources 저장된 기본 properties 정보 메모리 로드
            loadApplicationProperties(getResourceAsStream(propertiesName));
        } catch (Exception e) {
            logger.info("not found resource properties file({})", propertiesName);
        }

        // 외부 config 디렉토리에 저장된 기본 properties 정보 로드
        try {
            File file = new File(appRunDir + "/config/" + propertiesName);
            if (file.exists()) {
                loadApplicationProperties(new FileInputStream(file));
            }
        } catch (FileNotFoundException e) {
            logger.error("Exception", e);
        }
    }

    private static boolean loadApplicationProperties(InputStream inputStream) {
        return loadProperties(inputStream, Config.map);
    }

    public static boolean loadProperties(InputStream inputStream, Map<String, String> map) {
        if (inputStream != null) {
            try {
                BufferedInputStream bis = new BufferedInputStream(inputStream);

                Properties properties = new Properties();
                properties.load(bis);

                Enumeration keys = properties.keys();
                while (keys.hasMoreElements()) {
                    String key = (String) keys.nextElement();
                    map.put(key, properties.getProperty(key, ""));
                }
                bis.close();
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private final static String DEFAULT_LOGGING_CONFIG_XML =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<configuration>\n" +
                    "    <appender name=\"consoleAppender\" class=\"ch.qos.logback.core.ConsoleAppender\">\n" +
                    "        <layout class=\"ch.qos.logback.classic.PatternLayout\">\n" +
                    "            <pattern>[%d{yyyy:MM:dd HH:mm:ss.SSS}][%-5level][%logger{36}.%method] : %msg%n</pattern>\n" +
                    "        </layout>\n" +
                    "    </appender>\n" +
                    "\n" +
                    "    <root level=\"DEBUG\">\n" +
                    "        <appender-ref ref=\"consoleAppender\"/>\n" +
                    "    </root>\n" +
                    "</configuration>";

    private static Logger loadLoggingConfig(InputStream inputStream) {
        if (inputStream != null) {
            LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
            try {
                JoranConfigurator configurator = new JoranConfigurator();
                configurator.setContext(lc);
                // Call context.reset() to clear any previous configuration, e.g. default
                // configuration. For multi-step configuration, omit calling context.reset().
                lc.reset();
                configurator.doConfigure(inputStream);
            } catch (JoranException je) {
                // StatusPrinter will handle this
                je.printStackTrace();
            }
            StatusPrinter.printInCaseOfErrorsOrWarnings(lc);
        }
        return LoggerFactory.getLogger(Config.class);
    }

    public static boolean isEmpty(String str) {
        return (str == null || str.length() == 0);
    }

    public static String get(String key) {
        return map.get(key);
    }

    public static boolean getBoolean(String key) {
        return getBoolean(key, false);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        String value = map.get(key);
        if (value != null) {
            return ConvertUtils.objectToBoolean(value, defaultValue);
        }
        return defaultValue;
    }

    public static int getInteger(String key) {
        return getInteger(key, 0);
    }

    public static int getInteger(String key, int defaultValue) {
        String value = map.get(key);
        if (value != null) {
            return ConvertUtils.objectToInteger(value, defaultValue);
        }
        return defaultValue;
    }

    public static long getLong(String key) {
        return getLong(key, 0);
    }

    public static long getLong(String key, long defaultValue) {
        String value = map.get(key);
        if (value != null) {
            return ConvertUtils.objectToLong(value, defaultValue);
        }
        return defaultValue;
    }

    public static double getDouble(String key) {
        return getDouble(key, 0);
    }

    public static double getDouble(String key, double defaultValue) {
        String value = map.get(key);
        if (value != null) {
            return ConvertUtils.objectToDouble(value, defaultValue);
        }
        return defaultValue;
    }

    public static float getFloat(String key) {
        return getFloat(key);
    }

    public static float getFloat(String key, float defaultValue) {
        String value = map.get(key);
        if (value != null) {
            return ConvertUtils.objectToFloat(value, defaultValue);
        }
        return defaultValue;
    }

    public static String put(String key, String value) {
        return map.put(key, value);
    }

    public static String remove(String key) {
        return map.remove(key);
    }
}
