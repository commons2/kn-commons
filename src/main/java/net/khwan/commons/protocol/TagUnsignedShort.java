package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.utils.ConvertUtils;

import java.nio.ByteOrder;

public class TagUnsignedShort implements ProtocolBytes, Cloneable {
    private int value;

    private ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;

    public TagUnsignedShort() {
    }

    public TagUnsignedShort(int value) {
        setValue(value);
    }

    public TagUnsignedShort(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public TagUnsignedShort(int value, ByteOrder byteOrder) {
        setValue(value);
        this.byteOrder = byteOrder;
    }

    @Override
    public int getBytesLength() {
        return 2;
    }

    @Override
    public void setBytes(byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        setValue(ConvertUtils.byteArrayToShort(byteArray, startPos, byteOrder));
        return 2;
    }

    @Override
    public byte[] getBytes() {
        return ConvertUtils.shortToByteArray((short) value, byteOrder);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = (((short) value) & 0xFFFF);
    }

    public void plus(int value) {
        setValue(this.value + value);
    }

    public void minus(int value) {
        setValue(this.value - value);
    }

    public void divide(double value) {
        setValue((int) Math.round(this.value / value));
    }

    public void multiply(double value) {
        setValue((int) Math.round(this.value * value));
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagUnsignedShort) {
            return (((TagUnsignedShort) obj).getValue() == value);
        }
        return super.equals(obj);
    }
}
