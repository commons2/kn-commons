package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.utils.ConvertUtils;

import java.nio.ByteOrder;

public class TagUnsignedInteger implements ProtocolBytes, Cloneable {
    private long value;

    private ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;

    public TagUnsignedInteger() {

    }

    public TagUnsignedInteger(long value) {
        setValue(value);
    }

    public TagUnsignedInteger(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public TagUnsignedInteger(long value, ByteOrder byteOrder) {
        setValue(value);
        this.byteOrder = byteOrder;
    }

    @Override
    public int getBytesLength() {
        return 4;
    }

    @Override
    public void setBytes(byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        setValue(ConvertUtils.byteArrayToInteger(byteArray, startPos, byteOrder));
        return 4;
    }

    @Override
    public byte[] getBytes() {
        return ConvertUtils.integerToByteArray((int) value, byteOrder);
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = (value & 0xFFFFFFFFL);
    }


    public void plus(long value) {
        setValue(this.value + value);
    }

    public void minus(long value) {
        setValue(this.value - value);
    }

    public void divide(double value) {
        setValue((int) Math.round(this.value / value));
    }

    public void multiply(double value) {
        setValue((int) Math.round(this.value * value));
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagUnsignedInteger) {
            return (((TagUnsignedInteger) obj).getValue() == value);
        }
        return super.equals(obj);
    }
}
