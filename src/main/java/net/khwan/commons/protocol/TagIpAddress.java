package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.utils.ConvertUtils;

public class TagIpAddress implements ProtocolBytes, Cloneable {
    private byte[] values = new byte[4];
    private String strValue = null;
    private StringBuilder strBuilder = new StringBuilder();

    public TagIpAddress() {
    }

    public TagIpAddress(String ipAddress) throws Exception {
        if (!setValue(ipAddress)) {
            throw new Exception("invalid defaultValue(" + ipAddress + ")");
        }
    }

    public TagIpAddress(byte value1, byte value2, byte value3, byte value4) {
        setValue(value1, value2, value3, value4);
    }

    @Override
    public int getBytesLength() {
        return 4;
    }

    @Override
    public void setBytes(final byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        setValue(byteArray[startPos], byteArray[startPos + 1], byteArray[startPos + 2], byteArray[startPos + 3]);
        return 4;
    }

    @Override
    public byte[] getBytes() {
        return values;
    }

    public byte[] getValue() {
        return values;
    }

    public void setValue(byte value1, byte value2, byte value3, byte value4) {
        this.values[0] = (value1);
        this.values[1] = (value2);
        this.values[2] = (value3);
        this.values[3] = (value4);

        strBuilder.setLength(0);
        strBuilder.append((value1 & 0xFF));
        strBuilder.append('.');
        strBuilder.append((value2 & 0xFF));
        strBuilder.append('.');
        strBuilder.append((value3 & 0xFF));
        strBuilder.append('.');
        strBuilder.append((value4 & 0xFF));
        strValue = strBuilder.toString();
    }

    public boolean setValue(String ipAddress) {
        byte[] values = ConvertUtils.stringIpAddressToByteArray(ipAddress);
        if (values == null) return false;
        setValue(values[0], values[1], values[2], values[3]);
        return true;
    }

    @Override
    public String toString() {
        return strValue;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagIpAddress) {
            byte bytes[] = ((TagIpAddress) obj).getValue();
            return bytes[0] == this.values[0] && bytes[1] == this.values[1] && bytes[2] == this.values[2] && bytes[3] == this.values[3];
        }
        return super.equals(obj);
    }
}
