package net.khwan.commons.protocol.model;

import net.khwan.commons.utils.HexBin;

public class ReceiveResult {
    private final boolean result;
    private final Object type;
    private final byte[] receiveBuffer;
    private final int dataLength;
    private final String msg;

    public ReceiveResult(boolean result, byte[] buffer) {
        this.result = result;
        this.type = null;
        this.receiveBuffer = buffer;
        this.dataLength = 0;
        this.msg = null;
    }

    public ReceiveResult(boolean result, String msg) {
        this.result = result;
        this.type = null;
        this.receiveBuffer = null;
        this.dataLength = 0;
        this.msg = msg;
    }

    public ReceiveResult(boolean result, byte[] buffer, String msg) {
        this.result = result;
        this.type = null;
        this.receiveBuffer = buffer;
        this.dataLength = 0;
        this.msg = msg;
    }

    public ReceiveResult(boolean result, byte[] buffer, int dataLength) {
        this.result = result;
        this.type = null;
        this.receiveBuffer = buffer;
        this.dataLength = dataLength;
        this.msg = null;
    }

    public ReceiveResult(boolean result, byte[] buffer, int dataLength, String msg) {
        this.result = result;
        this.type = null;
        this.receiveBuffer = buffer;
        this.dataLength = dataLength;
        this.msg = msg;
    }

    public ReceiveResult(boolean result, Object type, byte[] buffer, int dataLength, String msg) {
        this.result = result;
        this.type = type;
        this.receiveBuffer = buffer;
        this.dataLength = dataLength;
        this.msg = msg;
    }

    public boolean isResult() {
        return result;
    }

    public Object getType() {
        return type;
    }

    public byte[] getReceiveBuffer() {
        return receiveBuffer;
    }

    public int getDataLength() {
        return dataLength;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ReceiveResult{");
        sb.append("result=").append(result);
        if (type != null) {
            sb.append(", defaultValue=").append(type);
        }
        sb.append(", receiveBuffer=").append(HexBin.encode(receiveBuffer));
        sb.append(", dataLength=").append(dataLength);
        sb.append(", msg='").append(msg).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
