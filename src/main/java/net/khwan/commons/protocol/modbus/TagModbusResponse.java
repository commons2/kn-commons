package net.khwan.commons.protocol.modbus;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.protocol.TagUnsignedByte;
import net.khwan.commons.utils.ConvertUtils;
import net.khwan.commons.utils.HexBin;

public class TagModbusResponse {
    private byte slaveAddress;
    private byte function;
    private TagUnsignedByte byteCount = new TagUnsignedByte();
    private byte data[];

    public int getBytesLength() {
        return (3 + ((data != null) ? data.length : 0));
    }

    public TagModbusResponse() {
    }

    public TagModbusResponse(TagModbusRequest tagModbusRequest) {
        this.slaveAddress = (byte)tagModbusRequest.getSlaveAddress();
        this.function = tagModbusRequest.getFunction().toByte();
    }

    public TagModbusResponse(TagModbusRequest tagModbusRequest, byte[] data) {
        this.slaveAddress = (byte)tagModbusRequest.getSlaveAddress();
        this.function = tagModbusRequest.getFunction().toByte();
        setData(data);
    }

    public TagModbusResponse(int slaveAddress, ModbusFunction function) {
        this.slaveAddress = (byte)slaveAddress;
        this.function = function.toByte();
    }

    public TagModbusResponse(int slaveAddress, ModbusFunction function, byte[] data) {
        this.slaveAddress = (byte)slaveAddress;
        this.function = function.toByte();
        setData(data);
    }

    public void setBytes(final byte[] byteArray) throws DeserializationException {
        if (byteArray.length < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, getBytesLength());
        }
        int byteArrayPosition = 0;
        slaveAddress = byteArray[byteArrayPosition++];
        function = byteArray[byteArrayPosition++];
        byteArrayPosition += byteCount.setBytes(byteArray, byteArrayPosition);
        if (byteArray.length < (byteCount.getValue() + byteArrayPosition)) {
            DeserializationException.newThrow(getClass(), byteArray, getBytesLength());
        }
        data = new byte[byteCount.getValue()];
        ConvertUtils.byteArrayCopy(byteArray, byteArrayPosition, data);
    }

    public byte[] getBytes() {
        byte[] byteArray = new byte[getBytesLength()];
        int byteArrayPosition = 0;
        byteArrayPosition += ConvertUtils.byteArrayCopy(slaveAddress, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(function, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(byteCount.getBytes(), byteArray, byteArrayPosition);
        if (data != null) {
            ConvertUtils.byteArrayCopy(data, byteArray, byteArrayPosition);
        }
        return byteArray;
    }

    public int getSlaveAddress() {
        return slaveAddress;
    }

    public void setSlaveAddress(int slaveAddress) {
        this.slaveAddress = (byte)slaveAddress;
    }

    public ModbusFunction getFunction() {
        return ModbusFunction.get(function);
    }

    public void setFunction(ModbusFunction function) {
        this.function = function.toByte();
    }

    public int getByteCount() {
        return byteCount.getValue();
    }

    public void setByteCount(int byteCount) {
        this.byteCount.setValue(byteCount);
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
        if (data != null) {
            this.byteCount.setValue(data.length);
        } else {
            this.byteCount.setValue(0);
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TagModbusResponse{");
        sb.append("slaveAddress=").append(slaveAddress);
        sb.append(", function=").append(function);
        sb.append(", byteCount=").append(byteCount);
        sb.append(", data=").append(HexBin.encode(data));
        sb.append('}');
        return sb.toString();
    }
}
