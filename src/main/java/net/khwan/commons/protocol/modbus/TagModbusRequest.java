package net.khwan.commons.protocol.modbus;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.protocol.TagUnsignedShort;
import net.khwan.commons.utils.ConvertUtils;

import java.nio.ByteOrder;

public class TagModbusRequest {
    private byte slaveAddress;
    private byte function;
    private TagUnsignedShort startingAddress = new TagUnsignedShort();
    private TagUnsignedShort quantityOfCoils = new TagUnsignedShort();

    private ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
    public final static int bytesLength = 6;

    public TagModbusRequest() {
    }

    public TagModbusRequest(int slaveAddress, ModbusFunction function) {
        this.slaveAddress = (byte) slaveAddress;
        this.function = function.toByte();
    }

    public TagModbusRequest(int slaveAddress, ModbusFunction function, int startingAddress, int quantityOfCoils) {
        this.slaveAddress = (byte) slaveAddress;
        this.function = function.toByte();
        this.startingAddress.setValue(startingAddress);
        this.quantityOfCoils.setValue(quantityOfCoils);
    }

    public TagModbusRequest(int slaveAddress, ModbusFunction function, int startingAddress, int quantityOfCoils, ByteOrder byteOrder) {
        this.slaveAddress = (byte) slaveAddress;
        this.function = function.toByte();
        this.startingAddress.setValue(startingAddress);
        this.quantityOfCoils.setValue(quantityOfCoils);
        setByteOrder(byteOrder);
    }

    public TagModbusRequest(int slaveAddress, ModbusFunction function, ByteOrder byteOrder) {
        this.slaveAddress = (byte) slaveAddress;
        this.function = function.toByte();
        this.byteOrder = byteOrder;
    }

    public TagModbusRequest(ByteOrder byteOrder) {
        setByteOrder(byteOrder);
    }

    public int getBytesLength() {
        return bytesLength;
    }

    public void setBytes(final byte[] byteArray) throws DeserializationException {
        if (byteArray.length < bytesLength) {
            DeserializationException.newThrow(getClass(), byteArray, getBytesLength());
        }
        int byteArrayPosition = 0;
        slaveAddress = byteArray[byteArrayPosition++];
        function = byteArray[byteArrayPosition++];
        byteArrayPosition += startingAddress.setBytes(byteArray, byteArrayPosition);
        byteArrayPosition += quantityOfCoils.setBytes(byteArray, byteArrayPosition);
    }

    public byte[] getBytes() {
        byte[] byteArray = new byte[getBytesLength()];
        int byteArrayPosition = 0;
        byteArrayPosition += ConvertUtils.byteArrayCopy(slaveAddress, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(function, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(startingAddress.getBytes(), byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(quantityOfCoils.getBytes(), byteArray, byteArrayPosition);
        return byteArray;
    }

    public int getSlaveAddress() {
        return slaveAddress;
    }

    public void setSlaveAddress(int slaveAddress) {
        this.slaveAddress = (byte) slaveAddress;
    }

    public ModbusFunction getFunction() {
        return ModbusFunction.get(function);
    }

    public void setFunction(ModbusFunction function) {
        this.function = function.toByte();
    }

    public int getStartingAddress() {
        return startingAddress.getValue();
    }

    public void setStartingAddress(int startingAddress) {
        this.startingAddress.setValue(startingAddress);
    }

    public int getQuantityOfCoils() {
        return quantityOfCoils.getValue();
    }

    public void setQuantityOfCoils(int quantityOfCoils) {
        this.quantityOfCoils.setValue(quantityOfCoils);
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }

    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
        this.startingAddress.setByteOrder(byteOrder);
        this.quantityOfCoils.setByteOrder(byteOrder);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TagModbusRequest{");
        sb.append("slaveAddress=").append(slaveAddress);
        sb.append(", function=").append(function);
        sb.append(", startingAddress=").append(startingAddress);
        sb.append(", quantityOfCoils=").append(quantityOfCoils);
        sb.append(", byteOrder=").append(byteOrder);
        sb.append('}');
        return sb.toString();
    }
}
