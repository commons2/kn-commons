package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;

public class TagUnsignedByte implements ProtocolBytes, Cloneable {
    private int value;

    public TagUnsignedByte() {
    }

    public TagUnsignedByte(int value) {
        setValue(value);
    }

    @Override
    public int getBytesLength() {
        return 1;
    }

    @Override
    public void setBytes(final byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        setValue(byteArray[startPos]);
        return 1;
    }

    @Override
    public byte[] getBytes() {
        return new byte[]{(byte) value};
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = (((byte) value) & 0xFF);
    }

    public void plus(int value) {
        setValue(this.value + value);
    }

    public void minus(int value) {
        setValue(this.value - value);
    }

    public void divide(double value) {
        setValue((int) Math.round(this.value / value));
    }

    public void multiply(double value) {
        setValue((int) Math.round(this.value * value));
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagUnsignedByte) {
            return (((TagUnsignedByte) obj).getValue() == value);
        }
        return super.equals(obj);
    }
}
