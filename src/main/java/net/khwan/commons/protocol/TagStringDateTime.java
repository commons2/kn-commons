package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.date.DateUtils;
import net.khwan.commons.utils.ConvertUtils;

import java.text.ParseException;
import java.util.Date;

public class TagStringDateTime implements ProtocolBytes, Cloneable {
    private byte[] valueBytes;
    private String valueString;
    private Date valueDate;

    private final int byteLength;
    private String dateFormat = "yyyy-MM-dd HH:mm:ss";

    public TagStringDateTime() {
        this(19);
    }

    public TagStringDateTime(int length) {
        this(null, length, null);
    }

    public TagStringDateTime(int length, String dateFormat) {
        this(null, length, dateFormat);
    }

    public TagStringDateTime(String value, int length) {
        this(value, length, null);
    }

    public TagStringDateTime(String value, int length, String dateFormat) {
        if (length < 0) {
            throw new RuntimeException("invalid length(" + length + ")");
        }
        this.byteLength = length;
        this.valueBytes = new byte[length];
        if (dateFormat != null) {
            this.dateFormat = dateFormat;
        }
        setValue(value);
    }

    @Override
    public int getBytesLength() {
        return byteLength;
    }

    @Override
    public void setBytes(byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        try {
            ConvertUtils.byteArrayCopy(byteArray, startPos, valueBytes, 0, byteLength);
            this.valueString = ConvertUtils.byteArrayToString(valueBytes);
            if (!isEmpty(valueString)) {
                this.valueDate = DateUtils.dateFromString(this.valueString, this.dateFormat);
            } else {
                this.valueDate = null;
            }
        } catch (ParseException e) {
            throw new DeserializationException(e.getMessage());
        }

        return byteLength;
    }

    @Override
    public byte[] getBytes() {
        return valueBytes;
    }

    @Override
    public String toString() {
        return valueString;
    }

    public String getValue() {
        return valueString;
    }

    public void setValue(String value) {
        if (!isEmpty(value)) {
            byte[] bytes = ConvertUtils.stringToByteArray(value, this.byteLength);
            if (bytes == null) throw new RuntimeException("NullPointException (value " + value + ")");
            System.arraycopy(bytes, 0, this.valueBytes, 0, this.byteLength);
            if (this.valueBytes[this.byteLength - 1] != 0x00) {
                this.valueString = ConvertUtils.byteArrayToString(bytes);
            } else {
                this.valueString = value;
            }
            try {
                this.valueDate = DateUtils.dateFromString(this.valueString, this.dateFormat);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public Date getDate() {
        return valueDate;
    }

    public void setDate(Date date) {
        this.valueDate = date;
        if (date != null) {
            this.valueString = DateUtils.stringFromDate(date, this.dateFormat);
            setValue(this.valueString);
        }
    }

    public static boolean isEmpty(String str) {
        return (str == null || str.length() == 0);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagString) {
            String value = ((TagString) obj).getValue();
            if (valueString != null) {
                return valueString.equals(value);
            } else if (value == null) return true;
        }
        return super.equals(obj);
    }
}
