package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;

public interface ProtocolBytes {
    int getBytesLength();

    void setBytes(final byte[] byteArray) throws DeserializationException;

    byte[] getBytes();
}
