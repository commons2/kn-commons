package net.khwan.commons.protocol.converter;

import net.khwan.commons.protocol.model.ReceiveResult;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Queue;

/**
 * Created by kkh on 2017-03-31.
 */
public interface ConvertProtocol {
    void setStxEtx(byte[] stx, byte[] etx);

    void setStxEtx(byte stx, byte etx);

    void setStxEtx(byte stx, byte[] etx);

    void setStxEtx(byte[] stx, byte etx);

    byte[] getStx();

    byte[] getEtx();

    void setDataLengthPosition(int position, DataLengthSize dataLengthSize);

    void setDataLengthPosition(int position, DataLengthSize dataLengthSize, int dataMoreLenSize);

    void setDataValid(DataValid dataValid);

    void setDataValid(DataValid dataValid, int dataValidStartPos);

    void setDataValid(DataValid dataValid, ByteOrder byteOrder);

    void setDataValid(DataValid dataValid, ByteOrder byteOrder, int dataValidStartPos);

    DataValid getDataValid();

    void setPacketTotalLength(int packetTotalLength);

    ByteOrder getByteOrder();

    void setByteOrder(ByteOrder mByteOrder);

    void setReceiveTimeoutMillis(long receiveTimeoutMillis);

    int getReceivePacketMinimumLength();

    void setReceivePacketMinimumLength(int receivePacketMinimumLength);

    void clearBuffer();

    byte[] sendPacket(byte[] buffer) throws BufferOverflowException;

    Queue<ReceiveResult> receivePacket(final byte[] receiveData, final int receiveDataSize);

    byte getCheckSum(byte[] buffer, int length);

    byte getCheckSum(byte[] buffer, int startPos, int length);

    byte getReverseCheckSum(byte[] buffer, int length);

    byte getReverseCheckSum(byte[] buffer, int startPos, int length);

    byte getXorCheckSum(byte[] buffer, int length);

    byte getXorCheckSum(byte[] buffer, int startPos, int length);

    int getCRC16(byte[] buffer, int bufferSize);

    int getModbusCRC16(byte[] buffer, int bufferSize);

    enum DataValid {
        NONE, CRC16, REVERSE_CHECKSUM, CHECKSUM, MODBUS_CRC16, XOR_CHECKSUM
    }

    enum DataLengthSize {
        NONE, UNSIGNED_BYTE, UNSIGNED_SHORT, INT
    }
}