package net.khwan.commons.protocol.converter;

import net.khwan.commons.protocol.model.ReceiveResult;
import net.khwan.commons.utils.ConvertUtils;
import net.khwan.commons.utils.HexBin;

import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.util.Queue;

/**
 * Created by kkh on 2017-03-31.
 */
public class DefaultConvertProtocol extends AbstractConvertProtocol {
    //    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ConvertProtocol.class);
    public DefaultConvertProtocol(int bufferMaxSize) {
        super(bufferMaxSize);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public byte[] sendPacket(byte[] buffer) throws BufferOverflowException {
        if (buffer == null) return null;

        Protocol protocol = this.protocol;
        ByteBuffer byteBuffer = this.sendByteBuffer;

        // 데이터 시작
        if (protocol.getStx() != null) {
            byteBuffer.put(protocol.getStx());
        }

        // 데이터 검증
        byte[] validBytes = null;
        if (protocol.getDataValid() != DataValid.NONE) {
            if (protocol.getDataValid() == DataValid.CRC16) {
                validBytes = ConvertUtils.shortToByteArray((short) getCRC16(buffer, buffer.length), dataValidByteOrder);
            } else if (protocol.getDataValid() == DataValid.MODBUS_CRC16) {
                validBytes = ConvertUtils.shortToByteArray((short) getModbusCRC16(buffer, buffer.length), dataValidByteOrder);
            } else if (protocol.getDataValid() == DataValid.CHECKSUM) {
                validBytes = new byte[]{getCheckSum(buffer, this.protocol.getDataValidStartPos(), buffer.length)};
            } else if (protocol.getDataValid() == DataValid.REVERSE_CHECKSUM) {
                validBytes = new byte[]{getReverseCheckSum(buffer, this.protocol.getDataValidStartPos(), buffer.length)};
            } else if (protocol.getDataValid() == DataValid.XOR_CHECKSUM) {
                validBytes = new byte[]{getXorCheckSum(buffer, this.protocol.getDataValidStartPos(), buffer.length)};
            }
        }

        byteBuffer.put(buffer);
        if (validBytes != null) {
            byteBuffer.put(validBytes);
        }

        // 데이터 종료
        if (protocol.getEtx() != null) {
            byteBuffer.put(protocol.getEtx());
        }

        // 버퍼 복사
        buffer = ConvertUtils.byteArrayCopyOf(byteBuffer.array(), byteBuffer.position());
        byteBuffer.clear();
        return buffer;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public Queue<ReceiveResult> receivePacket(byte[] receiveData, int receiveDataSize) {
        Queue<ReceiveResult> resultQueue = receiveResultQueue;
        resultQueue.clear();

        Protocol protocol = this.protocol;
        Receive receive = this.receive;

        // Timeout 시간이 0 보다 클 경우에만(기본 Timeout 처리 없음)
        long currentNanoTime = 0;
        if (receive.getTimeoutMillis() > 0) {
            // 이전 데이터 처리 시간이 Timeout 시간보다 크면 초기화 처리
            currentNanoTime = System.nanoTime();
            long elapsedTimeMillis = (currentNanoTime - receive.getStartNanoTime()) / 1000000;
            if (elapsedTimeMillis > receive.getTimeoutMillis()) {
                if (receive.getBufferCount() > 0) {
                    addReceiveErrorPacket("Receive packet timeout(" + elapsedTimeMillis + " ms elapsed)");
                }
                receive.clearBuffer(currentNanoTime);
            }
        }

        final byte[] stx = protocol.getStx();
        final byte[] etx = protocol.getEtx();
        int stxLength = (stx != null) ? stx.length : 0;
        int etxLength = (etx != null) ? etx.length : 0;

        int copyLength;
        int remainingReceiveDataLength = receiveDataSize;
        byte bTemp = 0;
        for (int receiveDataCount = 0; receiveDataCount < receiveDataSize; ) {
            if (stxLength > 0 && !receive.isStart()) {
                bTemp = receiveData[receiveDataCount];
                receive.add(bTemp);
            }

            if (!receive.isStart()) {
                if (stxLength > 0) {
                    if (receive.getBufferCount() >= stxLength && stx[stxLength - 1] == bTemp) {
                        if (comparisonResults(stx, stxLength - 1, receive.getBuffer(), receive.getBufferCount() - 1)) {
                            startReceiveBuffer(receive, currentNanoTime, false);
                            receiveDataCount++;
                            remainingReceiveDataLength = receiveDataSize - receiveDataCount;
                        } else {
                            // stx error
                            addReceiveErrorPacket("Invalid stx(" + HexBin.encode(ConvertUtils.byteArrayCopyOf(receive.getBuffer(), receive.getBufferCount() - etxLength, etxLength)) + ")");
                            receive.clearBuffer(currentNanoTime);
                            receiveDataCount++;
                            remainingReceiveDataLength = receiveDataSize - receiveDataCount;
                            continue;
                        }
                    } else {
                        receiveDataCount++;
                        remainingReceiveDataLength = receiveDataSize - receiveDataCount;
                    }
                } else {
                    startReceiveBuffer(receive, currentNanoTime);
                }
            } else {
                if (!receive.hasPacketTotalLength() && protocol.getDataLenSize() > 0 && receive.getDataLength() == -1) {
                    // dataLengthPosition 까지 복사
                    copyLength = (protocol.getDataLenLastPos() + 1) - receive.getBufferCount();
                    // 남은 수신 버퍼 길이가 복사할 버퍼 길이보다 작다면..
                    if (remainingReceiveDataLength < copyLength) {
                        receiveDataCount += receive.byteArrayCopy(receiveData, receiveDataCount, remainingReceiveDataLength);
                        remainingReceiveDataLength = receiveDataSize - receiveDataCount;
                        continue;
                    }
                    receiveDataCount += receive.byteArrayCopy(receiveData, receiveDataCount, copyLength);
                    remainingReceiveDataLength = receiveDataSize - receiveDataCount;
                    if (protocol.getDataLenSize() == 1) {
                        // unsigned byte
                        receive.setDataLength(receive.getBuffer()[receive.getBufferCount() - 1] & 0xFF);
                    } else if (protocol.getDataLenSize() == 2) {
                        // unsigned short
                        receive.setDataLength(ConvertUtils.byteArrayToShort(receive.getBuffer(), receive.getBufferCount() - 2, byteOrder) & 0xFFFF);
                    } else if (protocol.getDataLenSize() == 4) {
                        // int
                        receive.setDataLength(ConvertUtils.byteArrayToInteger(receive.getBuffer(), receive.getBufferCount() - 4, byteOrder));
                    }

                    if (receive.getDataLength() > BUFFER_MAX_SIZE) {
                        receive.clearBuffer(currentNanoTime);
                    } else {
                        receive.setPacketTotalLength(
                                protocol.getDataLenLastPos() +
                                        1 +
                                        receive.getDataLength() +
                                        protocol.getDataValidSize() +
                                        etxLength +
                                        protocol.getDataMoreLenSize()
                        );
                    }
                } else {
                    copyLength = receive.getPacketTotalLength() - receive.getBufferCount();
                    if (copyLength <= remainingReceiveDataLength) {
                        receiveDataCount += receive.byteArrayCopy(receiveData, receiveDataCount, copyLength);
                        remainingReceiveDataLength = receiveDataSize - receiveDataCount;

                        // etx check
                        if (!comparisonResults(etx, etxLength - 1, receive.getBuffer(), receive.getBufferCount() - 1)) {
                            // etx error
                            addReceiveErrorPacket("Invalid etx(" + HexBin.encode(ConvertUtils.byteArrayCopyOf(receive.getBuffer(), receive.getBufferCount() - etxLength, etxLength)) + ")");
                            receive.clearBuffer(currentNanoTime);
                            continue;
                        }
                        // 완료 처리
                        ReceiveResult receiveResult = receiveResultValid(receive.getBuffer(), receive.getBufferCount() - etxLength);
                        resultQueue.add(receiveResult);
                        receive.clearBuffer(currentNanoTime);
                    } else {
                        receiveDataCount += receive.byteArrayCopy(receiveData, receiveDataCount, remainingReceiveDataLength);
                        remainingReceiveDataLength = receiveDataSize - receiveDataCount;
                    }
                }
            }
        }

        return resultQueue;
    }
}
