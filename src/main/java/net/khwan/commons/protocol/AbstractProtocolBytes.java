package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.utils.ConvertUtils;

public abstract class AbstractProtocolBytes implements ProtocolBytes {
    protected byte[] basePacket;

    public AbstractProtocolBytes(int basePacketSize) {
        this.basePacket = new byte[basePacketSize];
    }

    public int getBytesLength() {
        return (basePacket != null) ? basePacket.length : 0;
    }

    @Override
    public void setBytes(final byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        int byteArrayPosition = startPos;
        byteArrayPosition += ConvertUtils.byteArrayCopy(byteArray, byteArrayPosition, basePacket);
        return byteArrayPosition - startPos;
    }

    @Override
    public byte[] getBytes() {
        byte[] byteArray = new byte[getBytesLength()];
        ConvertUtils.byteArrayCopy(basePacket, byteArray, 0);
        return byteArray;
    }
}
