package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.utils.ConvertUtils;
import net.khwan.commons.date.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class TagTime implements ProtocolBytes, Cloneable {
    private byte hour;
    private byte minute;
    private byte second;

    public final static int bytesLength = 3;

    public int getBytesLength() {
        return bytesLength;
    }

    public TagTime() {
    }

    public TagTime(int hour, int minute, int second) {
        this.hour = (byte) hour;
        this.minute = (byte) minute;
        this.second = (byte) second;
    }

    @Override
    public void setBytes(final byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < bytesLength) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        int byteArrayPosition = startPos;
        hour = byteArray[byteArrayPosition++];
        minute = byteArray[byteArrayPosition++];
        second = byteArray[byteArrayPosition++];
        return byteArrayPosition - startPos;
    }

    @Override
    public byte[] getBytes() {
        byte[] byteArray = new byte[getBytesLength()];
        int byteArrayPosition = 0;
        byteArrayPosition += ConvertUtils.byteArrayCopy(hour, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(minute, byteArray, byteArrayPosition);
        ConvertUtils.byteArrayCopy(second, byteArray, byteArrayPosition);
        return byteArray;
    }

    public byte getHour() {
        return hour;
    }

    public void setHour(byte hour) {
        this.hour = hour;
    }

    public byte getMinute() {
        return minute;
    }

    public void setMinute(byte minute) {
        this.minute = minute;
    }

    public byte getSecond() {
        return second;
    }

    public void setSecond(byte second) {
        this.second = second;
    }

    public void setTimeNow() {
        Calendar cal = Calendar.getInstance();
        hour = (byte) (cal.get(Calendar.HOUR_OF_DAY));
        minute = (byte) (cal.get(Calendar.MINUTE));
        second = (byte) (cal.get(Calendar.SECOND));
    }

    public void setDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        setCalendar(calendar);
    }

    public void setCalendar(Calendar calendar) {
        hour = (byte) calendar.get(Calendar.HOUR_OF_DAY);
        minute = (byte) calendar.get(Calendar.MINUTE);
        second = (byte) calendar.get(Calendar.SECOND);
    }

    public Date toDate() {
        return DateUtils.dateFromString(this.toString(), DateUtils.DateFormat.DEFAULT_TIME);
    }

    public Calendar toCalendar() {
        return DateUtils.calendarFromString(this.toString(), DateUtils.DateFormat.DEFAULT_TIME);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(10);
        if (((int) hour / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) hour);
        sb.append(":");
        if (((int) minute / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) minute);
        sb.append(":");
        if (((int) second / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) second);
        return sb.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagTime) {
            TagTime tagTime = (TagTime) obj;
            return tagTime.getHour() == hour && tagTime.getMinute() == minute && tagTime.getSecond() == second;
        }
        return super.equals(obj);
    }
}