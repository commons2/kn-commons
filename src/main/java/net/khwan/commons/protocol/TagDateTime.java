package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.utils.ConvertUtils;
import net.khwan.commons.date.DateUtils;

import java.nio.ByteOrder;
import java.util.Calendar;
import java.util.Date;

public class TagDateTime implements ProtocolBytes, Cloneable {
    private short year;
    private byte month;
    private byte day;
    private byte hour;
    private byte minute;
    private byte second;

    private ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
    public final static int bytesLength = 7;

    @Override
    public int getBytesLength() {
        return bytesLength;
    }

    public TagDateTime() {
    }

    public TagDateTime(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public TagDateTime(int year, int month, int day, int hour, int minute, int second) {
        this.year = (short) year;
        this.month = (byte) month;
        this.day = (byte) day;
        this.hour = (byte) hour;
        this.minute = (byte) minute;
        this.second = (byte) second;
    }

    public TagDateTime(ByteOrder byteOrder, int year, int month, int day, int hour, int minute, int second) {
        this.year = (short) year;
        this.month = (byte) month;
        this.day = (byte) day;
        this.hour = (byte) hour;
        this.minute = (byte) minute;
        this.second = (byte) second;
        this.byteOrder = byteOrder;
    }

    @Override
    public void setBytes(final byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < bytesLength) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }
        int byteArrayPosition = startPos;
        year = ConvertUtils.byteArrayToShort(byteArray, byteArrayPosition, byteOrder);
        byteArrayPosition += ConvertUtils.SHORT_SIZE;
        month = byteArray[byteArrayPosition++];
        day = byteArray[byteArrayPosition++];
        hour = byteArray[byteArrayPosition++];
        minute = byteArray[byteArrayPosition++];
        second = byteArray[byteArrayPosition++];
        return byteArrayPosition - startPos;
    }

    @Override
    public byte[] getBytes() {
        byte[] byteArray = new byte[getBytesLength()];
        int byteArrayPosition = 0;
        byteArrayPosition += ConvertUtils.byteArrayCopy(year, byteArray, byteArrayPosition, byteOrder);
        byteArrayPosition += ConvertUtils.byteArrayCopy(month, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(day, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(hour, byteArray, byteArrayPosition);
        byteArrayPosition += ConvertUtils.byteArrayCopy(minute, byteArray, byteArrayPosition);
        ConvertUtils.byteArrayCopy(second, byteArray, byteArrayPosition);
        return byteArray;
    }

    public ByteOrder getByteOrder() {
        return byteOrder;
    }

    public void setByteOrder(ByteOrder byteOrder) {
        this.byteOrder = byteOrder;
    }

    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    public byte getMonth() {
        return month;
    }

    public void setMonth(byte month) {
        this.month = month;
    }

    public byte getDay() {
        return day;
    }

    public void setDay(byte day) {
        this.day = day;
    }

    public byte getHour() {
        return hour;
    }

    public void setHour(byte hour) {
        this.hour = hour;
    }

    public byte getMinute() {
        return minute;
    }

    public void setMinute(byte minute) {
        this.minute = minute;
    }

    public byte getSecond() {
        return second;
    }

    public void setSecond(byte second) {
        this.second = second;
    }

    public void setDateTimeNow() {
        Calendar cal = Calendar.getInstance();
        year = (short) (cal.get(Calendar.YEAR));
        month = (byte) (cal.get(Calendar.MONTH) + 1);
        day = (byte) (cal.get(Calendar.DAY_OF_MONTH));
        hour = (byte) (cal.get(Calendar.HOUR_OF_DAY));
        minute = (byte) (cal.get(Calendar.MINUTE));
        second = (byte) (cal.get(Calendar.SECOND));
    }

    public void setDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        setCalendar(calendar);
    }

    public void setCalendar(Calendar calendar) {
        year = (short) calendar.get(Calendar.YEAR);
        month = (byte) (calendar.get(Calendar.MONTH) + 1);
        day = (byte) calendar.get(Calendar.DAY_OF_MONTH);
        hour = (byte) calendar.get(Calendar.HOUR_OF_DAY);
        minute = (byte) calendar.get(Calendar.MINUTE);
        second = (byte) calendar.get(Calendar.SECOND);
    }

    public Date toDate() {
        return DateUtils.dateFromString(this.toString(), DateUtils.DateFormat.DEFULAT);
    }

    public Calendar toCalendar() {
        return DateUtils.calendarFromString(this.toString(), DateUtils.DateFormat.DEFULAT);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(20);

        if (((int) year / 10 == 0)) {
            sb.append("000");
        } else if (((int) year / 100 == 0)) {
            sb.append("00");
        } else if (((int) year / 1000 == 0)) {
            sb.append("0");
        }
        sb.append((int) year);
        sb.append("-");
        if (((int) month / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) month);
        sb.append("-");
        if (((int) day / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) day);
        sb.append(" ");
        if (((int) hour / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) hour);
        sb.append(":");
        if (((int) minute / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) minute);
        sb.append(":");
        if (((int) second / 10 == 0)) {
            sb.append("0");
        }
        sb.append((int) second);
        return sb.toString();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagDateTime) {
            TagDateTime tagDateTime = (TagDateTime) obj;
            return tagDateTime.getYear() == year && tagDateTime.getMonth() == month && tagDateTime.getDay() == day && tagDateTime.getHour() == hour && tagDateTime.getMinute() == minute && tagDateTime.getSecond() == second;
        }
        return super.equals(obj);
    }
}