package net.khwan.commons.protocol;

import net.khwan.commons.exception.DeserializationException;
import net.khwan.commons.utils.ConvertUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteOrder;

public class TagDouble implements ProtocolBytes, Cloneable {
    public enum ValueType {
        BYTE, UNSIGNED_BYTE, SHORT, UNSIGNED_SHORT, INTEGER
    }

    private ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;

    private boolean valueChangeFlag = false;

    private int value;
    private BigDecimal bigDecimal;
    private String strValue;            // 표시 값(입력값 100, 나누기 10, 소수점 자리 수 1 => 10.0)

    public final ValueType valueType;
    public final int divide;            // 나누기
    public final int decimalPlaces;     // 소수점 자리수

    private final byte bytesLength;     // bytes 길이


    public TagDouble(ValueType valueType, int divide, int decimalPlaces) {
        this(valueType, 0, null, divide, decimalPlaces);
    }

    public TagDouble(ValueType valueType, int value, int divide, int decimalPlaces) {
        this(valueType, value, null, divide, decimalPlaces);
    }

    public TagDouble(ValueType valueType, ByteOrder byteOrder, int divide, int decimalPlaces) {
        this(valueType, 0, byteOrder, divide, decimalPlaces);
    }

    private TagDouble(ValueType valueType, int value, ByteOrder byteOrder, int divide, int decimalPlaces) {
        this.valueType = valueType;
        this.value = value;
        if (byteOrder != null) {
            this.byteOrder = byteOrder;
        }

        if (divide == 0) divide = 1;
        this.divide = divide;
        if (decimalPlaces < 0) decimalPlaces = 0;
        this.decimalPlaces = decimalPlaces;

        if (valueType == null) {
            valueType = ValueType.BYTE;
        }

        if (valueType == ValueType.SHORT || valueType == ValueType.UNSIGNED_SHORT) {
            bytesLength = 2;
        } else if (valueType == ValueType.INTEGER) {
            bytesLength = 4;
        } else {
            bytesLength = 1;
        }
    }

    @Override
    public int getBytesLength() {
        return bytesLength;
    }

    @Override
    public void setBytes(byte[] byteArray) throws DeserializationException {
        setBytes(byteArray, 0);
    }

    public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {
        if (byteArray.length - startPos < getBytesLength()) {
            DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());
        }

        if (valueType == ValueType.UNSIGNED_BYTE) {
            value = byteArray[startPos] & 0xFF;
        } else if (valueType == ValueType.SHORT) {
            value = (ConvertUtils.byteArrayToShort(byteArray, startPos, byteOrder));
        } else if (valueType == ValueType.UNSIGNED_SHORT) {
            value = (ConvertUtils.byteArrayToShort(byteArray, startPos, byteOrder)) & 0xFFFF;
        } else if (valueType == ValueType.INTEGER) {
            value = (ConvertUtils.byteArrayToInteger(byteArray, startPos, byteOrder));
        } else {
            value = byteArray[startPos];
        }
        this.valueChangeFlag = true;

        return bytesLength;
    }

    @Override
    public byte[] getBytes() {
        if (valueType == ValueType.SHORT || valueType == ValueType.UNSIGNED_SHORT) {
            return ConvertUtils.shortToByteArray((short) value, byteOrder);
        } else if (valueType == ValueType.INTEGER) {
            return ConvertUtils.integerToByteArray(value, byteOrder);
        } else {
            return (new byte[]{(byte) value});
        }
    }

    public String getStringValue() {
        if (this.valueChangeFlag) convertValue();
        return strValue;
    }

    public void setStringValue(String value) {
        setDoubleValue(Double.parseDouble(value));
    }

    public double getDoubleValue() {
        if (this.valueChangeFlag) convertValue();
        if (bigDecimal != null) return bigDecimal.doubleValue();
        else return 0;
    }

    public void setDoubleValue(double value) {
        int intValue = (int) Math.round(value * divide);
        setValue(intValue);
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
        this.valueChangeFlag = true;
    }

    public void plus(int value) {
        setValue(this.value + value);
    }

    public void minus(int value) {
        setValue(this.value - value);
    }

    public void divide(double value) {
        setValue((int) Math.round(this.value / value));
    }

    public void multiply(double value) {
        setValue((int) Math.round(this.value * value));
    }

    private void convertValue() {
        if (strValue == null && value == 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("0.");
            for (int i = 0; i < decimalPlaces; i++) {
                sb.append(0);
            }
            strValue = sb.toString();
        } else {
            bigDecimal = BigDecimal.valueOf((value / (double) divide)).setScale(decimalPlaces, RoundingMode.DOWN);
            strValue = bigDecimal.toString();
        }
    }

    @Override
    public String toString() {
        if (this.valueChangeFlag) convertValue();
        return strValue;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        if (this.valueChangeFlag) convertValue();
        return super.clone();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TagDouble) {
            return (((TagDouble) obj).getValue() == value);
        }
        return super.equals(obj);
    }
}
