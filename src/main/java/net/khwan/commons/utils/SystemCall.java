package net.khwan.commons.utils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kkh on 2017-07-04.
 */
public final class SystemCall {
    //private static final Logger logger = LoggerFactory.getLogger(SystemCall.class);
    private SystemCall() {
    }

    public enum Type {
        EMPTY,
        WINDOWS_CMD("cmd.exe", "/c"),
        WINDOWS_EN_CMD("cmd.exe", "/c", "chcp", "437", "&", "cmd.exe", "/c"),
        LINUX_BASH_CMD("/bin/bash"),
        LINUX_BASH_C_CMD("/bin/bash", "-c");

        private String cmd[];

        Type(String... cmd) {
            this.cmd = cmd;
        }
    }

    private static String[] getCommand(Type type, String... cmd) {
        if (type == null || type == Type.EMPTY) return cmd;
        else {
            String newCmd[] = new String[type.cmd.length + cmd.length];
            System.arraycopy(type.cmd, 0, newCmd, 0, type.cmd.length);
            System.arraycopy(cmd, 0, newCmd, type.cmd.length, cmd.length);
            return newCmd;
        }
    }

    public static boolean execWaitForResult(Type type, String... cmd) throws IOException, InterruptedException {
        return execWaitForResult(getCommand(type, cmd));
    }

    public static boolean execWaitForResult(String... cmd) throws IOException, InterruptedException {
        boolean result = false;

        Process process = Runtime.getRuntime().exec(cmd);
        process.getInputStream().close();
        process.getErrorStream().close();
        process.getOutputStream().close();
        if (process.waitFor() == 0) {
            result = true;
        }
        process.destroy();

        return result;
    }

    public static class Result {
        public final int errorCode;
        public final String msg;
        public final Process process;
        public final String errorMsg;
        public final List<Exception> exceptionList;

        public Result(int errorCode, Process process, String msg, String errorMsg, List<Exception> exceptionList) {
            this.errorCode = errorCode;
            this.process = process;
            this.msg = msg;
            this.errorMsg = errorMsg;
            this.exceptionList = exceptionList;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "errorCode=" + errorCode +
                    ", msg='" + msg + '\'' +
                    ", errorMsg='" + errorMsg + '\'' +
                    '}';
        }
    }

    public static Result execResultToString(Charset charset, Type type, String... cmd) throws IOException {
        return execResultToString(charset, getCommand(type, cmd));
    }

    public static Result execResultToString(Type type, String... cmd) throws IOException {
        return execResultToString(Charset.forName("UTF-8"), getCommand(type, cmd));
    }

    public static Result execResultToString(String... cmd) throws IOException {
        return execResultToString(Charset.forName("UTF-8"), cmd);
    }

    public static Result execResultToString(Charset charset, String... cmd) throws IOException {
        final StringBuilder inputStringBuilder = new StringBuilder();
        final StringBuilder errorStringBuilder = new StringBuilder();

        final Process process = Runtime.getRuntime().exec(cmd);
        final List<Exception> exceptionList = new ArrayList<>();
        process.getOutputStream().close();

        final ReadThread inputStreamThread = startReadThread(process.getInputStream(), inputStringBuilder, charset, exceptionList);
        final ReadThread errorStreamThread = startReadThread(process.getErrorStream(), errorStringBuilder, charset, exceptionList);

        boolean interrupt = false;
        int errorCode = 0;
        try {
            errorCode = process.waitFor();
        } catch (InterruptedException e) {
            errorCode = Integer.MIN_VALUE;
            exceptionList.add(e);
            interrupt = true;
        }
        while (!interrupt && (inputStreamThread.isRunning() || errorStreamThread.isRunning())) {
        }

        process.destroy();

        return (new Result(errorCode, process, inputStringBuilder.toString(), errorStringBuilder.toString(), exceptionList));
    }

    private static ReadThread startReadThread(final InputStream is, final StringBuilder sb, final Charset charset, final List<Exception> exceptionList) {
        ReadThread thread = new ReadThread(is, sb, charset, exceptionList);
        thread.start();
        return thread;
    }

    private static class ReadThread extends Thread {
        private Boolean running = true;

        private final InputStream is;
        private final StringBuilder sb;
        private final Charset charset;
        private final List<Exception> exceptionList;

        public ReadThread(final InputStream is, final StringBuilder sb, final Charset charset, final List<Exception> exceptionList) {
            this.is = is;
            this.sb = sb;
            this.charset = charset;
            this.exceptionList = exceptionList;
        }

        @Override
        public void run() {
            try {
                byte[] buffer = new byte[8192];
                int size;
                while ((size = is.read(buffer)) != -1) {
                    if (size != buffer.length) {
                        sb.append(new String(ConvertUtils.byteArrayCopyOf(buffer, 0, size), charset));
                    } else {
                        sb.append(new String(buffer, charset));
                    }
                }
                is.close();
            } catch (Exception e) {
                exceptionList.add(e);
            }
            setRunning(false);
        }

        public void setRunning(boolean running) {
            synchronized (this.running) {
                this.running = running;
            }
        }

        public boolean isRunning() {
            synchronized (running) {
                return running;
            }
        }
    }
}
