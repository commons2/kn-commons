package net.khwan.commons.utils;

import java.util.Arrays;

public class CircularQueue<E> {
    private int currentSize; //Current Circular Queue Size
    private E[] circularQueueElements;
    private int maxSize; //Circular Queue maximum size

    private int rear;//rear position of Circular queue(new element enqueued at rear).
    private int front; //front position of Circular queue(element will be dequeued from front).

    @SuppressWarnings("unchecked")
    public CircularQueue(int maxSize) {
        this.maxSize = maxSize;
        circularQueueElements = (E[]) new Object[this.maxSize];
        currentSize = 0;
        front = 0;
        rear = 0;
    }

    // Enqueue elements to rear.
    public void enqueue(E item) {
        syncData(item);
    }

    // Dequeue element from Front.
    public E dequeue() {
        return syncData(null);
    }

    private synchronized E syncData(E item) {
        if (item == null) {
            // deQueue
            E deQueuedElement;
            if (isEmpty()) {
                return null;
            } else {
                deQueuedElement = circularQueueElements[front];
                circularQueueElements[front] = null;
                front = (front + 1) % circularQueueElements.length;
                currentSize--;
            }
            return deQueuedElement;
        } else {
            // enQueue
            if (isFull()) {
                dequeue();
            }
            circularQueueElements[rear] = item;
            rear = (rear + 1) % circularQueueElements.length;
            currentSize++;
            return null;
        }
    }

    // Check if queue is full.
    public boolean isFull() {
        return (currentSize == circularQueueElements.length);
    }

    // Check if Queue is empty.
    public boolean isEmpty() {
        return (currentSize == 0);
    }

    public int getSize() {
        return currentSize;
    }

    @Override
    public String toString() {
        return "CircularQueue [" + Arrays.toString(circularQueueElements) + "]";
    }
}
