package net.khwan.commons.utils;

import java.net.*;
import java.util.Enumeration;

public final class SystemUtils {
    private SystemUtils() {
    }

    /**
     * 운영체제 정보 확인
     */
    public enum OperatingSystemType {
        WINDOWS, MAC, LINUX, ANDROID, IOS, UNKNOWN
    }

    private final static OperatingSystemType osType;
    public static String PLATFORM;

    static {
        String jvmName = System.getProperty("java.vm.name", "").toLowerCase();
        String osName = System.getProperty("os.name", "").toLowerCase();
        String osArch = System.getProperty("os.arch", "").toLowerCase();
        String abiType = System.getProperty("sun.arch.abi", "").toLowerCase();
        String libPath = System.getProperty("sun.boot.library.path", "").toLowerCase();
        if (osName.startsWith("linux") && jvmName.startsWith("dalvik")) {
            osType = OperatingSystemType.ANDROID;
            osName = "android";
        } else if (osName.startsWith("darwin") && jvmName.startsWith("robovm")) {
            osType = OperatingSystemType.IOS;
            osName = "ios";
            osArch = "arm";
        } else if (osName.startsWith("darwin") && osName.startsWith("mac os x")) {
            osType = OperatingSystemType.MAC;
            osName = "macosx";
        } else {
            int spaceIndex = osName.indexOf(' ');
            if (spaceIndex > 0) {
                osName = osName.substring(0, spaceIndex);
            }
            if (osName.contains("linux")) {
                osType = OperatingSystemType.LINUX;
            } else if (osName.contains("win")) {
                osType = OperatingSystemType.WINDOWS;
            } else {
                osType = OperatingSystemType.UNKNOWN;
            }
        }

        if (osArch.equals("i386") || osArch.equals("i486") || osArch.equals("i586") || osArch.equals("i686")) {
            osArch = "x86";
        } else if (osArch.equals("amd64") || osArch.equals("x86-64") || osArch.equals("x64")) {
            osArch = "x86_64";
        } else if (osArch.startsWith("aarch64") || osArch.startsWith("armv8") || osArch.startsWith("arm64")) {
            osArch = "arm64";
        } else if ((osArch.startsWith("arm")) && ((abiType.equals("gnueabihf")) || (libPath.contains("openjdk-armhf")))) {
            osArch = "armhf";
        } else if (osArch.startsWith("arm")) {
            osArch = "arm";
        }
        PLATFORM = osName + "-" + osArch;
    }

    public static boolean isWindows() {
        return (osType == OperatingSystemType.WINDOWS);
    }

    public static boolean isMac() {
        return (osType == OperatingSystemType.MAC);
    }

    public static boolean isIOS() {
        return (osType == OperatingSystemType.IOS);
    }

    public static boolean isLinux() {
        return (osType == OperatingSystemType.LINUX);
    }

    public static boolean isAndroid() {
        return (osType == OperatingSystemType.ANDROID);
    }

    /**
     * IP 주소 가져오기
     *
     * @return IP Address
     * @throws SocketException Exception
     */
    public static String getIpAddress() throws SocketException {
        // IP 취득
        InetAddress inetAddress = null;
        Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
        while (networkInterfaces.hasMoreElements()) {
            NetworkInterface ni = (NetworkInterface) networkInterfaces.nextElement();
            Enumeration<InetAddress> nias = ni.getInetAddresses();
            while (nias.hasMoreElements()) {
                InetAddress ia = (InetAddress) nias.nextElement();
                if (!ia.isLinkLocalAddress()
                        && !ia.isLoopbackAddress()
                        && ia instanceof Inet4Address) {
                    inetAddress = ia;
                    break;
                }
            }
        }

        return inetAddress != null ? inetAddress.getHostAddress() : null;
    }

    public static String getMacAddress() throws UnknownHostException, SocketException {
        // IP 취득
        InetAddress ip = InetAddress.getLocalHost();

        // 네트워크 인터페이스 취득
        NetworkInterface networkInterface = NetworkInterface.getByInetAddress(ip);

        // 네트워크 인터페이스가 NULL 이 아니면
        if (networkInterface != null) {
            // 맥 어드레스 취득
            byte[] macBytes = networkInterface.getHardwareAddress();
            String mac = HexBin.encode(macBytes, macBytes.length, 1);
            if (mac != null) {
                mac = mac.replaceAll(" ", ":");
                return mac;
            }
        }

        return null;
    }
}