package net.khwan.commons.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Key;

public class AES {
    private static final String algorithm = "AES";
    private static final String transformation = algorithm + "/ECB/PKCS5Padding";

    private Key key;
    private Charset charset;

    public AES(String key) {
        this(key, StandardCharsets.UTF_8);
    }

    public AES(String key, Charset charset) {
        this.key = new SecretKeySpec(key.getBytes(), algorithm);
        this.charset = charset;
    }

    public AES(SecretKeySpec key) {
        this(key, StandardCharsets.UTF_8);
    }

    public AES(SecretKeySpec key, Charset charset) {
        this.key = key;
        this.charset = charset;
    }

    public AES(Key key) {
        this(key, StandardCharsets.UTF_8);
    }

    public AES(Key key, Charset charset) {
        this.key = key;
        this.charset = charset;
    }

    /**
     * <p>
     * 원본 내용을 암호화한다.
     * </p>
     *
     * @param source 원본 내용
     * @return String
     * @throws Exception Exception
     */
    public String encrypt(String source) throws Exception {
        byte[] encrypt = crypt(Cipher.ENCRYPT_MODE, source.getBytes(charset));
        return Base64.encodeBase64String(encrypt);
    }

    /**
     * <p>
     * 암호화내용을 복호화 한다.
     * </p>
     *
     * @param encryptSource 암호화 내용
     * @return byte[]
     * @throws Exception Exception
     */
    public String decrypt(String encryptSource) throws Exception {
        byte[] decrypt = crypt(Cipher.DECRYPT_MODE, Base64.decodeBase64(encryptSource));
        return new String(decrypt, charset);
    }

    /**
     * <p>
     * 내용을 암/복호화한다
     * </p>
     *
     * @param mode   암/복호화 모드
     * @param source source
     * @return byte[]
     * @throws Exception Exception
     */
    private byte[] crypt(int mode, byte[] source) throws Exception {
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(mode, key);
        return cipher.doFinal(source);
    }

    /**
     * <p>
     * 원본 파일을 암호화해서 대상 파일을 만든다.
     * </p>
     *
     * @param source 원본 파일
     * @param dest   대상 파일
     * @throws Exception Exception
     */
    public void fileEncrypt(File source, File dest) throws Exception {
        fileCrypt(Cipher.ENCRYPT_MODE, source, dest);
    }

    /**
     * <p>
     * 원본 파일을 복호화해서 대상 파일을 만든다.
     * </p>
     *
     * @param source 원본 파일
     * @param dest   대상 파일
     * @throws Exception Exception
     */
    public void fileDecrypt(File source, File dest) throws Exception {
        fileCrypt(Cipher.DECRYPT_MODE, source, dest);
    }

    /**
     * <p>
     * 원본 파일을 암/복호화해서 대상 파일을 만든다.
     * </p>
     *
     * @param mode   암/복호화 모드
     * @param source 원본 파일
     * @param dest   대상 파일
     * @throws Exception Exception
     */
    private void fileCrypt(int mode, File source, File dest) throws Exception {
        Cipher cipher = Cipher.getInstance(transformation);
        cipher.init(mode, key);
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            bis = new BufferedInputStream(new FileInputStream(source));
            bos = new BufferedOutputStream(new FileOutputStream(dest));
            byte[] buffer = new byte[1024];
            int read = -1;
            while ((read = bis.read(buffer)) != -1) {
                bos.write(cipher.update(buffer, 0, read));
            }
            bos.write(cipher.doFinal());
            bos.flush();
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException ie) {
                }
            }
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException ie) {
                }
            }
        }
    }

    /**
     * <p>
     * 문자열을 바이트배열로 바꾼다.
     * </p>
     *
     * @param digits 문자열
     * @param radix  진수
     * @return byte[]
     * @throws IllegalArgumentException IllegalArgumentException
     * @throws NumberFormatException    NumberFormatException
     */
    public static byte[] toBytes(String digits, int radix) throws IllegalArgumentException, NumberFormatException {
        if (digits == null) {
            return null;
        }
        if (radix != 16 && radix != 10 && radix != 8) {
            throw new IllegalArgumentException("For input radix: \"" + radix + "\"");
        }
        int divLen = (radix == 16) ? 2 : 3;
        int length = digits.length();
        if (length % divLen == 1) {
            throw new IllegalArgumentException("For input string: \"" + digits + "\"");
        }
        length = length / divLen;
        byte[] bytes = new byte[length];
        for (int i = 0; i < length; i++) {
            int index = i * divLen;
            bytes[i] = (byte) (Short.parseShort(digits.substring(index, index + divLen), radix));
        }
        return bytes;
    }
}
