package net.khwan.commons.utils;

/**
 * format validation
 * <p>
 * This class encodes/decodes hexadecimal data
 *
 * @author Jeffrey Rodriguez
 */
public final class HexBin {
    private HexBin() {
    }

    static private final int BASELENGTH = 128;
    static private final int LOOKUPLENGTH = 16;
    static final private byte[] hexNumberTable = new byte[BASELENGTH];
    static final private char[] lookUpHexAlphabet = new char[LOOKUPLENGTH];


    static {
        for (int i = 0; i < BASELENGTH; i++) {
            hexNumberTable[i] = -1;
        }
        for (int i = '9'; i >= '0'; i--) {
            hexNumberTable[i] = (byte) (i - '0');
        }
        for (int i = 'F'; i >= 'A'; i--) {
            hexNumberTable[i] = (byte) (i - 'A' + 10);
        }
        for (int i = 'f'; i >= 'a'; i--) {
            hexNumberTable[i] = (byte) (i - 'a' + 10);
        }

        for (int i = 0; i < 10; i++) {
            lookUpHexAlphabet[i] = (char) ('0' + i);
        }
        for (int i = 10; i <= 15; i++) {
            lookUpHexAlphabet[i] = (char) ('A' + i - 10);
        }
    }

    /**
     * Encode a byte array to hex string
     *
     * @param binary array of byte to encode
     * @return return encoded string
     */
    static public String encode(byte[] binary) {
        if (binary == null)
            return null;
        return encode(binary, binary.length);
    }

    static public String encode(byte[] binary, int binaryLength) {
        return encode(binary, binaryLength, 0);
    }

    static public String encode(byte[] binary, int binaryLength, int hexSeparationSize) {
        if (binary == null) return null;
        else if (binary.length < binaryLength) return null;

        if (hexSeparationSize < 0) hexSeparationSize = 0;
        boolean isHexSeparation = (hexSeparationSize > 0);

        int lengthEncode = (binaryLength * 2);

        if (isHexSeparation) {
            lengthEncode += (binaryLength / hexSeparationSize);
            if (binaryLength % hexSeparationSize == 0) {
                lengthEncode--;
            }
        }

        char[] encodedData = new char[lengthEncode];
        int temp;
        int position = 0;
        for (int i = 0; i < binaryLength; i++) {
            temp = binary[i];
            if (temp < 0)
                temp += 256;
            encodedData[position++] = lookUpHexAlphabet[temp >> 4];
            encodedData[position++] = lookUpHexAlphabet[temp & 0xf];

            if (isHexSeparation && position < lengthEncode && ((i + 1) % hexSeparationSize) == 0) {
                encodedData[position++] = ' ';
            }
        }
        return new String(encodedData);
    }

    /**
     * Decode hex string to a byte array
     *
     * @param encoded encoded string
     * @return return array of byte to encode
     */
    static public byte[] decode(String encoded) {
        if (encoded == null)
            return null;
        int lengthData = encoded.length();
        if (lengthData % 2 != 0)
            return null;

        char[] binaryData = encoded.toCharArray();
        int lengthDecode = lengthData / 2;
        byte[] decodedData = new byte[lengthDecode];
        byte temp1, temp2;
        char tempChar;
        for (int i = 0; i < lengthDecode; i++) {
            tempChar = binaryData[i * 2];
            temp1 = (tempChar < BASELENGTH) ? hexNumberTable[tempChar] : -1;
            if (temp1 == -1)
                return null;
            tempChar = binaryData[i * 2 + 1];
            temp2 = (tempChar < BASELENGTH) ? hexNumberTable[tempChar] : -1;
            if (temp2 == -1)
                return null;
            decodedData[i] = (byte) ((temp1 << 4) | temp2);
        }
        return decodedData;
    }
}