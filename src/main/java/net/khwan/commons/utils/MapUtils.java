package net.khwan.commons.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public final class MapUtils {
    private MapUtils() {
    }

    public static int getIntegerValue(Map map, Object key) {
        return getIntegerValue(map, key, 0);
    }

    public static int getIntegerValue(Map map, Object key, int defaultValue) {
        return ConvertUtils.objectToInteger(map.get(key), defaultValue);
    }

    public static long getLongValue(Map map, Object key) {
        return getLongValue(map, key, 0);
    }

    public static long getLongValue(Map map, Object key, long defaultValue) {
        return ConvertUtils.objectToLong(map.get(key), defaultValue);
    }

    public static String getStringValue(Map map, Object key) {
        return getStringValue(map, key, null);
    }

    public static String getStringValue(Map map, Object key, String defaultValue) {
        return ConvertUtils.objectToString(map.get(key), defaultValue);
    }

    public static double getDoubleValue(Map map, Object key) {
        return getDoubleValue(map, key, 0);
    }

    public static double getDoubleValue(Map map, Object key, double defaultValue) {
        return ConvertUtils.objectToDouble(map.get(key), defaultValue);
    }

    public static float getFloatValue(Map map, Object key) {
        return getFloatValue(map, key, 0);
    }

    public static float getFloatValue(Map map, Object key, float defaultValue) {
        return ConvertUtils.objectToFloat(map.get(key), defaultValue);
    }

    public static boolean getBooleanValue(Map map, Object key) {
        return getBooleanValue(map, key, false);
    }

    public static boolean getBooleanValue(Map map, Object key, boolean defaultValue) {
        return ConvertUtils.objectToBoolean(map.get(key), defaultValue);
    }

    public static Map<String, Object> convertObjectToMap(final Object classObject, String... exceptFieldNames) {
        Map<String, Object> map = new HashMap<>();
        final Class<? extends Object> clazz = classObject.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            field.setAccessible(true);
            String fieldName = field.getName();
            Object fieldValue = null;
            if (exceptFieldNames != null) {
                boolean except = false;
                for (String name : exceptFieldNames) {
                    if (fieldName.equals(name)) {
                        except = true;
                        break;
                    }
                }
                if (except) {
                    continue;
                }
            }
            try {
                fieldValue = field.get(classObject);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            map.put(fieldName, fieldValue);
        }

        return map;
    }

//    public static Object convertMapToObject(Map map, Object objClass) {
//        String keyAttribute = null;
//        String setMethodString = "set";
//        String methodString = null;
//        Iterator itr = map.keySet().iterator();
//        while (itr.hasNext()) {
//            keyAttribute = (String) itr.next();
//            methodString = setMethodString + keyAttribute.substring(0, 1).toUpperCase() + keyAttribute.substring(1);
//            try {
//                Method[] methods = objClass.getClass().getDeclaredMethods();
//                for (int i = 0, size = methods.length; i < size; i++) {
//                    if (methodString.equals(methods[i].getName())) {
//                        System.out.println("invoke : " + methodString);
//                        methods[i].invoke(objClass, map.get(keyAttribute));
//                    }
//                }
//            } catch (SecurityException e) {
//                e.printStackTrace();
//            } catch (IllegalAccessException e) {
//                e.printStackTrace();
//            } catch (IllegalArgumentException e) {
//                e.printStackTrace();
//            } catch (InvocationTargetException e) {
//                e.printStackTrace();
//            }
//        }
//        return objClass;
//    }

}
