package net.khwan.commons.utils;

import java.io.UnsupportedEncodingException;
import java.nio.ByteOrder;

public final class ConvertUtils {
    private ConvertUtils() {
    }

    public static final int SHORT_SIZE = 2;
    public static final int INTEGER_SIZE = 4;
    public static final int LONG_SIZE = 8;
    public static final int FLOAT_SIZE = 4;
    public static final int DOUBLE_SIZE = 8;
    public static final int DATETIME_SIZE = 7;
    public static final int TIME_SIZE = 3;

    public static void charArrayCopy(char[] src, char[] dest) {
        int size = dest.length;
        if (size > src.length) size = src.length;
        System.arraycopy(src, 0, dest, 0, size);
    }

    // Value <-> ByteArray 반환
    public static byte[] shortToByteArray(short value, ByteOrder byteOrder) {
        byte[] dest = new byte[SHORT_SIZE];
        byteArrayCopy(value, dest, 0, SHORT_SIZE, byteOrder);
        return dest;
    }

    public static byte[] integerToByteArray(int value, ByteOrder byteOrder) {
        byte[] dest = new byte[INTEGER_SIZE];
        byteArrayCopy(value, dest, 0, INTEGER_SIZE, byteOrder);
        return dest;
    }

    public static byte[] longToByteArray(long value, ByteOrder byteOrder) {
        byte[] dest = new byte[INTEGER_SIZE];
        byteArrayCopy(value, dest, 0, INTEGER_SIZE, byteOrder);
        return dest;
    }

    public static byte[] floatToByteArray(float value, ByteOrder byteOrder) {
        byte[] dest = new byte[FLOAT_SIZE];
        byteArrayCopy(Float.floatToIntBits(value), dest, 0, FLOAT_SIZE, byteOrder);
        return dest;
    }

    public static byte[] doubleToByteArray(double value, ByteOrder byteOrder) {
        byte[] dest = new byte[DOUBLE_SIZE];
        byteArrayCopy(Double.doubleToLongBits(value), dest, 0, DOUBLE_SIZE, byteOrder);
        return dest;
    }

    // Object -> ByteArray 복사
    public static int byteArrayCopy(final short src, final byte[] dest, final int destPos, final ByteOrder byteOrder) {
        return byteArrayCopy(src, dest, destPos, SHORT_SIZE, byteOrder);
    }

    public static int byteArrayCopy(final int src, final byte[] dest, final int destPos, final ByteOrder byteOrder) {
        return byteArrayCopy(src, dest, destPos, INTEGER_SIZE, byteOrder);
    }

    public static int byteArrayCopy(final long src, final byte[] dest, final int destPos, final ByteOrder byteOrder) {
        return byteArrayCopy(src, dest, destPos, LONG_SIZE, byteOrder);
    }

    public static int byteArrayCopy(final float src, final byte[] dest, final int destPos, final ByteOrder byteOrder) {
        return byteArrayCopy(Float.floatToIntBits(src), dest, destPos, FLOAT_SIZE, byteOrder);
    }

    public static int byteArrayCopy(final double src, final byte[] dest, final int destPos, final ByteOrder byteOrder) {
        return byteArrayCopy(Double.doubleToLongBits(src), dest, destPos, DOUBLE_SIZE, byteOrder);
    }

    public static int byteArrayCopy(final char src, final byte[] dest, final int destPos) {
        return byteArrayCopy((byte) src, dest, destPos);
    }

    public static int byteArrayCopy(final byte src, final byte[] dest, final int destPos) {
        dest[destPos] = src;
        return 1;
    }

    public static int byteArrayCopy(final byte[] src, final byte[] dest, final int destPos) {
        return byteArrayCopy(src, 0, dest, destPos, src.length);
    }

    public static int byteArrayCopy(final byte[] src, final byte[] dest, final int destPos, final int length) {
        return byteArrayCopy(src, 0, dest, destPos, length);
    }

    public static int byteArrayCopy(final byte[] src, final int srcPos, final byte[] dest) {
        return byteArrayCopy(src, srcPos, dest, 0, dest.length);
    }

    public static int byteArrayCopy(final byte[] src, final int srcPos, final byte[] dest, final int destPos, final int length) {
        System.arraycopy(src, srcPos, dest, destPos, length);
        return length;
    }

    private static int byteArrayCopy(final long src, final byte[] dest, final int destPos, int length, ByteOrder byteOrder) {
        int i, shift;
        for (i = 0; i < length; i++) {
            if (byteOrder == ByteOrder.BIG_ENDIAN)
                shift = (length - 1 - i) * 8; // 56, 48, 40, 32, 24, 16, 8, 0
            else
                shift = i * 8; // 0, 8, 16, 24, 32, 40, 48, 56

            dest[i + destPos] = (byte) (src >>> shift);
        }
        return length;
    }

    // ByteArray -> Value 반환
    public static short byteArrayToShort(final byte[] src, ByteOrder byteOrder) {
        return (short) byteArrayToValue(src, 0, SHORT_SIZE, byteOrder);
    }

    public static short byteArrayToShort(final byte[] src, final int srcPos, ByteOrder byteOrder) {
        return (short) byteArrayToValue(src, srcPos, SHORT_SIZE, byteOrder);
    }

    public static int byteArrayToInteger(final byte[] src, ByteOrder byteOrder) {
        return (int) byteArrayToValue(src, 0, INTEGER_SIZE, byteOrder);
    }

    public static int byteArrayToInteger(final byte[] src, final int srcPos, ByteOrder byteOrder) {
        return (int) byteArrayToValue(src, srcPos, INTEGER_SIZE, byteOrder);
    }

    public static long byteArrayToLong(final byte[] src, ByteOrder byteOrder) {
        return byteArrayToValue(src, 0, LONG_SIZE, byteOrder);
    }

    public static long byteArrayToLong(final byte[] src, final int srcPos, ByteOrder byteOrder) {
        return byteArrayToValue(src, srcPos, LONG_SIZE, byteOrder);
    }

    public static float byteArrayToFloat(final byte[] src, ByteOrder byteOrder) {
        return Float.intBitsToFloat((int) byteArrayToValue(src, 0, FLOAT_SIZE, byteOrder));
    }

    public static float byteArrayToFloat(final byte[] src, final int srcPos, ByteOrder byteOrder) {
        return Float.intBitsToFloat((int) byteArrayToValue(src, srcPos, FLOAT_SIZE, byteOrder));
    }

    public static double byteArrayToDouble(final byte[] src, ByteOrder byteOrder) {
        return Double.longBitsToDouble(byteArrayToValue(src, 0, DOUBLE_SIZE, byteOrder));
    }

    public static double byteArrayToDouble(final byte[] src, final int srcPos, ByteOrder byteOrder) {
        return Double.longBitsToDouble(byteArrayToValue(src, srcPos, DOUBLE_SIZE, byteOrder));
    }

    public static long byteArrayToValue(final byte[] src, final int srcPos, int length, ByteOrder byteOrder) {
        long result = 0;
        int i, shift;
        for (i = 0; i < length; i++) {
            if (byteOrder == ByteOrder.BIG_ENDIAN)
                shift = (length - 1 - i) * 8; // 56, 48, 40, 32, 24, 16, 8, 0
            else
                shift = i * 8; // 0, 8, 16, 24, 32, 40, 48, 56

            result = ((long) src[i + srcPos] & 0xFF) << shift | result;
        }
        return result;
    }

    // String <-> Byte 변환
    public static String byteArrayToString(byte[] byteArray) {
        try {
            return byteArrayToString(byteArray, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static String byteArrayToString(byte[] byteArray, String charsetName) throws UnsupportedEncodingException {
        for (int i = 0; i < byteArray.length; i++) {
            if (byteArray[i] == 0x00) {
                byteArray = byteArrayCopyOf(byteArray, i);
                break;
            }
        }
        return new String(byteArray, charsetName);
    }

    public static byte[] stringToByteArray(String srcValue, int byteArrayLength) {
        try {
            return stringToByteArray(srcValue, byteArrayLength, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public static byte[] stringToByteArray(String srcValue, int byteArrayLength, String charsetName) throws UnsupportedEncodingException {
        byte[] srcByteArray = srcValue.getBytes(charsetName);

        byte[] destByteArray = new byte[byteArrayLength];

        if (srcByteArray.length < byteArrayLength) {
            byteArrayLength = srcByteArray.length;
        }

        System.arraycopy(srcByteArray, 0, destByteArray, 0, byteArrayLength);

        return destByteArray;
    }

    // Char Array 를 Byte Array 로 반환한다.
    public static byte[] charArrayToByteArray(char[] charArray) {
        int size = charArray.length;
        byte[] byteArray = new byte[size];
        for (int i = 0; i < size; i++) byteArray[i] = (byte) charArray[i];
        return byteArray;
    }

    // Byte Array 길이를 copyLength 만큼 생성, 복사하고 반환
    public static byte[] byteArrayCopyOf(byte[] src, int length) {
        return byteArrayCopyOf(src, 0, length);
    }

    // Byte Array 길이를 copyLength 만큼 생성, 복사하고 반환
    public static byte[] byteArrayCopyOf(byte[] src, int startPos, int length) {
        byte[] dest = new byte[length];
        if (length > src.length) {
            length = src.length;
        }

        System.arraycopy(src, startPos, dest, 0, length);
        return dest;
    }

    // byteArray IpAddress To String
    public static String byteArrayToStringIpAddress(byte[] byteArray) {
        if (byteArray == null || byteArray.length != 4) {
            return null;
        }
        return (byteArray[0] & 0xFF) + "." + (byteArray[1] & 0xFF) + '.' + (byteArray[2] & 0xFF) + '.' + (byteArray[3] & 0xFF);
    }

    public static byte[] stringIpAddressToByteArray(String ipAddress) {
        if (ipAddress == null || ipAddress.length() == 0) return null;

        int dotCount = 0;
        int index = -1;
        while ((index = ipAddress.indexOf(".", index + 1)) >= 0) {
            dotCount++;
        }
        if (dotCount != 3) return null;

        String[] array = ipAddress.split("\\.");
        int length = array.length;
        if (length != 4) return null;

        byte[] values = new byte[4];
        for (int i = 0; i < length; i++) {
            String str = array[i];
            try {
                int value = Integer.parseInt(str);
                if (0 <= value && value <= 255) {
                    values[i] = (byte) value;
                } else {
                    return null;
                }
            } catch (NumberFormatException e) {
                return null;
            }
        }
        return values;
    }

    public static String convertCamelCaseToUnderscore(String camelStr) {
        return camelStr.replaceAll("([A-Z]+)", "\\_$1").toLowerCase();
    }

    public static String convertUnderscoreToCamelCase(String underStr) {
        String[] atoms = underStr.split("_");
        StringBuilder camel = new StringBuilder();
        boolean first = true;
        for (String atom : atoms) {
            int length = atom.length();

            if (first) {
                camel.append(Character.toLowerCase(atom.charAt(0)));
                first = false;
            } else if (length > 0 && !first) {
                camel.append(Character.toUpperCase(atom.charAt(0)));
            }

            if (length > 1) {
                camel.append(atom.substring(1).toLowerCase());
            }
        }
        return camel.toString();
    }

    // Object 변환
    public static int objectToInteger(Object value) {
        return objectToInteger(value, 0);
    }

    public static int objectToInteger(Object value, int defaultValue) {
        if (value == null) {
            return defaultValue;
        } else if (value instanceof Integer) {
            return (int) value;
        } else if (value instanceof String) {
            String strValue = (String) value;
            return (isEmpty(strValue) ? defaultValue : Integer.valueOf(strValue));
        } else if (value instanceof Long) {
            return (int) (long) value;
        } else if (value instanceof Double) {
            return (int) (double) value;
        } else if (value instanceof Float) {
            return (int) (float) value;
        } else if (value instanceof Boolean) {
            return (boolean) value ? 1 : 0;
        } else {
            return defaultValue;
        }
    }

    public static long objectToLong(Object value) {
        return objectToLong(value, 0);
    }

    public static long objectToLong(Object value, long defaultValue) {
        if (value == null) {
            return defaultValue;
        } else if (value instanceof Long) {
            return (long) value;
        } else if (value instanceof Integer) {
            return (long) (int) value;
        } else if (value instanceof String) {
            String strValue = (String) value;
            return (isEmpty(strValue) ? defaultValue : Long.valueOf(strValue));
        } else if (value instanceof Double) {
            return (long) (double) value;
        } else if (value instanceof Float) {
            return (long) (float) value;
        } else if (value instanceof Boolean) {
            return (boolean) value ? 1 : 0;
        } else {
            return defaultValue;
        }
    }

    public static String objectToString(Object value) {
        return objectToString(value, null);
    }

    public static String objectToString(Object value, String defaultValue) {
        if (value == null) {
            return defaultValue;
        } else if (value instanceof String) {
            return (String) value;
        } else if (value instanceof Integer) {
            return String.valueOf((int) value);
        } else if (value instanceof Long) {
            return String.valueOf((long) value);
        } else if (value instanceof Double) {
            return String.valueOf((double) value);
        } else if (value instanceof Float) {
            return String.valueOf((float) value);
        } else if (value instanceof Boolean) {
            return String.valueOf((boolean) value);
        } else {
            return defaultValue;
        }
    }

    public static double objectToDouble(Object value) {
        return objectToDouble(value, 0);
    }

    public static double objectToDouble(Object value, double defaultValue) {
        if (value == null) {
            return defaultValue;
        } else if (value instanceof Double) {
            return (double) value;
        } else if (value instanceof Float) {
            return (double) (float) value;
        } else if (value instanceof String) {
            String strValue = (String) value;
            return (isEmpty(strValue) ? defaultValue : Double.valueOf(strValue));
        } else if (value instanceof Integer) {
            return (double) (int) value;
        } else if (value instanceof Long) {
            return (double) (long) value;
        } else if (value instanceof Boolean) {
            return (boolean) value ? 1 : 0;
        } else {
            return defaultValue;
        }
    }

    public static float objectToFloat(Object value) {
        return objectToFloat(value, 0);

    }

    public static float objectToFloat(Object value, float defaultValue) {
        if (value == null) {
            return defaultValue;
        } else if (value instanceof Float) {
            return (float) value;
        } else if (value instanceof Double) {
            return (float) (double) value;
        } else if (value instanceof String) {
            String strValue = (String) value;
            return (isEmpty(strValue) ? defaultValue : Float.valueOf(strValue));
        } else if (value instanceof Integer) {
            return (float) (int) value;
        } else if (value instanceof Long) {
            return (float) (long) value;
        } else if (value instanceof Boolean) {
            return (boolean) value ? 1 : 0;
        } else {
            return defaultValue;
        }
    }

    public static boolean objectToBoolean(Object value) {
        return objectToBoolean(value, false);
    }

    public static boolean objectToBoolean(Object value, boolean defaultValue) {
        if (value == null) {
            return defaultValue;
        } else if (value instanceof Boolean) {
            return (boolean) value;
        } else if (value instanceof String) {
            String strValue = (String) value;
            return (isEmpty(strValue) ? defaultValue : Boolean.valueOf(strValue));
        } else if (value instanceof Integer) {
            return (((int) value) == 1);
        } else if (value instanceof Long) {
            return (((int) (long) value) == 1);
        } else if (value instanceof Float) {
            return (((int) ((float) value)) == 1);
        } else if (value instanceof Double) {
            return (((int) ((double) value)) == 1);
        } else {
            return defaultValue;
        }
    }

    public static boolean isEmpty(String str) {
        return (str == null || str.length() == 0);
    }
}