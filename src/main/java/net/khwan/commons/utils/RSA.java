package net.khwan.commons.utils;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

/**
 * Created by kkh on 2017-11-21.
 */

public class RSA {
    private static Cipher publicEncryptCipher;
    private static Cipher publicDecryptCipher;
    private static Cipher privateEncryptCipher;
    private static Cipher privateDecryptCipher;

    public static void initPrivateKey(String privateKey) throws Exception {
        initPrivateKey(privateKey, null);
    }

    public static void initPrivateKey(String privateKey, String provider) throws Exception {
        privateEncryptCipher = createCipher(Cipher.ENCRYPT_MODE, true, privateKey, provider);
        privateDecryptCipher = createCipher(Cipher.DECRYPT_MODE, true, privateKey, provider);
    }

    public static void initPublicKey(String publicKey) throws Exception {
        initPublicKey(publicKey, null);
    }

    public static void initPublicKey(String publicKey, String provider) throws Exception {
        publicEncryptCipher = createCipher(Cipher.ENCRYPT_MODE, false, publicKey, provider);
        publicDecryptCipher = createCipher(Cipher.DECRYPT_MODE, false, publicKey, provider);
    }

    public static byte[] privateEncrypt(String input) throws BadPaddingException, IllegalBlockSizeException {
        try {
            return privateEncrypt(input, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] privateEncrypt(String input, String charsetName) throws UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        return privateEncryptCipher.doFinal(input.getBytes(charsetName));
    }

    public static String privateDecrypt(byte[] input) throws BadPaddingException, IllegalBlockSizeException {
        try {
            return privateDecrypt(input, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String privateDecrypt(byte[] input, String charsetName) throws UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        return (new String(privateDecryptCipher.doFinal(input), charsetName));
    }

    public static byte[] publicEncrypt(String input) throws BadPaddingException, IllegalBlockSizeException {
        try {
            return publicEncrypt(input, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] publicEncrypt(String input, String charsetName) throws UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        return publicEncryptCipher.doFinal(input.getBytes(charsetName));
    }

    public static String publicDecrypt(byte[] input) throws BadPaddingException, IllegalBlockSizeException {
        try {
            return publicDecrypt(input, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String publicDecrypt(byte[] input, String charsetName) throws UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        return (new String(publicDecryptCipher.doFinal(input), charsetName));
    }

    public static Cipher createCipher(int mode, boolean isPrivate, String strKey, String provider) throws Exception {
        byte[] keyBytes = isPrivate ? getPrivateKeyOnly(strKey).getBytes(StandardCharsets.UTF_8) : getPublicKeyOnly(strKey).getBytes(StandardCharsets.UTF_8);
        keyBytes = Base64.decodeBase64(keyBytes);

        Key key;
        if (isPrivate) {
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(keyBytes);
            if (provider != null) {
                key = KeyFactory.getInstance("RSA", provider).generatePrivate(privateKeySpec);
            } else {
                key = KeyFactory.getInstance("RSA").generatePrivate(privateKeySpec);
            }
        } else {
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(keyBytes);

            if (provider != null) {
                key = KeyFactory.getInstance("RSA", provider).generatePublic(publicKeySpec);
            } else {
                key = KeyFactory.getInstance("RSA").generatePublic(publicKeySpec);
            }
        }

        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(mode, key);
        return cipher;
    }

    private static String getPublicKeyOnly(String key) {
        return key.replaceAll("-----BEGIN PUBLIC KEY-----", "").replaceAll("-----END PUBLIC KEY-----", "").replaceAll("\n", "").trim();
    }

    private static String getPrivateKeyOnly(String key) {
        return key.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "").replaceAll("-----END RSA PRIVATE KEY-----", "").replaceAll("\n", "").trim();
    }
}