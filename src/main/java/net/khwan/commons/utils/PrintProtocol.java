package net.khwan.commons.utils;

import net.khwan.commons.protocol.ProtocolBytes;

import java.lang.reflect.Field;
import java.nio.ByteOrder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PrintProtocol {
    private PrintProtocol() {
    }

    public static void printClassSetBytesGetBytesMethod(final Class clazz, ByteOrder byteOrder) {
        final Object classObj;
        final String classSimpleName;
        try {
            Class<?> _clazz = Class.forName(clazz.getName());
            classObj = _clazz.newInstance();
            classSimpleName = clazz.getSimpleName();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        final boolean hasInterface;
        if ((classObj instanceof ProtocolBytes)) {
            hasInterface = true;
        } else {
            hasInterface = false;
        }
        Field fieldList[] = clazz.getDeclaredFields();
        StringBuilder setBytes = new StringBuilder();
        StringBuilder getBytes = new StringBuilder();

        StringBuilder constructor = new StringBuilder();

        if (hasInterface) {
            setBytes.append("   int i, byteArrayPosition = startPos;\n");
        } else {
            setBytes.append("   int i, byteArrayPosition = 0;\n");
        }

        int fieldLength = 0;
        Field subDataField = null;
        ClazzInfo subDataFieldClazz = null;
        Field preField = null;
        for (Field field : fieldList) {
            field.setAccessible(true);
            String fieldName = field.getName();
            if (fieldName.equals("byteOrder") || fieldName.equals("bytesLength")) {
                continue;
            }
            String fieldType = field.getType().getSimpleName();
            Object fieldValue = null;
            try {
                fieldValue = field.get(classObj);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            //System.out.println("fieldType : "+fieldType+", fieldName : "+fieldName+", fieldValue : "+fieldValue);

            if (fieldType.equals("boolean")) {
                setBytes.append("   ").append(fieldName).append(" = (byteArray[byteArrayPosition++] != (byte) 0x00);\n");
                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(" ? (byte)0x01 : (byte)0x00, byteArray, byteArrayPosition);\n");
                fieldLength++;
            } else if (fieldType.equals("byte")) {
                setBytes.append("   ").append(fieldName).append(" = byteArray[byteArrayPosition++];\n");

                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition);\n");
                fieldLength++;
            } else if (fieldType.equals("byte[]")) {
                byte[] bytes = ((byte[]) fieldValue);
                if (bytes == null) {
                    subDataField = field;
                    boolean preFieldObjectType = false;
                    try {
                        Object preFieldValue = preField.get(classObj);
                        if (preFieldValue instanceof ProtocolBytes) {
                            preFieldObjectType = true;
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    setBytes.append(String.format("   if (byteArray.length < (%s%s + byteArrayPosition)) {\n", preField.getName(), preFieldObjectType ? ".getValue()" : ""));
                    setBytes.append(String.format("       DeserializationException.newThrow(getClass(), byteArray, %s%s + byteArrayPosition);\n", preField.getName(), preFieldObjectType ? ".getValue()" : ""));
                    setBytes.append("   }\n");
                    setBytes.append("   ").append(fieldName).append(" = new byte[").append(preField.getName())
                            .append(preFieldObjectType ? ".getValue()" : "").append("];\n");
                }
                setBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(byteArray, byteArrayPosition, ").append(fieldName).append(");\n");

                if (bytes == null) {
                    getBytes.append("   if (").append(fieldName).append(" != null) {\n");
                    getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition);\n");
                    getBytes.append("   }\n");
                } else {
                    getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition);\n");
                    fieldLength += ((byte[]) fieldValue).length;
                }
            } else if (fieldType.equals("char")) {
                setBytes.append("   ").append(fieldName).append(" = (char) byteArray[byteArrayPosition++];\n");

                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition);\n");
                fieldLength++;
            } else if (fieldType.equals("char[]")) {
                setBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                setBytes.append("       ").append(fieldName).append("[i] = (char) byteArray[byteArrayPosition++];\n");
                setBytes.append("   }\n");

                getBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append("[i], byteArray, byteArrayPosition);\n");
                getBytes.append("   }\n");
                fieldLength += ((char[]) fieldValue).length;

            } else if (fieldType.equals("short")) {
                setBytes.append("   ").append(fieldName).append(" = ConvertUtils.byteArrayToShort(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("   byteArrayPosition += ConvertUtils.SHORT_SIZE;\n");

                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition, byteOrder);\n");
                fieldLength += ConvertUtils.SHORT_SIZE;
            } else if (fieldType.equals("short[]")) {
                setBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                setBytes.append("       ").append(fieldName).append("[i] = ConvertUtils.byteArrayToShort(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("       byteArrayPosition += ConvertUtils.SHORT_SIZE;\n");
                setBytes.append("   }\n");

                getBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append("[i], byteArray, byteArrayPosition, byteOrder);\n");
                getBytes.append("   }\n");
                fieldLength += ((short[]) fieldValue).length * ConvertUtils.SHORT_SIZE;
            } else if (fieldType.equals("int")) {
                setBytes.append("   ").append(fieldName).append(" = ConvertUtils.byteArrayToInteger(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("   byteArrayPosition += ConvertUtils.INTEGER_SIZE;\n");

                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition, byteOrder);\n");
                fieldLength += ConvertUtils.INTEGER_SIZE;
            } else if (fieldType.equals("int[]")) {
                setBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                setBytes.append("       ").append(fieldName).append("[i] = ConvertUtils.byteArrayToInteger(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("       byteArrayPosition += ConvertUtils.INTEGER_SIZE;\n");
                setBytes.append("   }\n");

                getBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append("[i], byteArray, byteArrayPosition, byteOrder);\n");
                getBytes.append("   }\n");
                fieldLength += ((int[]) fieldValue).length * ConvertUtils.INTEGER_SIZE;
            } else if (fieldType.equals("long")) {
                setBytes.append("   ").append(fieldName).append(" = ConvertUtils.byteArrayToLong(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("   byteArrayPosition += ConvertUtils.LONG_SIZE;\n");

                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition, byteOrder);\n");
                fieldLength += ConvertUtils.LONG_SIZE;
            } else if (fieldType.equals("long[]")) {
                setBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                setBytes.append("       ").append(fieldName).append("[i] = ConvertUtils.byteArrayToLong(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("       byteArrayPosition += ConvertUtils.LONG_SIZE;\n");
                setBytes.append("   }\n");

                getBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append("[i], byteArray, byteArrayPosition, byteOrder);\n");
                getBytes.append("   }\n");
                fieldLength += ((long[]) fieldValue).length * ConvertUtils.LONG_SIZE;
            } else if (fieldType.equals("float")) {
                setBytes.append("   ").append(fieldName).append(" = ConvertUtils.byteArrayToFloat(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("   byteArrayPosition += ConvertUtils.FLOAT_SIZE;\n");

                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition, byteOrder);\n");
                fieldLength += ConvertUtils.FLOAT_SIZE;
            } else if (fieldType.equals("float[]")) {
                setBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                setBytes.append("       ").append(fieldName).append("[i] = ConvertUtils.byteArrayToFloat(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("       byteArrayPosition += ConvertUtils.FLOAT_SIZE;\n");
                setBytes.append("   }\n");

                getBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append("[i], byteArray, byteArrayPosition, byteOrder);\n");
                getBytes.append("   }\n");
                fieldLength += ((float[]) fieldValue).length * ConvertUtils.FLOAT_SIZE;
            } else if (fieldType.equals("double")) {
                setBytes.append("   ").append(fieldName).append(" = ConvertUtils.byteArrayToDouble(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("   byteArrayPosition += ConvertUtils.DOUBLE_SIZE;\n");

                getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(", byteArray, byteArrayPosition, byteOrder);\n");
                fieldLength += ConvertUtils.DOUBLE_SIZE;
            } else if (fieldType.equals("double[]")) {
                setBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                setBytes.append("       ").append(fieldName).append("[i] = ConvertUtils.byteArrayToDouble(byteArray, byteArrayPosition, byteOrder);\n");
                setBytes.append("       byteArrayPosition += ConvertUtils.DOUBLE_SIZE;\n");
                setBytes.append("   }\n");

                getBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append("[i], byteArray, byteArrayPosition, byteOrder);\n");
                getBytes.append("   }\n");
                fieldLength += ((double[]) fieldValue).length * ConvertUtils.DOUBLE_SIZE;
            } else {
                ClazzInfo clazzInfo = clazzNewInstance(field, fieldValue);
                if (clazzInfo == null) {
                    throw new RuntimeException(String.format("fieldName : %s, defaultValue : %s 처리 할 수 없음", fieldName, fieldType));
                } else if (clazzInfo.type.equals("ProtocolBytes")) {
                    setBytes.append("   byteArrayPosition += ").append(fieldName).append(".setBytes(byteArray, byteArrayPosition);\n");

                    getBytes.append("   byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append(".getBytes(), byteArray, byteArrayPosition);\n");

                    fieldLength += clazzInfo.protocolBytes.getBytesLength();
                } else if (clazzInfo.type.equals("ProtocolBytes[]")) {
                    int bytesLength = clazzInfo.protocolBytes.getBytesLength();
                    if (fieldValue == null && subDataField == null) {
                        subDataField = field;
                        subDataFieldClazz = clazzInfo;
                        boolean preFieldObjectType = false;
                        try {
                            Object preFieldValue = preField.get(classObj);
                            if (preFieldValue instanceof ProtocolBytes) {
                                preFieldObjectType = true;
                            }
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                        setBytes.append(String.format("   %s = new %s[%s%s];\n", fieldName, clazzInfo.simpleName, preField.getName(), preFieldObjectType ? ".getValue()" : ""));

                        setBytes.append(String.format("   for (i = 0; i < %s.length; i++) {\n", fieldName));
                        setBytes.append(String.format("       %s[i] = new %s();\n", fieldName, clazzInfo.simpleName));
                        setBytes.append(String.format("       byteArrayPosition += %s[i].setBytes(byteArray, byteArrayPosition);\n", fieldName));
                        setBytes.append(String.format("   }\n", fieldName));


                        getBytes.append(String.format("   if (%s != null) {\n", fieldName));

                        getBytes.append(String.format("       for (i = 0; i < %s.length; i++) {\n", fieldName));
                        getBytes.append(String.format("           byteArrayPosition += ConvertUtils.byteArrayCopy(%s[i].getBytes(), byteArray, byteArrayPosition);\n", fieldName));
                        getBytes.append(String.format("       }\n", fieldName));
                        getBytes.append(String.format("   }\n", fieldName));
                    } else if (fieldValue != null) {
                        setBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                        setBytes.append("       byteArrayPosition += ").append(fieldName).append("[i].setBytes(byteArray, byteArrayPosition);\n");
                        setBytes.append("   }\n");

                        getBytes.append("   for (i = 0; i < ").append(fieldName).append(".length; i++) {\n");
                        getBytes.append("       byteArrayPosition += ConvertUtils.byteArrayCopy(").append(fieldName).append("[i].getBytes(), byteArray, byteArrayPosition);\n");
                        getBytes.append("   }\n");

                        int length = ((ProtocolBytes[]) fieldValue).length;
                        fieldLength += (length * bytesLength);

                        // 생성자 생성
                        if (constructor.length() == 0) {
                            constructor.append("public ").append(classSimpleName).append("() {\n");
                            constructor.append("   int i;\n");
                        }
                        constructor.append("   for (i = 0; i < ").append(length).append("; i++) {\n");
                        constructor.append("       ").append(fieldName).append("[i] = ").append("new ").append(clazzInfo.simpleName).append("();\n");
                        constructor.append("   }\n");
                    }
                } else {
                    throw new ClassCastException("Unknown FieldType " + fieldType);
                }
            }

            preField = field;
        }
        if (hasInterface) {
            setBytes.append("   return byteArrayPosition - startPos;\n");
        }
        setBytes.append("}\n");

        getBytes.insert(0, "   int i, byteArrayPosition = 0;\n");
        getBytes.insert(0, "   byte[] byteArray = new byte[getBytesLength()];\n");
        getBytes.insert(0, "public byte[] getBytes() {\n");
        getBytes.insert(0, "@SuppressWarnings(\"Duplicates\")\n");

        if (hasInterface) {
            getBytes.insert(0, "@Override\n");
        }

        getBytes.append("   return byteArray;\n");
        getBytes.append("}\n");

        if (subDataField == null) {
            if (hasInterface) {
                setBytes.insert(0, "   if (byteArray.length - startPos < bytesLength) {\n       DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());\n   }\n");
                setBytes.insert(0, "public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {\n");
                setBytes.insert(0, "@SuppressWarnings(\"Duplicates\")\n");
                setBytes.insert(0, "@Override\npublic void setBytes(final byte[] byteArray) throws DeserializationException {\n   setBytes(byteArray, 0);\n}\n");
                setBytes.insert(0, "@SuppressWarnings(\"Duplicates\")\n");
            } else {
                setBytes.insert(0, "   if (byteArray.length < bytesLength) {\n       DeserializationException.newThrow(getClass(), byteArray, getBytesLength());\n   }\n");
                setBytes.insert(0, "public void setBytes(final byte[] byteArray) throws DeserializationException {\n");
                setBytes.insert(0, "@SuppressWarnings(\"Duplicates\")\n");
            }

            setBytes.insert(0, "public int getBytesLength() {\n   return bytesLength;\n}\n\n");
            setBytes.insert(0, "public final static int bytesLength = " + fieldLength + ";\n");
        } else {
            if (hasInterface) {
                setBytes.insert(0, "   if (byteArray.length - startPos < getBytesLength()) {\n       DeserializationException.newThrow(getClass(), byteArray, startPos, getBytesLength());\n   }\n");
                setBytes.insert(0, "public int setBytes(final byte[] byteArray, final int startPos) throws DeserializationException {\n");
                setBytes.insert(0, "@SuppressWarnings(\"Duplicates\")\n");
                setBytes.insert(0, "@Override\npublic void setBytes(final byte[] byteArray) throws DeserializationException {\n   setBytes(byteArray, 0);\n}\n");
                setBytes.insert(0, "@SuppressWarnings(\"Duplicates\")\n");
            } else {
                setBytes.insert(0, "   if (byteArray.length < getBytesLength()) {\n       DeserializationException.newThrow(getClass(), byteArray, getBytesLength());\n   }\n");
                setBytes.insert(0, "public void setBytes(final byte[] byteArray) throws DeserializationException {\n");
                setBytes.insert(0, "@SuppressWarnings(\"Duplicates\")\n");
            }
            if (subDataFieldClazz == null) {
                setBytes.insert(0, String.format("public int getBytesLength() {\n   return (%d + ((%s != null) ? %s.length  : 0));\n}\n\n", fieldLength, subDataField.getName(), subDataField.getName()));
            } else {
                setBytes.insert(0, String.format("public int getBytesLength() {\n   return (%d + ((%s != null) ? %s.length * %s.bytesLength : 0));\n}\n\n", fieldLength, subDataField.getName(), subDataField.getName(), subDataFieldClazz.simpleName));
            }
        }
        setBytes.insert(0, "private final ByteOrder byteOrder = ByteOrder." + byteOrder + ";\n");

        if (constructor.length() != 0) {
            constructor.append("}\n");
            System.out.println(constructor.toString());
        }
        String strSetBytes = setBytes.toString();

        Pattern pattern = Pattern.compile("for\\s*\\(i\\s*=\\s*0");
        Matcher matcher = pattern.matcher(strSetBytes);
        if (!matcher.find()) {
            pattern = Pattern.compile("int i, byteArrayPosition");
            matcher = pattern.matcher(strSetBytes);
            if (matcher.find()) {
                strSetBytes = matcher.replaceAll("int byteArrayPosition");
            }
        }
        System.out.println(strSetBytes);

        String strGetBytes = getBytes.toString();
        pattern = Pattern.compile("for\\s*\\(i\\s*=\\s*0");
        matcher = pattern.matcher(strGetBytes);
        if (!matcher.find()) {
            pattern = Pattern.compile("int i, byteArrayPosition");
            matcher = pattern.matcher(strGetBytes);
            if (matcher.find()) {
                strGetBytes = matcher.replaceAll("int byteArrayPosition");
            }
        }
        System.out.println(strGetBytes);
    }

    private static ClazzInfo clazzNewInstance(Field field, Object fieldValue) {
        String clazzCanonicalName = field.getType().getCanonicalName();
        int position = clazzCanonicalName.indexOf("[]");
        boolean isArray = false;
        if (position > 0) {
            isArray = true;
            clazzCanonicalName = clazzCanonicalName.substring(0, position);
        }
        String simpleName = clazzCanonicalName.substring(clazzCanonicalName.lastIndexOf(".") + 1, clazzCanonicalName.length());
        try {
            if (fieldValue != null) {
                ProtocolBytes protocolBytes = null;
                if (fieldValue instanceof ProtocolBytes[]) {
                    protocolBytes = ((ProtocolBytes[]) fieldValue)[0];
                } else if (fieldValue instanceof ProtocolBytes) {
                    protocolBytes = (ProtocolBytes) fieldValue;
                }
                if (protocolBytes != null) {
                    return (new ClazzInfo(fieldValue, protocolBytes, "ProtocolBytes" + (isArray ? "[]" : ""), simpleName));
                }
            } else {
                Class<?> _clazz = Class.forName(clazzCanonicalName);
                Object instance = _clazz.newInstance();
                if (instance instanceof ProtocolBytes) {
                    ProtocolBytes protocolBytes = (ProtocolBytes) instance;
                    return (new ClazzInfo(instance, protocolBytes, "ProtocolBytes" + (isArray ? "[]" : ""), simpleName));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    private static class ClazzInfo {
        public final Object instance;
        public final ProtocolBytes protocolBytes;
        public final String type;
        public final String simpleName;

        public ClazzInfo(Object instance, ProtocolBytes protocolBytes, String type, String simpleName) {
            this.instance = instance;
            this.protocolBytes = protocolBytes;
            this.type = type;
            this.simpleName = simpleName;
        }
    }

    public static void printClassVariableFirstAlphabetLowercase(final Object classObject) {
        final Class<? extends Object> mClass = classObject.getClass();
        Field fieldList[] = mClass.getDeclaredFields();

        StringBuilder sb = new StringBuilder();
        for (Field field : fieldList) {
            field.setAccessible(true);
            String fieldType = field.getType().getSimpleName();
            String fieldName = field.getName();
            Object fieldValue = null;
            try {
                fieldValue = field.get(classObject);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            int fieldArrayLength = -1;
            if (fieldType.equals("byte[]")) {
                fieldArrayLength = ((byte[]) fieldValue).length;
            } else if (fieldType.equals("char[]")) {
                fieldArrayLength = ((char[]) fieldValue).length;
            } else if (fieldType.equals("short[]")) {
                fieldArrayLength = ((short[]) fieldValue).length;
            } else if (fieldType.equals("int[]")) {
                fieldArrayLength = ((int[]) fieldValue).length;
            } else if (fieldType.equals("long[]")) {
                fieldArrayLength = ((long[]) fieldValue).length;
            } else if (fieldType.equals("float[]")) {
                fieldArrayLength = ((float[]) fieldValue).length;
            } else if (fieldType.equals("double[]")) {
                fieldArrayLength = ((double[]) fieldValue).length;
            }

            sb.append("   private " + fieldType + " " + Character.toLowerCase(fieldName.charAt(0)) + fieldName.substring(1, fieldName.length()));
            if (fieldArrayLength != -1) {
                sb.append(" = new " + fieldType.substring(0, fieldType.length() - 2) + "[" + fieldArrayLength + "]");
            }
            sb.append(";\n");
        }

        System.out.println(sb.toString());
    }
}