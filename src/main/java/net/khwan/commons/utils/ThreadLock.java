package net.khwan.commons.utils;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class ThreadLock {
    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private boolean threadLocked = false;

    public boolean isThreadLocked() {
        lock.readLock().lock();
        try {
            return threadLocked;
        } finally {
            lock.readLock().unlock();
        }
    }

    private void setThreadLocked(boolean locked) {
        lock.writeLock().lock();
        threadLocked = locked;
        lock.writeLock().unlock();
    }

    public void lock() throws InterruptedException {
        lock(0);
    }

    public synchronized void lock(long millis) throws InterruptedException {
        if (!isThreadLocked()) {
            setThreadLocked(true);
            if (millis > 0) {
                this.wait(millis);
            } else {
                this.wait();
            }
            setThreadLocked(false);
        }
    }

    public synchronized void unlock() {
        if (isThreadLocked()) {
            try {
                this.notify();
            } finally {
                setThreadLocked(false);
            }
        }
    }
}