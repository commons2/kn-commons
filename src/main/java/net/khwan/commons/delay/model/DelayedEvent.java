package net.khwan.commons.delay.model;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author 김경환
 * @version 1.0
 * @since 2020-01-13
 */
public class DelayedEvent implements Delayed {
    private final LocalDateTime activationDateTime;
    private final Runnable runnable;
    private long taskIndex;

    public DelayedEvent(LocalDateTime activationDateTime, Runnable runnable) {
        this.runnable = runnable;
        this.activationDateTime = activationDateTime;
    }

    public Runnable getRunnable() {
        return runnable;
    }

    public LocalDateTime getActivationDateTime() {
        return activationDateTime;
    }

    public long getTaskIndex() {
        return taskIndex;
    }

    public void setTaskIndex(long taskIndex) {
        this.taskIndex = taskIndex;
    }

    @Override
    public int compareTo(Delayed that) {
        long result = this.getDelay(TimeUnit.NANOSECONDS)
                - that.getDelay(TimeUnit.NANOSECONDS);
        if (result < 0) {
            return -1;
        } else if (result > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        LocalDateTime now = LocalDateTime.now();
        long diff = now.until(activationDateTime, ChronoUnit.MILLIS);
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public String toString() {
        return "DelayedEvent{" +
                "activationDateTime=" + activationDateTime +
                ", runnable=" + runnable +
                '}';
    }
}