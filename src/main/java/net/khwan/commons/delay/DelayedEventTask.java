package net.khwan.commons.delay;

import net.khwan.commons.delay.model.DelayedEvent;
import net.khwan.commons.thread.AbstractThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 김경환
 * @version 1.0
 * @since 2020-02-05
 */
public class DelayedEventTask extends AbstractThread {
    private static final Logger logger = LoggerFactory.getLogger(DelayedEventTask.class);
    private final DelayQueue<DelayedEvent> queue = new DelayQueue<>();
    private final ConcurrentHashMap<Long, DelayedEvent> waitTaskMap = new ConcurrentHashMap<>();
    private ExecutorService singleExecutorService;
    private final WaitEventCheckTask waitEventCheckTask = new WaitEventCheckTask();

    public DelayedEventTask() {
        // default thread 10
        this(10);
    }

    public DelayedEventTask(int nThreads) {
        super(true, nThreads);
    }

    @Override
    public void run() {
        logger.info("DelayedEventTask thread start");
        long taskIndex = 0;
        try {
            while (!Thread.interrupted()) {
                final DelayedEvent delayedEvent = queue.take();
                taskIndex++;
                if (taskIndex >= Long.MAX_VALUE) {
                    taskIndex = 1;
                }
                delayedEvent.setTaskIndex(taskIndex);
                if (logger.isTraceEnabled()) {
                    logger.trace("task '{}' start", delayedEvent.getTaskIndex());
                }
                waitTaskMap.put(taskIndex, delayedEvent);
                executorService.submit(new Runnable() {
                    @Override
                    public void run() {
                        long startNanoTime = System.nanoTime();
                        try {
                            if (delayedEvent.getRunnable() != null) {
                                delayedEvent.getRunnable().run();
                            }
                        } catch (Exception e) {
                            logger.error("Exception", e);
                        }
                        waitTaskMap.remove(delayedEvent.getTaskIndex());
                        long elapsedTimeMillis = getElapsedNanoTimeInMillis(startNanoTime, 0);
                        logger.trace("task '{}' done, elapsedTimeMillis '{}'", delayedEvent.getTaskIndex(), elapsedTimeMillis);
                    }
                });
            }
        } catch (InterruptedException e) {
            logger.error("InterruptedException", e);
        }
        logger.info("DelayedEventTask thread interrupted!");
    }

    public static long getElapsedNanoTimeInMillis(long startNanoTime, long endNanoTime) {
        if (endNanoTime <= 0) endNanoTime = System.nanoTime();

        long elapsedNanoTime = endNanoTime - startNanoTime;
        return elapsedNanoTime / 1000000;
    }

    public void postDelayed(LocalDateTime activationDateTime, Runnable runnable) {
        this.queue.add(new DelayedEvent(activationDateTime, runnable));
    }

    @Override
    public void start() throws RuntimeException {
        synchronized (lock) {
            if (singleExecutorService == null) {
                singleExecutorService = Executors.newSingleThreadExecutor();
                singleExecutorService.execute(waitEventCheckTask);
            } else {
                throw new RuntimeException("Thread already started");
            }
        }
        super.start();
    }

    @Override
    public void stop() {
        synchronized (lock) {
            if (singleExecutorService != null) {
                singleExecutorService.shutdownNow();
                singleExecutorService = null;
            }
        }
        super.stop();
    }

    public final class WaitEventCheckTask implements Runnable {
        @Override
        public void run() {
            logger.info("DelayedEventTask.WaitEventCheckTask thread start");
            try {
                while (!Thread.interrupted()) {
                    if (waitTaskMap.size() > 10) {
                        logger.warn("'{}' events waiting. need to increase event thread size", waitTaskMap.size());
                    }
                    Thread.sleep(10000);
                }
            } catch (Exception e) {
                logger.error("Exception", e);
            }

            logger.info("DelayedEventTask.WaitEventCheckTask thread interrupted!");
        }
    }
}
