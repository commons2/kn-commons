package net.khwan.commons.constant;

public class BitConstant {
    public final static byte BIT0 = (1 << 0);
    public final static byte BIT1 = (1 << 1);
    public final static byte BIT2 = (1 << 2);
    public final static byte BIT3 = (1 << 3);
    public final static byte BIT4 = (1 << 4);
    public final static byte BIT5 = (1 << 5);
    public final static byte BIT6 = (1 << 6);
    public final static byte BIT7 = (byte) (1 << 7);

    public final static short S_BIT0 = (1 << 0);
    public final static short S_BIT1 = (1 << 1);
    public final static short S_BIT2 = (1 << 2);
    public final static short S_BIT3 = (1 << 3);
    public final static short S_BIT4 = (1 << 4);
    public final static short S_BIT5 = (1 << 5);
    public final static short S_BIT6 = (1 << 6);
    public final static short S_BIT7 = (1 << 7);
    public final static short S_BIT8 = (1 << 8);
    public final static short S_BIT9 = (1 << 9);
    public final static short S_BIT10 = (1 << 10);
    public final static short S_BIT11 = (1 << 11);
    public final static short S_BIT12 = (1 << 12);
    public final static short S_BIT13 = (1 << 13);
    public final static short S_BIT14 = (1 << 14);
    public final static short S_BIT15 = (short)(1 << 15);

    public final static int I_BIT0 = (1 << 0);
    public final static int I_BIT1 = (1 << 1);
    public final static int I_BIT2 = (1 << 2);
    public final static int I_BIT3 = (1 << 3);
    public final static int I_BIT4 = (1 << 4);
    public final static int I_BIT5 = (1 << 5);
    public final static int I_BIT6 = (1 << 6);
    public final static int I_BIT7 = (1 << 7);
    public final static int I_BIT8 = (1 << 8);
    public final static int I_BIT9 = (1 << 9);
    public final static int I_BIT10 = (1 << 10);
    public final static int I_BIT11 = (1 << 11);
    public final static int I_BIT12 = (1 << 12);
    public final static int I_BIT13 = (1 << 13);
    public final static int I_BIT14 = (1 << 14);
    public final static int I_BIT15 = (1 << 15);
    public final static int I_BIT16 = (1 << 16);
    public final static int I_BIT17 = (1 << 17);
    public final static int I_BIT18 = (1 << 18);
    public final static int I_BIT19 = (1 << 19);
    public final static int I_BIT20 = (1 << 20);
    public final static int I_BIT21 = (1 << 21);
    public final static int I_BIT22 = (1 << 22);
    public final static int I_BIT23 = (1 << 23);

    public final static int I_BIT24 = (1 << 24);
    public final static int I_BIT25 = (1 << 25);
    public final static int I_BIT26 = (1 << 26);
    public final static int I_BIT27 = (1 << 27);
    public final static int I_BIT28 = (1 << 28);
    public final static int I_BIT29 = (1 << 29);
    public final static int I_BIT30 = (1 << 30);
    public final static int I_BIT31 = (1 << 31);
}
