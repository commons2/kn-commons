package net.khwan.commons.jackson;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;

public class JacksonXml {
    public JacksonXmlModule jacksonXmlModule;

    public XmlMapper xmlMapper;

    public JacksonXml() {
        this(new JacksonXmlModule() {{
            setDefaultUseWrapper(false);
        }});
    }

    public JacksonXml(JacksonXmlModule jacksonXmlModule) {
        this(jacksonXmlModule, new XmlMapper(jacksonXmlModule) {{
            configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
            enable(SerializationFeature.INDENT_OUTPUT);
        }});
    }

    public JacksonXml(JacksonXmlModule jacksonXmlModule, XmlMapper xmlMapper) {
        this.jacksonXmlModule = jacksonXmlModule;
        this.xmlMapper = xmlMapper;
    }

    public String toXml(Object value) {
        try {
            return xmlMapper.writeValueAsString(value);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public <T> T fromXml(String content, Class<T> valueType) {
        try {
            return xmlMapper.readValue(content, valueType);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public <T> T fromXml(String content, TypeReference<T> valueTypeRef) {
        try {
            return xmlMapper.readValue(content, valueTypeRef);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
