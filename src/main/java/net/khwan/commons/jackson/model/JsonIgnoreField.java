package net.khwan.commons.jackson.model;

/**
 * @author 김경환
 * @version 1.0
 * @since 2019-10-23
 */

import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

public abstract class JsonIgnoreField {
    protected Map<String, Object> ignoreFields;

    public Map<String, Object> getIgnoreFields() {
        return ignoreFields;
    }

    @JsonAnySetter
    public void ignored(String name, Object value) {
        if (this.ignoreFields == null) {
            this.ignoreFields = new HashMap<>();
        }
        this.ignoreFields.put(name, value);
    }
}
