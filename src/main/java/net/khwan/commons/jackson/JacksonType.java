package net.khwan.commons.jackson;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.List;
import java.util.Map;

/**
 * @author 김경환
 * @version 1.0
 * @since 2020-08-04
 */
public class JacksonType {
    public static final TypeReference<Map<Object, Object>> mapObjectObject = new TypeReference<Map<Object, Object>>() {
    };
    public static final TypeReference<Map<String, Object>> mapStringObject = new TypeReference<Map<String, Object>>() {
    };
    public static final TypeReference<Map<String, String>> mapStringString = new TypeReference<Map<String, String>>() {
    };

    public static final TypeReference<List<Map<Object, Object>>> listMapObjectObject = new TypeReference<List<Map<Object, Object>>>() {
    };
    public static final TypeReference<List<Map<String, Object>>> listMapStringObject = new TypeReference<List<Map<String, Object>>>() {
    };
    public static final TypeReference<List<Map<String, String>>> listMapStringString = new TypeReference<List<Map<String, String>>>() {
    };

    public static final TypeReference<List<Object>> listObject = new TypeReference<List<Object>>() {
    };
    public static final TypeReference<List<Integer>> listInteger = new TypeReference<List<Integer>>() {
    };
    public static final TypeReference<List<Double>> listDouble = new TypeReference<List<Double>>() {
    };
    public static final TypeReference<List<Float>> listFloat = new TypeReference<List<Float>>() {
    };
    public static final TypeReference<List<String>> listString = new TypeReference<List<String>>() {
    };

}
