package net.khwan.commons.jackson;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.javaprop.JavaPropsMapper;

public class JacksonProperties {
    public JavaPropsMapper propsMapper;

    public JacksonProperties() {
        this(new JavaPropsMapper());
    }

    public JacksonProperties(JavaPropsMapper propsMapper) {
        this.propsMapper = propsMapper;
    }

    public String toProps(Object value) {
        try {
            return propsMapper.writeValueAsString(value);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public <T> T fromProps(String content, Class<T> valueType) {
        try {
            return propsMapper.readValue(content, valueType);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public <T> T fromProps(String content, TypeReference<T> valueTypeRef) {
        try {
            return propsMapper.readValue(content, valueTypeRef);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
