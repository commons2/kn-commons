package net.khwan.commons.http.model;

import net.khwan.commons.file.FileIOCommon;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.List;

public class HttpResponse {
    private final String charsetName;
    private final int responseCode;
    private final URLConnection conn;
    private String content;
    private final String contentType;
    private final long contentLength;
    private final boolean isHttps;
    private boolean disconnected = false;

    public HttpResponse(URLConnection conn, String charsetName) throws IOException {
        this.conn = conn;
        this.charsetName = charsetName;
        this.isHttps = "https".equals(conn.getURL().getProtocol());
        this.contentLength = conn.getContentLength();
        this.contentType = conn.getContentType();

        if (isHttps) {
            HttpsURLConnection _conn = ((HttpsURLConnection) conn);
            this.responseCode = _conn.getResponseCode();
        } else {
            HttpURLConnection _conn = ((HttpURLConnection) conn);
            this.responseCode = _conn.getResponseCode();
        }
    }

    public String getCookie() {
        if (conn.getHeaderFields() != null && conn.getHeaderFields().containsKey("Set-Cookie")) {
            List<String> cookieList = conn.getHeaderFields().get("Set-Cookie");
            if (cookieList != null) {
                String cookie = null;
                for (String _cookie : cookieList) {
                    int index = _cookie.indexOf(";");
                    if (index > 0) {
                        cookie = _cookie.substring(0, index);
                    }
                    return cookie;
                }
            }
        }
        return null;
    }

    public interface Listener {
        void progress(int length);
    }

    public void saveFile(String path, final Listener listener) throws IOException {
        saveFile(path, 8192, listener);
    }

    public void saveFile(String path, int bufferSize, final Listener listener) throws IOException {
        FileIOCommon.Listener _listener = null;
        if (listener != null) {
            _listener = new FileIOCommon.Listener() {
                @Override
                public void progress(int length) {
                    listener.progress(length);
                }
            };
        }
        try {
            FileIOCommon.copy(conn.getInputStream(), new FileOutputStream(path), bufferSize, _listener);
        } finally {
            close();
        }
    }

    public void close() {
        if (!disconnected) {
            if (conn != null) {
                if (isHttps) {
                    ((HttpsURLConnection) conn).disconnect();
                } else {
                    ((HttpURLConnection) conn).disconnect();
                }
            }
            disconnected = true;
        }
    }

    public String getContent() throws IOException {
        if (content == null) {
            try {
                if (responseCode >= 200 && responseCode < 300) {
                    content = readAllString(conn.getInputStream(), charsetName);
                } else {
                    if (isHttps) {
                        content = readAllString(((HttpsURLConnection) conn).getErrorStream(), charsetName);
                    } else {
                        content = readAllString(((HttpURLConnection) conn).getErrorStream(), charsetName);
                    }
                }
            } finally {
                close();
            }
        }
        return content;
    }

    private String readAllString(InputStream in, String charsetName) throws IOException {
        byte[] buffer = new byte[8192];
        BufferedInputStream bis = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            bis = new BufferedInputStream(in);
            int size;
            while ((size = bis.read(buffer)) != -1) {
                bos.write(buffer, 0, size);
            }

        } finally {
            if (bis != null) {
                try {
                    bis.close();
                    bis = null;
                } catch (IOException e) {
                }
            }

            try {
                buffer = bos.toByteArray();
                bos.close();
                bos = null;
            } catch (IOException e) {
            }
        }
        return (new String(buffer, charsetName));
    }

    public String getJsonContent() throws IOException {
        if (content == null) getContent();
        if (content != null) {
            content = content.trim().replaceFirst("\ufeff", "");
        }
        return content;
    }

    public String getCharsetName() {
        return charsetName;
    }

    public URLConnection getConn() {
        return conn;
    }

    public String getContentType() {
        return contentType;
    }

    public long getContentLength() {
        return contentLength;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public boolean isHttps() {
        return isHttps;
    }

    public boolean isDisconnected() {
        return disconnected;
    }

    @Override
    public String toString() {
        if (!disconnected) {
            try {
                getContent();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return "HttpResponse{" +
                "charsetName='" + charsetName + '\'' +
                ", responseCode=" + responseCode +
                ", conn=" + conn +
                ", content='" + content + '\'' +
                ", contentType='" + contentType + '\'' +
                ", contentLength=" + contentLength +
                ", isHttps=" + isHttps +
                ", disconnected=" + disconnected +
                '}';
    }
}
