package net.khwan.commons.http.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URLConnection;

/**
 * @author 김경환
 * @version 1.0
 * @since 2019-06-21
 */
public class HttpMultipartFile {
    private final String fieldName;
    private final File file;
    private final InputStream inputStream;
    private final String fileName;
    private final String contentType;


    public HttpMultipartFile(String fieldName, File file, InputStream inputStream, String fileName, String contentType) {
        this.fieldName = fieldName;
        this.inputStream = inputStream;
        this.fileName = fileName;
        this.contentType = contentType;
        this.file = file;
    }

    public HttpMultipartFile(String fieldName, File file) throws FileNotFoundException {
        this(fieldName, file, file.getName());
    }

    public HttpMultipartFile(String fieldName, File file, String fileName) throws FileNotFoundException {
        this(fieldName, file, new FileInputStream(file), fileName, URLConnection.guessContentTypeFromName(fileName));
    }

    public HttpMultipartFile(String fieldName, InputStream inputStream, String fileName) {
        this(fieldName, null, inputStream, fileName, URLConnection.guessContentTypeFromName(fileName));
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public File getFile() {
        return file;
    }

    @Override
    public String toString() {
        return "HttpMultipartFile{" +
                "fieldName='" + fieldName + '\'' +
                ", inputStream=" + inputStream +
                ", fileName='" + fileName + '\'' +
                ", contentType='" + contentType + '\'' +
                '}';
    }
}
