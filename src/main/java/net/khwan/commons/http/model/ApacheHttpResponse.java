package net.khwan.commons.http.model;

import net.khwan.commons.file.FileIOCommon;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.util.EntityUtils;

import java.io.FileOutputStream;
import java.io.IOException;

public class ApacheHttpResponse {
    private final String charsetName;
    private final int responseCode;
    private final HttpResponse response;
    private String content;
    private final String contentType;
    private final long contentLength;
    private boolean disconnected = false;

    public ApacheHttpResponse(HttpResponse response, String charsetName) {
        this.responseCode = response.getStatusLine().getStatusCode();
        this.response = response;
        if (response.getEntity() != null) {
            this.contentLength = response.getEntity().getContentLength();
        } else {
            this.contentLength = 0;
        }
        this.contentType = getContentType(response);
        this.charsetName = charsetName;
    }

    private String getContentType(HttpResponse response) {
        Header[] headers = response.getHeaders("Content-Type");

        if (headers != null) {
            if (headers.length > 0) {
                Header header = headers[0];
                return header.getValue();
            }
        }
        return null;
    }

    public String getCookie() {
        Header[] headers = response.getHeaders("Set-Cookie");
        if (headers != null) {
            if (headers.length > 0) {
                Header header = headers[0];
                String temp = header.getValue();
                int index = temp.indexOf(";");
                if (index > 0) {
                    return temp.substring(0, index);
                }
            }
        }
        return null;
    }

    public interface Listener {
        void progress(int length);
    }

    public void saveFile(String path, final Listener listener) throws IOException {
        saveFile(path, 8192, listener);
    }

    public void saveFile(String path, int bufferSize, final Listener listener) throws IOException {
        FileIOCommon.Listener _listener = null;
        if (listener != null) {
            _listener = new FileIOCommon.Listener() {
                @Override
                public void progress(int length) {
                    listener.progress(length);
                }
            };
        }
        try {
            FileIOCommon.copy(response.getEntity().getContent(), new FileOutputStream(path), bufferSize, _listener);
        } finally {
            close();
        }
    }

    public void close() {
        HttpClientUtils.closeQuietly(response);
        disconnected = true;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public HttpResponse getResponse() {
        return response;
    }

    public String getContent() throws IOException, ParseException {
        if (content == null) {
            try {
                content = EntityUtils.toString(response.getEntity(), charsetName);
            } finally {
                close();
            }
        }
        return content;
    }

    public String getJsonContent() throws IOException, ParseException {
        if (content == null) getContent();
        if (content != null) {
            content = content.trim().replaceFirst("\ufeff", "");
        }
        return content;
    }

    public String getContentType() {
        return contentType;
    }

    public long getContentLength() {
        return contentLength;
    }

    public boolean isDisconnected() {
        return disconnected;
    }

    @Override
    public String toString() {
        if (!disconnected) {
            try {
                getContent();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return "ApacheHttpResponse{" +
                "charsetName='" + charsetName + '\'' +
                ", responseCode=" + responseCode +
                ", response=" + response +
                ", content='" + content + '\'' +
                ", contentType='" + contentType + '\'' +
                ", contentLength=" + contentLength +
                ", disconnected=" + disconnected +
                '}';
    }
}
