package net.khwan.commons.http.payload;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.HashMap;
import java.util.Map;

public class ApiResult {
    private boolean result;
    private Integer code;
    private String message;

    private Map<String, Object> data;

    private ApiResult() {

    }

    private ApiResult(Builder builder) {
        setResult(builder.result);
        setCode(builder.code);
        setMessage(builder.message);
        setData(builder.data);
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    @JsonAnySetter
    public void ignored(String name, Object value) {
        if (this.data == null) {
            this.data = new HashMap<>();
        }
        this.data.put(name, value);
    }

    @JsonValue
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("result", result);
        if (message != null) map.put("message", message);
        if (code != null) map.put("code", code);
        if (data != null) {
            for (String key : data.keySet()) {
                if (map.containsKey(key)) {
                    System.err.println("ApiResult Duplicates Key(" + key + ")");
                }
                map.put(key, data.get(key));
            }
        }
        return map;
    }

    @Override
    public String toString() {
        return "ApiResult{" +
                "result=" + result +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private boolean result;
        private Integer code;
        private String message;
        private Map<String, Object> data = new HashMap<>();

        public Builder() {
        }

        public Builder result(boolean val) {
            result = val;
            return this;
        }

        public Builder code(Integer val) {
            code = val;
            return this;
        }

        public Builder message(String val) {
            message = val;
            return this;
        }

        public Builder data(String dataName, Object data) {
            this.data.put(dataName, data);
            return this;
        }

        public ApiResult build() {
            return new ApiResult(this);
        }
    }
}
