package net.khwan.commons.http.payload;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ApiErrorResponse {
    private Map<String, Object> data;

    protected ApiErrorResponse() {
        data = new HashMap<>();
    }

    protected ApiErrorResponse(Builder builder) {
        data = builder.data;
    }

    public Date getTimestamp() {
        Object val = data.get("timestamp");
        if (val instanceof Date) return (Date) val;
        else return null;
    }

    public void setTimestamp(Date timestamp) {
        this.data.put("timestamp", timestamp);
    }

    public int getStatus() {
        Object val = data.get("status");
        if (val instanceof Integer) return (int) val;
        else return 0;
    }

    public void setStatus(int status) {
        this.data.put("status", status);
    }

    public String getError() {
        Object val = data.get("error");
        if (val instanceof String) return (String) val;
        else return null;
    }

    public void setError(String error) {
        this.data.put("error", error);
    }

    public String getMessage() {
        Object val = data.get("message");
        if (val instanceof String) return (String) val;
        else return null;
    }

    public void setMessage(String message) {
        this.data.put("message", message);
    }

    public String getPath() {
        Object val = data.get("path");
        if (val instanceof String) return (String) val;
        else return null;
    }

    public void setPath(String path) {
        this.data.put("path", path);
    }

    public Map<String, Object> getData() {
        return data;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonAnySetter
    public void ignored(String name, Object value) {
        if (this.data == null) {
            this.data = new HashMap<>();
        }
        this.data.put(name, value);
    }

    @JsonValue
    public Map<String, Object> toMap() {
        return data;
    }

    @Override
    public String toString() {
        return "ApiErrorResponse{" + data + '}';
    }

    public static final class Builder {
        private Map<String, Object> data;

        private Builder() {
            this.data = new HashMap<>();
        }

        public Builder timestamp(Date val) {
            this.data.put("timestamp", val);
            return this;
        }

        public Builder status(int val) {
            this.data.put("status", val);
            return this;
        }

        public Builder error(String val) {
            this.data.put("error", val);
            return this;
        }

        public Builder message(String val) {
            this.data.put("message", val);
            return this;
        }

        public Builder path(String val) {
            this.data.put("path", val);
            return this;
        }

        public Builder data(String dataName, Object data) {
            if (this.data.containsKey(dataName)) {
                throw new IllegalArgumentException("Duplicate key exists(" + dataName + ")");
            } else if (data != null) {
                this.data.put(dataName, data);
            }
            return this;
        }

        public ApiErrorResponse build() {
            return new ApiErrorResponse(this);
        }
    }
}
