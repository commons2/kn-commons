package net.khwan.commons.http.payload;

import com.fasterxml.jackson.annotation.JsonAnySetter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

public class TableRequest {
    @NotNull
    @Min(0)
    private Long start;
    @NotNull
    @Min(0)
    private Long length;
    @NotBlank
    private String orderColumnName;
    @NotBlank
    private String order;

    private int draw;

    private Map<String, String> searchColumns;

    private Map<String, Object> ignoreData;

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public String getOrderColumnName() {
        return orderColumnName;
    }

    public void setOrderColumnName(String orderColumnName) {
        this.orderColumnName = orderColumnName;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public Map<String, String> getSearchColumns() {
        return searchColumns;
    }

    public void setSearchColumns(Map<String, String> searchColumns) {
        this.searchColumns = searchColumns;
    }

    public Map<String, Object> getIgnoreData() {
        return ignoreData;
    }

    public void setIgnoreData(Map<String, Object> ignoreData) {
        this.ignoreData = ignoreData;
    }

    @JsonAnySetter
    public void ignored(String name, Object value) {
        if (this.ignoreData == null) {
            this.ignoreData = new HashMap<>();
        }
        this.ignoreData.put(name, value);
    }

    public Map<String, String> toParams() {
        Map<String, String> param = new HashMap<>();
        param.put("start", String.valueOf(start));
        param.put("length", String.valueOf(length));
        param.put("orderColumnName", String.valueOf(orderColumnName));
        param.put("order", String.valueOf(order));
        param.put("draw", String.valueOf(draw));
        if (searchColumns != null) {
            for (String key : searchColumns.keySet()) {
                param.put("searchColumns[" + key + "]", searchColumns.get(key));
            }
        }

        return param;
    }

    public static Map<String, String> convertSearchDate(Map<String, String> searchColumns) {
        if (searchColumns.containsKey("searchDate")) {
            String date = searchColumns.get("searchDate");
            if (date != null) {
                String[] dateSplit = date.split("/");
                if (dateSplit.length == 2) {
                    String startDate = dateSplit[0].trim() + " 00:00:00";
                    String endDate = dateSplit[1].trim() + " 23:59:59";
                    searchColumns.put("startDate", startDate);
                    searchColumns.put("endDate", endDate);
                }
            }
            searchColumns.remove("searchDate");
        }
        return searchColumns;
    }

    @Override
    public String toString() {
        return "TableRequest{" +
                "start=" + start +
                ", length=" + length +
                ", orderColumnName='" + orderColumnName + '\'' +
                ", order='" + order + '\'' +
                ", draw=" + draw +
                ", searchColumns=" + searchColumns +
                ", ignoreData=" + ignoreData +
                '}';
    }
}
