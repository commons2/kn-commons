package net.khwan.commons.http.payload;

import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TableResponse<T> {
    private int draw;
    private long recordsTotal;
    private long recordsFiltered;
    private List<T> data;

    private Map<String, Object> ignoreData;

    public TableResponse() {
    }

    public TableResponse(int draw, long recordsTotal, long recordsFiltered, List<T> data) {
        this.draw = draw;
        this.recordsTotal = recordsTotal;
        this.recordsFiltered = recordsFiltered;
        this.data = data;
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public Map<String, Object> getIgnoreData() {
        return ignoreData;
    }

    public void setIgnoreData(Map<String, Object> ignoreData) {
        this.ignoreData = ignoreData;
    }

    @JsonAnySetter
    public void ignored(String name, Object value) {
        if (this.ignoreData == null) {
            this.ignoreData = new HashMap<>();
        }
        this.ignoreData.put(name, value);
    }

    @Override
    public String toString() {
        return "TableResponse{" +
                "draw=" + draw +
                ", recordsTotal=" + recordsTotal +
                ", recordsFiltered=" + recordsFiltered +
                ", data=" + data +
                ", ignoreData=" + ignoreData +
                '}';
    }
}
