package net.khwan.commons.http;

import net.khwan.commons.http.model.*;

import javax.net.ssl.*;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.cert.X509Certificate;
import java.util.Map;

public abstract class AbstractHttpClient {
    protected SSLSocketFactory sslSocketFactory = null;
    protected String charsetName = "UTF-8";
    protected int connectTimeout;
    protected final HttpRequestHeader jsonHeader = new HttpRequestHeader();
    protected final HttpRequestHeader textHtmlHeader = new HttpRequestHeader();

    protected static final String LINE_FEED = "\r\n";

    public enum MethodType {
        GET("GET"),
        POST("POST"),
        PUT("PUT"),
        PATCH("PATCH"),
        DELETE("DELETE"),
        POST_MULTIPART("POST"),
        PUT_MULTIPART("PUT");

        private final String name;

        MethodType(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public AbstractHttpClient() {
        this(3000, "UTF-8");
    }

    public AbstractHttpClient(int connectTimeout) {
        this(connectTimeout, "UTF-8");
    }

    public AbstractHttpClient(String charsetName) {
        this(3000, charsetName);
    }

    public AbstractHttpClient(int connectTimeout, String charsetName) {
        this.charsetName = charsetName;
        this.connectTimeout = connectTimeout;

        textHtmlHeader.put("Cache-Control", "no-cache");
        textHtmlHeader.put("Content-Type", "text/html");
        textHtmlHeader.put("Accept", "text/html");

        jsonHeader.put("Cache-Control", "no-cache");
        jsonHeader.put("Content-Type", "application/json");
        jsonHeader.put("Accept", "application/json");
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse get(String url) throws IOException {
        return get(url, jsonHeader);
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url      http 요청 url
     * @param urlParam http 요청 url 파라미터, ex) url?key=value
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse get(String url, HttpRequestUrlParam urlParam) throws IOException {
        return get(url, jsonHeader, urlParam);
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse get(String url, HttpRequestHeader header) throws IOException {
        return get(url, header, null);
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url      http 요청 url
     * @param header   http 요청 헤더
     * @param urlParam http 요청 url 파라미터, ex) url?key=value
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse get(String url, HttpRequestHeader header, HttpRequestUrlParam urlParam) throws IOException {
        String strUrlParam = getParameter(urlParam);
        if (strUrlParam != null) {
            url = url + "?" + strUrlParam;
        }
        return request(MethodType.GET, url, header);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url) throws IOException {
        return post(url, (String) null);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, HttpRequestBody body) throws IOException {
        return post(url, null, body);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        return request(MethodType.POST_MULTIPART, url, null, body, multipartFiles);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, HttpMultipartFile... multipartFiles) throws IOException {
        return request(MethodType.POST_MULTIPART, url, null, null, multipartFiles);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, String body) throws IOException {
        return post(url, null, body);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, HttpRequestHeader header) throws IOException {
        return post(url, header, (String) null);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        return request(MethodType.POST, url, header, body);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param header         http 요청 헤더
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, HttpRequestHeader header, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        return request(MethodType.POST_MULTIPART, url, header, body, multipartFiles);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse post(String url, HttpRequestHeader header, String body) throws IOException {
        return request(MethodType.POST, url, header, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url) throws IOException {
        return put(url, (String) null);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url, HttpRequestBody body) throws IOException {
        return put(url, null, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        return request(MethodType.PUT_MULTIPART, url, null, body, multipartFiles);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url, String body) throws IOException {
        return put(url, null, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url, HttpRequestHeader header) throws IOException {
        return put(url, header, (String) null);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        return request(MethodType.PUT, url, header, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param header         http 요청 헤더
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url, HttpRequestHeader header, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        return request(MethodType.PUT_MULTIPART, url, header, body, multipartFiles);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse put(String url, HttpRequestHeader header, String body) throws IOException {
        return request(MethodType.PUT, url, header, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse delete(String url) throws IOException {
        return delete(url, (String) null);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse delete(String url, HttpRequestBody body) throws IOException {
        return delete(url, null, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse delete(String url, String body) throws IOException {
        return delete(url, null, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse delete(String url, HttpRequestHeader header) throws IOException {
        return delete(url, header, (String) null);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse delete(String url, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        return request(MethodType.DELETE, url, header, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public HttpResponse delete(String url, HttpRequestHeader header, String body) throws IOException {
        return request(MethodType.DELETE, url, header, body);
    }

    public HttpResponse request(MethodType methodType, String strUrl, HttpRequestHeader header) throws IOException {
        return request(methodType, strUrl, header, null);
    }

    public HttpResponse request(MethodType methodType, String strUrl, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        return request(methodType, strUrl, header, getParameter(body));
    }

    public HttpResponse request(MethodType methodType, String strUrl, HttpRequestHeader header, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        return request(methodType, strUrl, header, (Object) body, multipartFiles);
    }

    private HttpResponse request(MethodType methodType, String strUrl, HttpRequestHeader header, Object objBody, HttpMultipartFile... multipartFiles) throws IOException {
        if (methodType == null) {
            throw new NullPointerException("The method defaultValue can not be null.");
        }

        URL url = new URL(strUrl);

        boolean isHttps = isUrlHttps(url);
        if (isHttps && sslSocketFactory == null) {
            createSslSessionFactory();
        }

        URLConnection conn = url.openConnection();
        if (isHttps) {
            HttpsURLConnection _conn = (HttpsURLConnection) conn;
            _conn.setSSLSocketFactory(sslSocketFactory);
            // add request header
            _conn.setRequestMethod(methodType.getName());

        } else {
            HttpURLConnection _conn = (HttpURLConnection) conn;
            // add request header
            _conn.setRequestMethod(methodType.getName());
        }

        conn.setConnectTimeout(connectTimeout);

        String boundary = null;
        if (methodType == MethodType.POST_MULTIPART || methodType == MethodType.PUT_MULTIPART) {
            conn.setUseCaches(false);
            boundary = "----" + System.currentTimeMillis();
            conn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            conn.setRequestProperty("Connection", "keep-alive");
        }

        if (header != null) {
            for (String key : header.keySet()) {
                conn.setRequestProperty(key, header.get(key));
            }
        }

        if (objBody != null) {
            conn.setDoOutput(true);

            if (methodType == MethodType.POST_MULTIPART || methodType == MethodType.PUT_MULTIPART) {
                StringBuilder sb = new StringBuilder();
                if (objBody instanceof HttpRequestBody) {
                    HttpRequestBody body = (HttpRequestBody) objBody;
                    for (String key : body.keySet()) {
                        String value = body.get(key);

                        sb.append("--").append(boundary).append(LINE_FEED);
                        sb.append("Content-Disposition: form-data; name=\"").append(key).append("\"").append(LINE_FEED);
                        sb.append(LINE_FEED);
                        sb.append(value).append(LINE_FEED);
                    }
                }

                DataOutputStream dataOutputStream = new DataOutputStream(conn.getOutputStream());
                if (sb.length() > 0) {
                    dataOutputStream.write(sb.toString().getBytes(charsetName));
                    dataOutputStream.flush();
                    sb.setLength(0);
                }

                if (multipartFiles != null) {
                    byte[] buffer = new byte[8192];
                    int bytesRead;
                    for (HttpMultipartFile multipartFile : multipartFiles) {
                        if (multipartFile != null) {
                            sb.setLength(0);
                            sb.append("--").append(boundary).append(LINE_FEED);
                            sb.append("Content-Disposition: form-data; name=\"").append(multipartFile.getFieldName())
                                    .append("\"; filename=\"").append(multipartFile.getFileName()).append("\"").append(LINE_FEED);
                            sb.append("Content-Type: ").append(multipartFile.getContentType()).append(LINE_FEED);
                            sb.append(LINE_FEED);
                            dataOutputStream.write(sb.toString().getBytes(charsetName));

                            while ((bytesRead = multipartFile.getInputStream().read(buffer)) != -1) {
                                dataOutputStream.write(buffer, 0, bytesRead);
                            }
                            multipartFile.getInputStream().close();
                            dataOutputStream.flush();
                        }
                    }
                }

                dataOutputStream.write(LINE_FEED.getBytes(charsetName));
                dataOutputStream.write(("--" + boundary + "--").getBytes(charsetName));
                dataOutputStream.write(LINE_FEED.getBytes(charsetName));
                dataOutputStream.flush();

                dataOutputStream.close();
            } else if (objBody instanceof String) {
                write(conn.getOutputStream(), (String) objBody);
            }
        }

        return (new HttpResponse(conn, charsetName));
    }

    public String getParameter(Map<String, String> map) {
        if (map != null) {
            StringBuilder sb = new StringBuilder();
            for (String key : map.keySet()) {
                try {
                    sb.append(URLEncoder.encode(key, charsetName)).append("=").append(URLEncoder.encode(map.get(key), charsetName)).append("&");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            if (sb.length() > 0) {
                sb.setLength(sb.length() - 1);
                return sb.toString();
            }
        }
        return null;
    }

    public boolean isUrlHttps(URL url) throws IOException {
        if ("https".equals(url.getProtocol())) {
            // It is https
            return true;
        } else if ("http".equals(url.getProtocol())) {
            // It is http
            return false;
        }
        throw new IOException("Unknown ProtocolType");
    }

    private void write(OutputStream out, String data) throws IOException {
        if (data == null) data = "";
        DataOutputStream dataOutputStream = new DataOutputStream(out);
        dataOutputStream.write(data.getBytes(charsetName));
        dataOutputStream.flush();
        dataOutputStream.close();
    }

    private void createSslSessionFactory() {
        if (sslSocketFactory == null) {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });
                HttpsURLConnection.setFollowRedirects(true);

                sslSocketFactory = sc.getSocketFactory();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static boolean isEmpty(String str) {
        return (str == null || str.length() == 0);
    }

    public HttpRequestHeader getJsonHeader() {
        return jsonHeader;
    }

    public HttpRequestHeader getTextHtmlHeader() {
        return textHtmlHeader;
    }
}
