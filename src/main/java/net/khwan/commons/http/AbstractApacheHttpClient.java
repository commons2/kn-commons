package net.khwan.commons.http;

import net.khwan.commons.http.model.*;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractApacheHttpClient {
    protected int connectTimeout;
    protected RequestConfig requestConfig;
    protected org.apache.http.client.HttpClient httpClient;
    protected String charsetName = "UTF-8";
    protected final HttpRequestHeader jsonHeader = new HttpRequestHeader();
    protected final HttpRequestHeader textHtmlHeader = new HttpRequestHeader();

    public AbstractApacheHttpClient() {
        this(3000);
    }

    public AbstractApacheHttpClient(int connectTimeout) {
        this(false, 20, 2, connectTimeout);
    }

    public AbstractApacheHttpClient(PoolingHttpClientConnectionManager manager) {
        this(3000, manager);
    }

    public AbstractApacheHttpClient(int connectTimeout, PoolingHttpClientConnectionManager manager) {
        httpClient = HttpClients.custom().setConnectionManager(manager).build();
        setConnectTimeout(connectTimeout);

        textHtmlHeader.put("Cache-Control", "no-cache");
        textHtmlHeader.put("Content-Type", "text/html");
        textHtmlHeader.put("Accept", "text/html");

        jsonHeader.put("Cache-Control", "no-cache");
        jsonHeader.put("Content-Type", "application/json");
        jsonHeader.put("Accept", "application/json");
    }

    public AbstractApacheHttpClient(boolean enableSSL, int maxTotal, int defaultMaxPerRoute) {
        this(enableSSL, maxTotal, defaultMaxPerRoute, 3000);
    }

    public AbstractApacheHttpClient(boolean enableSSL, int maxTotal, int defaultMaxPerRoute, int connectTimeout) {
        if (enableSSL) {
            // use the TrustSelfSignedStrategy to allow Self Signed Certificates
            SSLContext sslContext = null;

            try {
                sslContext = SSLContextBuilder
                        .create()
                        .loadTrustMaterial(new TrustSelfSignedStrategy())
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (sslContext != null) {
                // we can optionally disable hostname verification.
                // if you don't want to further weaken the security, you don't have to include this.
                HostnameVerifier allowAllHosts = new NoopHostnameVerifier();

                // create an SSL Socket Factory to use the SSLContext with the trust self signed certificate strategy
                // and allow all hosts verifier.
                SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, allowAllHosts);

                // finally create the HttpClient using HttpClient factory methods and assign the ssl socket factory
                Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
                        .<ConnectionSocketFactory>create()
                        .register("http", PlainConnectionSocketFactory.getSocketFactory())
                        .register("https", connectionFactory)
                        .build();

                PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
                manager.setMaxTotal(maxTotal);
                manager.setDefaultMaxPerRoute(defaultMaxPerRoute);


                httpClient = HttpClients.custom().setConnectionManager(manager).build();
            }
        } else {
            PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager();
            manager.setMaxTotal(maxTotal);
            manager.setDefaultMaxPerRoute(defaultMaxPerRoute);
            httpClient = HttpClients.custom().setConnectionManager(manager).build();
        }
        setConnectTimeout(connectTimeout);

        textHtmlHeader.put("Cache-Control", "no-cache");
        textHtmlHeader.put("Content-Type", "text/html");
        textHtmlHeader.put("Accept", "text/html");

        jsonHeader.put("Cache-Control", "no-cache");
        jsonHeader.put("Content-Type", "application/json");
        jsonHeader.put("Accept", "application/json");
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse get(String url) throws IOException {
        return get(url, jsonHeader);
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url      http 요청 url
     * @param urlParam http 요청 url 파라미터, ex) url?key=value
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse get(String url, HttpRequestUrlParam urlParam) throws IOException {
        return get(url, jsonHeader, urlParam);
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse get(String url, HttpRequestHeader header) throws IOException {
        return get(url, header, null);
    }

    /**
     * http(get) 요청 결과 반환
     *
     * @param url      http 요청 url
     * @param header   http 요청 헤더
     * @param urlParam http 요청 url 파라미터, ex) url?key=value
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse get(String url, HttpRequestHeader header, HttpRequestUrlParam urlParam) throws IOException {
        String strUrlParam = getParameter(urlParam);
        if (strUrlParam.length() > 0) {
            url = url + "?" + strUrlParam;
        }
        HttpGet get = new HttpGet(url);
        return request(get, header);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url) throws IOException {
        return post(url, (String) null);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpRequestBody body) throws IOException {
        return post(url, null, body);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        HttpPost post = new HttpPost(url);
        return request(post, null, body, multipartFiles);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpMultipartFile... multipartFiles) throws IOException {
        HttpPost post = new HttpPost(url);
        return request(post, null, null, multipartFiles);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, String body) throws IOException {
        return post(url, null, body);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpRequestHeader header) throws IOException {
        return post(url, header, (String) null);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        HttpPost post = new HttpPost(url);
        return request(post, header, body);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param header         http 요청 헤더
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpRequestHeader header, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        HttpPost post = new HttpPost(url);
        return request(post, header, body, multipartFiles);
    }


    /**
     * http(post) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param header         http 요청 헤더
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpRequestHeader header, HttpMultipartFile... multipartFiles) throws IOException {
        HttpPost post = new HttpPost(url);
        return request(post, header, null, multipartFiles);
    }

    /**
     * http(post) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse post(String url, HttpRequestHeader header, String body) throws IOException {
        HttpPost post = new HttpPost(url);
        return request(post, header, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url) throws IOException {
        return put(url, (String) null);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url, HttpRequestBody body) throws IOException {
        return put(url, null, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        HttpPut put = new HttpPut(url);
        return request(put, null, body, multipartFiles);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url, String body) throws IOException {
        return put(url, null, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url, HttpRequestHeader header) throws IOException {
        return put(url, header, (String) null);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        HttpPut put = new HttpPut(url);
        return request(put, header, body);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url            http 요청 url
     * @param header         http 요청 헤더
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url, HttpRequestHeader header, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        HttpPut put = new HttpPut(url);
        return request(put, header, body, multipartFiles);
    }

    /**
     * http(put) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse put(String url, HttpRequestHeader header, String body) throws IOException {
        HttpPut put = new HttpPut(url);
        return request(put, header, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url http 요청 url
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse delete(String url) throws IOException {
        return delete(url, (String) null);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse delete(String url, HttpRequestBody body) throws IOException {
        return delete(url, null, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url  http 요청 url
     * @param body http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse delete(String url, String body) throws IOException {
        return delete(url, null, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse delete(String url, HttpRequestHeader header) throws IOException {
        return delete(url, header, (String) null);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse delete(String url, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        HttpDeleteWithBody delete = new HttpDeleteWithBody(url);
        return request(delete, header, body);
    }

    /**
     * http(delete) 요청 결과 반환
     *
     * @param url    http 요청 url
     * @param header http 요청 헤더
     * @param body   http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    public ApacheHttpResponse delete(String url, HttpRequestHeader header, String body) throws IOException {
        HttpDeleteWithBody delete = new HttpDeleteWithBody(url);
        return request(delete, header, body);
    }

    /**
     * http(post, put) 요청 결과 반환
     *
     * @param requestBase http 요청 객체
     * @param header      http 요청 헤더
     * @param body        http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    private ApacheHttpResponse request(HttpEntityEnclosingRequestBase requestBase, HttpRequestHeader header, String body) throws IOException {
        StringEntity stringEntity = null;
        if (body != null) {
            stringEntity = new StringEntity(body, Charset.forName(charsetName));
        }
        return request(requestBase, header, stringEntity);
    }

    /**
     * http(post, put) 요청 결과 반환
     *
     * @param requestBase http 요청 객체
     * @param header      http 요청 헤더
     * @param body        http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    private ApacheHttpResponse request(HttpEntityEnclosingRequestBase requestBase, HttpRequestHeader header, HttpRequestBody body) throws IOException {
        UrlEncodedFormEntity urlEncodedFormEntity = null;
        if (body != null) {
            List<NameValuePair> urlParameters = new ArrayList<>();
            for (String key : body.keySet()) {
                String value = body.get(key);
                urlParameters.add(new BasicNameValuePair(key, value));
            }
            urlEncodedFormEntity = new UrlEncodedFormEntity(urlParameters, charsetName);
        }
        return request(requestBase, header, urlEncodedFormEntity);
    }

    /**
     * http(post, put) 요청 결과 반환
     *
     * @param requestBase    http 요청 객체
     * @param header         http 요청 헤더
     * @param body           http 요청 body
     * @param multipartFiles http 업로드 파일
     * @return http 요청 결과
     * @throws IOException Exception
     */
    private ApacheHttpResponse request(HttpEntityEnclosingRequestBase requestBase, HttpRequestHeader header, HttpRequestBody body, HttpMultipartFile... multipartFiles) throws IOException {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create().setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        if (body != null) {
            for (String key : body.keySet()) {
                builder.addTextBody(key, body.get(key), ContentType.MULTIPART_FORM_DATA.withCharset(charsetName));
            }
        }

        if (multipartFiles != null) {
            for (HttpMultipartFile multipartFile : multipartFiles) {
                if (multipartFile != null) {
                    if (multipartFile.getFile() != null) {
                        builder.addBinaryBody(multipartFile.getFieldName(), multipartFile.getFile(), ContentType.DEFAULT_BINARY, URLEncoder.encode(multipartFile.getFileName(), charsetName));
                    } else {
                        builder.addBinaryBody(multipartFile.getFieldName(), multipartFile.getInputStream(), ContentType.DEFAULT_BINARY, URLEncoder.encode(multipartFile.getFileName(), charsetName));
                    }
                }
            }
        }

        requestBase.setConfig(requestConfig);

        if (body != null || multipartFiles != null) {
            requestBase.setEntity(builder.build());
        }

        return request(requestBase, header);
    }

    /**
     * http(post, put) 요청 결과 반환
     *
     * @param requestBase http 요청 객체
     * @param header      http 요청 헤더
     * @param body        http 요청 body
     * @return http 요청 결과
     * @throws IOException Exception
     */
    private ApacheHttpResponse request(HttpEntityEnclosingRequestBase requestBase, HttpRequestHeader header, StringEntity body) throws IOException {
        requestBase.setConfig(requestConfig);

        if (body != null) {
            requestBase.setEntity(body);
        }

        return request(requestBase, header);
    }

    /**
     * http(get, delete) 요청 결과 반환
     *
     * @param requestBase http 요청 객체
     * @param header      http 요청 헤더
     * @return http 요청 결과
     * @throws IOException Exception
     */
    private ApacheHttpResponse request(HttpRequestBase requestBase, HttpRequestHeader header) throws IOException {
        requestBase.setConfig(requestConfig);
        if (header != null) {
            for (String key : header.keySet()) {
                String value = header.get(key);
                requestBase.setHeader(key, value);
            }
        }
        HttpResponse response = httpClient.execute(requestBase);
        return (new ApacheHttpResponse(response, charsetName));
    }


    public String getParameter(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        if (map != null) {
            for (String key : map.keySet()) {
                try {
                    sb.append(URLEncoder.encode(key, charsetName)).append("=").append(URLEncoder.encode(map.get(key), charsetName)).append("&");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            if (sb.length() > 0) {
                sb.setLength(sb.length() - 1);
            }
        }
        return sb.toString();
    }


    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        this.requestConfig = RequestConfig.custom()
                .setCookieSpec(CookieSpecs.STANDARD)
                .setConnectTimeout(connectTimeout)
                .setConnectionRequestTimeout(connectTimeout)
                .setSocketTimeout(connectTimeout)
                .setCookieSpec("easy")
                .build();
    }

    public RequestConfig getRequestConfig() {
        return requestConfig;
    }

    public void setRequestConfig(RequestConfig requestConfig) {
        this.requestConfig = requestConfig;
    }

    public String getCharsetName() {
        return charsetName;
    }

    public void setCharsetName(String charsetName) {
        this.charsetName = charsetName;
    }

    public HttpRequestHeader getJsonHeader() {
        return jsonHeader;
    }

    public HttpRequestHeader getTextHtmlHeader() {
        return textHtmlHeader;
    }
}
