package net.khwan.commons.kakao.payload;

import com.fasterxml.jackson.annotation.JsonSetter;
import net.khwan.commons.jackson.model.JsonIgnoreField;

import java.util.List;

/**
 * @author 김경환
 * @version 1.0
 * @since 2019-12-17
 */
public class KakaoAddress extends JsonIgnoreField {
    private List<Document> documents;

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public static class Document extends JsonIgnoreField {
        private Address address;
        private RoadAddress roadAddress;
        private String addressName;
        private String addressType;
        private Double latitude;
        private Double longitude;

        public Address getAddress() {
            return address;
        }

        @JsonSetter("address")
        public void setAddress(Address address) {
            this.address = address;
        }

        public RoadAddress getRoadAddress() {
            return roadAddress;
        }

        @JsonSetter("road_address")
        public void setRoadAddress(RoadAddress roadAddress) {
            this.roadAddress = roadAddress;
        }

        public String getAddressName() {
            return addressName;
        }

        @JsonSetter("address_name")
        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }

        public String getAddressType() {
            return addressType;
        }

        @JsonSetter("address_type")
        public void setAddressType(String addressType) {
            this.addressType = addressType;
        }

        public Double getLatitude() {
            return latitude;
        }

        @JsonSetter("y")
        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        @JsonSetter("x")
        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }
    }

    public static class Address extends JsonIgnoreField {
        private String addressName;
        private String bcode;
        private String hcode;
        private String mainAddressNo;
        private String mountainYn;
        private String region1DepthName;
        private String region2DepthName;
        private String region3DepthHName;
        private String region3DepthName;
        private String subAddressNo;
        private String latitude;
        private String longitude;
        private String zipCode;

        public String getAddressName() {
            return addressName;
        }

        @JsonSetter("address_name")
        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }

        public String getBcode() {
            return bcode;
        }

        @JsonSetter("b_code")
        public void setBcode(String bCode) {
            this.bcode = bCode;
        }

        public String getHcode() {
            return hcode;
        }

        @JsonSetter("h_code")
        public void setHcode(String hcode) {
            this.hcode = hcode;
        }

        public String getMainAddressNo() {
            return mainAddressNo;
        }

        @JsonSetter("main_address_no")
        public void setMainAddressNo(String mainAddressNo) {
            this.mainAddressNo = mainAddressNo;
        }

        public String getMountainYn() {
            return mountainYn;
        }

        @JsonSetter("mountain_yn")
        public void setMountainYn(String mountainYn) {
            this.mountainYn = mountainYn;
        }

        public String getRegion1DepthName() {
            return region1DepthName;
        }

        @JsonSetter("region_1depth_name")
        public void setRegion1DepthName(String region1DepthName) {
            this.region1DepthName = region1DepthName;
        }

        public String getRegion2DepthName() {
            return region2DepthName;
        }

        @JsonSetter("region_2depth_name")
        public void setRegion2DepthName(String region2DepthName) {
            this.region2DepthName = region2DepthName;
        }

        public String getRegion3DepthHName() {
            return region3DepthHName;
        }

        @JsonSetter("region_3depth_h_name")
        public void setRegion3DepthHName(String region3DepthHName) {
            this.region3DepthHName = region3DepthHName;
        }

        public String getRegion3DepthName() {
            return region3DepthName;
        }

        @JsonSetter("region_3depth_name")
        public void setRegion3DepthName(String region3DepthName) {
            this.region3DepthName = region3DepthName;
        }

        public String getSubAddressNo() {
            return subAddressNo;
        }

        @JsonSetter("sub_address_no")
        public void setSubAddressNo(String subAddressNo) {
            this.subAddressNo = subAddressNo;
        }

        public String getLatitude() {
            return latitude;
        }

        @JsonSetter("y")
        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        @JsonSetter("x")
        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getZipCode() {
            return zipCode;
        }

        @JsonSetter("zip_code")
        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }
    }

    public static class RoadAddress extends JsonIgnoreField {
        private String addressName;
        private String buildingName;
        private String mainBuildingNo;
        private String region1DepthName;
        private String region2DepthName;
        private String region3DepthName;
        private String roadName;
        private String subBuildingNo;
        private String undergrounYn;
        private String undergroundYn;
        private String latitude;
        private String longitude;
        private String zoneNo;

        public String getAddressName() {
            return addressName;
        }

        @JsonSetter("address_name")
        public void setAddressName(String addressName) {
            this.addressName = addressName;
        }

        public String getBuildingName() {
            return buildingName;
        }

        @JsonSetter("building_name")
        public void setBuildingName(String buildingName) {
            this.buildingName = buildingName;
        }

        public String getMainBuildingNo() {
            return mainBuildingNo;
        }

        @JsonSetter("main_building_no")
        public void setMainBuildingNo(String mainBuildingNo) {
            this.mainBuildingNo = mainBuildingNo;
        }

        public String getRegion1DepthName() {
            return region1DepthName;
        }

        @JsonSetter("region_1depth_name")
        public void setRegion1DepthName(String region1DepthName) {
            this.region1DepthName = region1DepthName;
        }

        public String getRegion2DepthName() {
            return region2DepthName;
        }

        @JsonSetter("region_2depth_name")
        public void setRegion2DepthName(String region2DepthName) {
            this.region2DepthName = region2DepthName;
        }

        public String getRegion3DepthName() {
            return region3DepthName;
        }

        @JsonSetter("region_3depth_name")
        public void setRegion3DepthName(String region3DepthName) {
            this.region3DepthName = region3DepthName;
        }

        public String getRoadName() {
            return roadName;
        }

        @JsonSetter("road_name")
        public void setRoadName(String roadName) {
            this.roadName = roadName;
        }

        public String getSubBuildingNo() {
            return subBuildingNo;
        }

        @JsonSetter("sub_building_no")
        public void setSubBuildingNo(String subBuildingNo) {
            this.subBuildingNo = subBuildingNo;
        }

        public String getUndergrounYn() {
            return undergrounYn;
        }

        @JsonSetter("undergroun_yn")
        public void setUndergrounYn(String undergrounYn) {
            this.undergrounYn = undergrounYn;
        }

        public String getUndergroundYn() {
            return undergroundYn;
        }

        @JsonSetter("underground_yn")
        public void setUndergroundYn(String undergroundYn) {
            this.undergroundYn = undergroundYn;
        }

        public String getLatitude() {
            return latitude;
        }

        @JsonSetter("y")
        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        @JsonSetter("x")
        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getZoneNo() {
            return zoneNo;
        }

        @JsonSetter("zone_no")
        public void setZoneNo(String zoneNo) {
            this.zoneNo = zoneNo;
        }
    }
}
