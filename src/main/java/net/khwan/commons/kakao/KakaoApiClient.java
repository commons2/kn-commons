package net.khwan.commons.kakao;

import net.khwan.commons.http.AbstractApacheHttpClient;
import net.khwan.commons.http.model.ApacheHttpResponse;
import net.khwan.commons.http.model.HttpRequestHeader;
import net.khwan.commons.http.model.HttpRequestUrlParam;
import net.khwan.commons.jackson.Jackson;
import net.khwan.commons.kakao.payload.KakaoAddress;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ConnectException;

/**
 * @author 김경환
 * @version 1.0
 * @since 2019-12-17
 */
public class KakaoApiClient extends AbstractApacheHttpClient {
    private static final Logger logger = LoggerFactory.getLogger(KakaoApiClient.class);

    private final String url;

    private final Jackson jackson;
    private final String restApiKey;

    public KakaoApiClient(final String restApiKey) {
        this("https://dapi.kakao.com/v2/local/search/address.json", restApiKey, new Jackson(), true, 10, 10);
    }

    public KakaoApiClient(final String url, final String restApiKey) {
        this(url, restApiKey, new Jackson(), true, 10, 10);
    }

    public KakaoApiClient(final String restApiKey, final Jackson jackson) {
        this("https://dapi.kakao.com/v2/local/search/address.json", restApiKey, jackson, true, 10, 10);
    }

    public KakaoApiClient(final String restApiKey, final Jackson jackson, final boolean enableSSL, int maxTotal, int defaultMaxPerRoute) {
        this("https://dapi.kakao.com/v2/local/search/address.json", restApiKey, jackson, enableSSL, maxTotal, defaultMaxPerRoute);
    }

    public KakaoApiClient(final String url, final String restApiKey, final Jackson jackson, final boolean enableSSL, int maxTotal, int defaultMaxPerRoute) {
        super(enableSSL, maxTotal, defaultMaxPerRoute);
        this.url = url;
        if (restApiKey.contains("KakaoAK")) {
            this.restApiKey = restApiKey;
        } else {
            this.restApiKey = "KakaoAK " + restApiKey;
        }
        this.jackson = jackson;
    }

    public KakaoAddress searchAddress(String searchAddress) {
        try {
            HttpRequestUrlParam param = new HttpRequestUrlParam();
            param.put("query", searchAddress);

            logger.info("url   : {}, {}", url, searchAddress);
            HttpRequestHeader header = getJsonHeader();
            header.put("Authorization", restApiKey);
            ApacheHttpResponse response = get(url, header, param);
            if (response.getResponseCode() == HttpStatus.SC_OK) {
                String content = response.getContent();
                logger.debug("response content : {}", content);
                if (content != null && content.length() > 0) {
                    return jackson.fromJson(content, KakaoAddress.class);
                }
            } else {
                logger.debug("ResponseCode    : {}", response.getResponseCode());
                logger.debug("ResponseContent : {}", response.getContent());
            }
        } catch (Exception e) {
            if (e instanceof ConnectException) {
                logger.warn("Connection refused, {}", url);
            } else {
                logger.error("Exception", e);
            }
        }
        return null;
    }

    public static class Address {
        private final String address;
        private final double latitude;
        private final double longitude;

        public Address(String address, double latitude, double longitude) {
            this.address = address;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public String getAddress() {
            return address;
        }

        public double getLatitude() {
            return latitude;
        }

        public double getLongitude() {
            return longitude;
        }
    }
}
