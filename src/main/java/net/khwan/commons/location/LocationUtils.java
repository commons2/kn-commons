package net.khwan.commons.location;

import net.khwan.commons.model.Coordinate;
import net.khwan.commons.model.MinMaxCoordinate;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * @author 김경환
 * @version 1.0
 * @since 2020-10-16
 */
public class LocationUtils {
    private LocationUtils() {
    }

    // 지구 평균 반지름
    public final static int EARTH_MEAN_RADIUS = 6371009;


    // 반경 m 이내의 위도 차이(degree)
    public static MinMaxCoordinate minMaxCoordinate(double lat, double lng, double distance) {
        double diffLat = latInDifference(distance);
        double diffLng = lngInDifference(lat, distance);
        return (new MinMaxCoordinate((new Coordinate(lat - diffLat, lng - diffLng)), (new Coordinate(lat + diffLat, lng + diffLng))));
    }

    // 반경 m 이내의 위도 차이(degree)
    public static double latInDifference(double distance) {
        return (distance * 360.0) / (2 * Math.PI * EARTH_MEAN_RADIUS);
    }

    // 반경 m 이내의 경도 차이(degree)
    public static double lngInDifference(double lat, double distance) {
        return (distance * 360.0) / (2 * Math.PI * EARTH_MEAN_RADIUS * Math.cos(Math.toRadians(lat)));
    }

    // 시작 위경도, 종료 위경도로 방위각 구하기
    public static double bearing(double startLat, double startLng, double endLat, double endLng) {
        double lat1R = Math.toRadians(startLat);
        double lat2R = Math.toRadians(endLat);
        double dLngR = Math.toRadians((endLng - startLng));
        double a = Math.sin(dLngR) * Math.cos(lat2R);
        double b = Math.cos(lat1R) * Math.sin(lat2R) - Math.sin(lat1R) * Math.cos(lat2R)
                * Math.cos(dLngR);
        double bearing = Math.toDegrees(Math.atan2(a, b));
        bearing = bearing % 360;
        if (bearing < 0) bearing += 360;
        return bearing;
    }

    // 위, 경도, 방위각, 거리(M) 로 좌표 구하기
    public static Coordinate travel(double lat, double lng, double bearing, double distance) {
        double bR = Math.toRadians(bearing);
        double lat1R = Math.toRadians(lat);
        double lng1R = Math.toRadians(lng);
        double dR = distance / 6371009;    // 지구 타원체 평균 값

        double a = Math.sin(dR) * Math.cos(lat1R);
        double lat2 = Math.asin(Math.sin(lat1R) * Math.cos(dR) + a * Math.cos(bR));
        double lng2 = lng1R
                + Math.atan2(Math.sin(bR) * a, Math.cos(dR) - Math.sin(lat1R) * Math.sin(lat2));
        return (new Coordinate(Math.toDegrees(lat2), Math.toDegrees(lng2)));
    }

    // 거리 미터로 구하기
    public static double distance(double startLat, double startLng, double endLat, double endLng) {
        double lat1R = Math.toRadians(startLat);
        double lat2R = Math.toRadians(endLat);
        double dLatR = Math.abs(lat2R - lat1R);
        double dLngR = Math.toRadians(Math.abs((endLng - startLng)));
        double a = Math.sin(dLatR / 2) * Math.sin(dLatR / 2) + Math.cos(lat1R) * Math.cos(lat2R)
                * Math.sin(dLngR / 2) * Math.sin(dLngR / 2);
        return 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)) * 6371009;    // 지구 타원체 평균 값
    }

    public static double speed(double startLat, double startLng, double endLat, double endLng, Date startDt, Date endDt) {
        long elapsedTimeMillis = (startDt.getTime() - endDt.getTime());
        return speed(startLat, startLng, endLat, endLng, elapsedTimeMillis);
    }

    public static double speed(double startLat, double startLng, double endLat, double endLng, LocalDateTime startDt, LocalDateTime endDt) {
        long elapsedTimeMillis = ChronoUnit.MILLIS.between(startDt, endDt);
        return speed(startLat, startLng, endLat, endLng, elapsedTimeMillis);
    }

    public static double speed(double startLat, double startLng, double endLat, double endLng, long elapsedTimeMillis) {
        double distance = distance(startLat, startLng, endLat, endLng);
        return speed(distance, elapsedTimeMillis);
    }

    // 속도 km/h 반환
    public static double speed(double distance, long elapsedTimeMillis) {
        long elapsedTimeSeconds = elapsedTimeMillis / 1000;
        if (distance > 0 && elapsedTimeMillis > 0) {
            // m/s -> km/h 변환
            double speed = distance / elapsedTimeSeconds * 3.6;
            if (speed > 0) {
                return doubleValue(speed);
            }
        }
        return 0;
    }

    private static double doubleValue(double value) {
        value = (long) (value * 100);
        return value / 100;
    }
}
