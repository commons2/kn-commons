package net.khwan.commons.date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;

public class LocalDateUtils {

    public static HashMap<String, DateTimeFormatter> dateTimeFormatterMap = new HashMap<>();

    /**
     * LocalDateTime 객체를 yyyy-MM-dd HH:mm:ss 형태의 문자열로 변환합니다.
     *
     * @param dateFormat 날짜 포맷
     * @return 변환된 문자열
     */
    public static String stringFromLocalDateTime(String dateFormat) {
        LocalDateTime localDateTime = LocalDateTime.now();
        return stringFromLocalDateTime(localDateTime, dateFormat);
    }

    public static String stringFromLocalDateTime(DateUtils.DateFormat dateFormat) {
        return stringFromLocalDateTime(dateFormat.toString());
    }

    /**
     * LocalDateTime 객체를 yyyy-MM-dd HH:mm:ss 형태의 문자열로 변환합니다.
     *
     * @param localDateTime LocalDateTime 객체
     * @param dateFormat    날짜 포맷
     * @return 변환된 문자열
     */
    public static String stringFromLocalDateTime(LocalDateTime localDateTime, String dateFormat) {
        DateTimeFormatter formatter = dateTimeFormatterMap.get(dateFormat);
        if (formatter == null) {
            formatter = DateTimeFormatter.ofPattern(dateFormat);
            dateTimeFormatterMap.put(dateFormat, formatter);
        }
        return localDateTime.format(formatter);
    }

    public static String stringFromLocalDateTime(LocalDateTime localDateTime, DateUtils.DateFormat dateFormat) {
        return stringFromLocalDateTime(localDateTime, dateFormat.toString());
    }

    /**
     * 입력된 형태의 문자열을 캘린더 객체로 변환합니다.
     * 만약 변환에 실패할 경우 오늘 날짜를 반환합니다.
     *
     * @param date       날짜를 나타내는 문자열
     * @param dateFormat 날짜 포맷
     * @return 변환된 캘린더 객체
     */
    public static LocalDateTime localDateTimeFromString(String date, String dateFormat) {
        DateTimeFormatter formatter = dateTimeFormatterMap.get(dateFormat);
        if (formatter == null) {
            formatter = DateTimeFormatter.ofPattern(dateFormat);
            dateTimeFormatterMap.put(dateFormat, formatter);
        }
        return LocalDateTime.parse(date, formatter);
    }

    /**
     * 입력된 형태의 문자열을 캘린더 객체로 변환합니다.
     * 만약 변환에 실패할 경우 오늘 날짜를 반환합니다.
     *
     * @param date       날짜를 나타내는 문자열
     * @param dateFormat 날짜 포맷
     * @return 변환된 캘린더 객체
     */
    public static LocalDate localDateFromString(String date, String dateFormat) {
        DateTimeFormatter formatter = dateTimeFormatterMap.get(dateFormat);
        if (formatter == null) {
            formatter = DateTimeFormatter.ofPattern(dateFormat);
            dateTimeFormatterMap.put(dateFormat, formatter);
        }
        return LocalDate.parse(date, formatter);
    }

    /**
     * 입력된 형태의 문자열을 캘린더 객체로 변환합니다.
     * 만약 변환에 실패할 경우 오늘 날짜를 반환합니다.
     *
     * @param date       날짜를 나타내는 문자열
     * @param dateFormat 날짜 포맷 Enum
     * @return 변환된 캘린더 객체
     */
    public static LocalDateTime localDateTimeFromString(String date, DateUtils.DateFormat dateFormat) {
        return localDateTimeFromString(date, dateFormat.toString());
    }

    public static LocalDate localDateFromString(String date, DateUtils.DateFormat dateFormat) {
        return localDateFromString(date, dateFormat.toString());
    }

    public static LocalDateTime localDateTimeFromDate(Date date) {
        return localDateTimeFromDate(date, ZoneId.systemDefault());
    }

    public static LocalDateTime localDateTimeFromDate(Date date, ZoneId zoneId) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static Date dateFromLocalDateTime(LocalDateTime localDateTime) {
        return dateFromLocalDateTime(localDateTime, ZoneId.systemDefault());
    }

    public static Date dateFromLocalDateTime(LocalDateTime localDateTime, ZoneId zoneId) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
