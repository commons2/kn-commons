package net.khwan.commons.date;

import net.khwan.commons.protocol.TagDateTime;
import net.khwan.commons.protocol.TagTime;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by kkh on 2016-01-22.
 * 날짜 관련
 */
public final class DateUtils {
    private DateUtils() {
    }

    public static final String DATE_FORMAT_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ssX";
    public static final String DATE_FORMAT_DEFAULT_TIME = "HH:mm:ss";
    public static final String DATE_FORMAT_DEFULAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_DEFAULT_SLASH = "yyyy/MM/dd HH:mm:ss";
    public static final String DATE_FORMAT_DEFULAT_MILLISEC1 = "yyyy-MM-dd HH:mm:ss,SSS";
    public static final String DATE_FORMAT_DEFULAT_MILLISEC2 = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String DATE_FORMAT_SIMPLE = "yyyy-MM-dd";
    public static final String DATE_FORMAT_yyyyMMdd = "yyyyMMdd";
    public static final String DATE_FORMAT_yyyyMMddHH = "yyyyMMddHH";
    public static final String DATE_FORMAT_yyyyMMddHHmm = "yyyyMMddHHmm";
    public static final String DATE_FORMAT_yyyyMMddHHmmss = "yyyyMMddHHmmss";
    public static final String DATE_FORMAT_yyyyMMddHHmmssSSS = "yyyyMMddHHmmssSSS";
    public static final String DATE_FORMAT_yyyyMMdd_HHmmss = "yyyyMMdd_HHmmss";
    public static final String DATE_FORMAT_HHmmss = "HHmmss";
    public static final String DATE_FORMAT_HHmmssSSS = "HHmmssSSS";

    public enum DateFormat {
        TIMEZONE {
            @Override
            public String toString() {
                return DATE_FORMAT_TIMEZONE;
            }
        },
        DEFULAT {
            @Override
            public String toString() {
                return DATE_FORMAT_DEFULAT;
            }
        },
        DEFAULT_SLASH {
            @Override
            public String toString() {
                return DATE_FORMAT_DEFAULT_SLASH;
            }
        },
        DEFULAT_MILLISEC1 {
            @Override
            public String toString() {
                return DATE_FORMAT_DEFULAT_MILLISEC1;
            }
        },
        DEFULAT_MILLISEC2 {
            @Override
            public String toString() {
                return DATE_FORMAT_DEFULAT_MILLISEC2;
            }
        },
        SIMPLE {
            @Override
            public String toString() {
                return DATE_FORMAT_SIMPLE;
            }
        },
        yyyyMMdd_HHmmss {
            @Override
            public String toString() {
                return DATE_FORMAT_yyyyMMdd_HHmmss;
            }
        },
        yyyyMMddHHmmssSSS {
            @Override
            public String toString() {
                return DATE_FORMAT_yyyyMMddHHmmssSSS;
            }
        },
        yyyyMMddHHmmss {
            @Override
            public String toString() {
                return DATE_FORMAT_yyyyMMddHHmmss;
            }
        },
        yyyyMMddHHmm {
            @Override
            public String toString() {
                return DATE_FORMAT_yyyyMMddHHmm;
            }
        },
        yyyyMMddHH {
            @Override
            public String toString() {
                return DATE_FORMAT_yyyyMMddHH;
            }
        },
        yyyyMMdd {
            @Override
            public String toString() {
                return DATE_FORMAT_yyyyMMdd;
            }
        },
        HHmmss {
            @Override
            public String toString() {
                return DATE_FORMAT_HHmmss;
            }
        },
        HHmmssSSS {
            @Override
            public String toString() {
                return DATE_FORMAT_HHmmssSSS;
            }
        },
        DEFAULT_TIME {
            @Override
            public String toString() {
                return DATE_FORMAT_DEFAULT_TIME;
            }
        }
    }

    /**
     * 캘린더 객체를 yyyy-MM-dd HH:mm:ss 형태의 문자열로 변환합니다.
     *
     * @param dateFormat 날짜 포맷
     * @return 변환된 문자열
     */
    public static String stringFromCalendar(String dateFormat) {
        Calendar cal = Calendar.getInstance();
        // 날짜를 통신용 문자열로 변경
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(cal.getTime());
    }

    public static String stringFromCalendar(DateFormat dateFormat) {
        return stringFromCalendar(dateFormat.toString());
    }

    /**
     * 캘린더 객체를 yyyy-MM-dd HH:mm:ss 형태의 문자열로 변환합니다.
     *
     * @param cal        캘린더 객체
     * @param dateFormat 날짜 포맷
     * @return 변환된 문자열
     */
    public static String stringFromCalendar(Calendar cal, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(cal.getTime());
    }

    public static String stringFromCalendar(Calendar cal, DateFormat dateFormat) {
        return stringFromCalendar(cal, dateFormat.toString());
    }

    /**
     * Date 객체를 yyyy-MM-dd HH:mm:ss 형태의 문자열로 변환합니다.
     *
     * @param date       Date 객체
     * @param dateFormat 날짜 포맷
     * @return 변환된 문자열
     */
    public static String stringFromDate(Date date, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(date.getTime());
    }

    public static String stringFromDate(Date date, DateFormat dateFormat) {
        return stringFromDate(date, dateFormat.toString());
    }

    /**
     * 입력된 형태의 문자열을 캘린더 객체로 변환합니다.
     * 만약 변환에 실패할 경우 오늘 날짜를 반환합니다.
     *
     * @param date       날짜를 나타내는 문자열
     * @param dateFormat 날짜 포맷
     * @return 변환된 캘린더 객체
     * @throws ParseException ParseException
     */
    public static Calendar calendarFromString(String date, String dateFormat) throws ParseException {
        Calendar cal = Calendar.getInstance();

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        cal.setTime(formatter.parse(date));

        return cal;
    }

    /**
     * 입력된 형태의 문자열을 캘린더 객체로 변환합니다.
     * 만약 변환에 실패할 경우 오늘 날짜를 반환합니다.
     *
     * @param date       날짜를 나타내는 문자열
     * @param dateFormat 날짜 포맷 Enum
     * @return 변환된 캘린더 객체
     */
    public static Calendar calendarFromString(String date, DateFormat dateFormat) {
        try {
            return calendarFromString(date, dateFormat.toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 입력된 형태의 문자열을 Date 객체로 변환합니다.
     * 만약 변환에 실패할 경우 오늘 날짜를 반환합니다.
     *
     * @param date       날짜를 나타내는 문자열
     * @param dateFormat 날짜 포맷 문자열
     * @return Date 변환된 캘린더 객체
     * @throws ParseException ParseException
     */
    public static Date dateFromString(String date, String dateFormat) throws ParseException {
        Calendar cal = calendarFromString(date, dateFormat);
        if (cal != null) {
            return cal.getTime();
        }
        return null;
    }

    /**
     * 입력된 형태의 문자열을 Date 객체로 변환합니다.
     * 만약 변환에 실패할 경우 오늘 날짜를 반환합니다.
     *
     * @param date       날짜를 나타내는 문자열
     * @param dateFormat 날짜 포맷 Enum
     * @return Date 변환된 캘린더 객체
     */
    public static Date dateFromString(String date, DateFormat dateFormat) {
        Calendar cal = calendarFromString(date, dateFormat);
        if (cal != null) {
            return cal.getTime();
        }
        return null;
    }

    /**
     * Date 객체를 Calendar 객체로 변환하여 반환
     *
     * @param date date 객체
     * @return 변환된 Calendar 객체 반환
     */
    public static Calendar calendarFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    /**
     * Calendar 객체에서 현재 년도를 반환
     *
     * @return 현재 년도를 Integer 형 으로 반환
     */
    public static int getYearInt() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    public static int getYearInt(Calendar cal) {
        if (cal == null) cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    /**
     * Calendar 객체에서 현재 년도를 반환
     *
     * @return 현재 년도를 String 형 으로 반환
     */
    public static String getYearStr() {
        Calendar cal = Calendar.getInstance();
        return Integer.toString(cal.get(Calendar.YEAR));
    }

    /**
     * Calendar 객체에서 현재 월을 반환
     *
     * @return 현재 월를 Integer 형 으로 반환
     */
    public static int getMonthInt() {
        Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.MONTH) + 1);
    }

    public static int getMonthInt(Calendar cal) {
        if (cal == null) cal = Calendar.getInstance();
        return (cal.get(Calendar.MONTH) + 1);
    }

    /**
     * Calendar 객체에서 현재 월을 반환
     *
     * @return 현재 월를 String 형 으로 반환
     */
    public static String getMonthStr() {
        Calendar cal = Calendar.getInstance();
        return Integer.toString((cal.get(Calendar.MONTH) + 1));
    }

    /**
     * Calendar 객체에서 현재 일자를 반환
     *
     * @return 현재 일자를 Integer 형 으로 반환
     */
    public static int getDayInt() {
        Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.DAY_OF_MONTH));
    }

    public static int getDayInt(Calendar cal) {
        if (cal == null) cal = Calendar.getInstance();
        return (cal.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Calendar 객체에서 현재 일자를 반환
     *
     * @return 현재 일자를 String 형 으로 반환
     */
    public static String getDayStr() {
        Calendar cal = Calendar.getInstance();
        return Integer.toString(cal.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * Calendar 객체에서 현재 시간을 반환
     *
     * @return 현재 시간을 Integer 형 으로 반환
     */
    public static int getHourInt() {
        Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.HOUR_OF_DAY));
    }

    public static int getHourInt(Calendar cal) {
        if (cal == null) cal = Calendar.getInstance();
        return (cal.get(Calendar.HOUR_OF_DAY));
    }

    /**
     * Calendar 객체에서 현재 시간을 반환
     *
     * @return 현재 시간을 String 형 으로 반환
     */
    public static String getHourStr() {
        Calendar cal = Calendar.getInstance();
        return Integer.toString(cal.get(Calendar.HOUR_OF_DAY));
    }

    /**
     * Calendar 객체에서 현재 분을 반환
     *
     * @return 현재 분을 Integer 형 으로 반환
     */
    public static int getMinuteInt() {
        Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.MINUTE));
    }

    public static int getMinuteInt(Calendar cal) {
        if (cal == null) cal = Calendar.getInstance();
        return (cal.get(Calendar.MINUTE));
    }

    /**
     * Calendar 객체에서 현재 분을 반환
     *
     * @return 현재 분을 String 형 으로 반환
     */
    public static String getMinuteStr() {
        Calendar cal = Calendar.getInstance();
        return Integer.toString(cal.get(Calendar.MINUTE));
    }

    /**
     * Calendar 객체에서 현재 초를 반환
     *
     * @return 현재 초를 Integer 형 으로 반환
     */
    public static int getSecondInt() {
        Calendar cal = Calendar.getInstance();
        return (cal.get(Calendar.SECOND));
    }

    public static int getSecondInt(Calendar cal) {
        if (cal == null) cal = Calendar.getInstance();
        return (cal.get(Calendar.SECOND));
    }

    /**
     * Calendar 객체에서 현재 초를 반환
     *
     * @return 현재 초를 String 형 으로 반환
     */
    public static String getSecondStr() {
        Calendar cal = Calendar.getInstance();
        return Integer.toString(cal.get(Calendar.SECOND));
    }

    /**
     * Calendar 객체를 byte 배열 Little Endian 으로 반환
     *
     * @return byte[]
     */
    public static byte[] getByteArrayLittleEndianFromCalendar() {
        return getByteArrayFromCalendar(Calendar.getInstance(), ByteOrder.LITTLE_ENDIAN);
    }

    /**
     * Calendar 객체를 byte 배열 Little Endian 으로 반환
     *
     * @param cal Calendar 객체
     * @return byte[]
     */
    public static byte[] getByteArrayLittleEndianFromCalendar(Calendar cal) {
        return getByteArrayFromCalendar(cal, ByteOrder.LITTLE_ENDIAN);
    }

    /**
     * Calendar 객체를 byte 배열 Big Endian 으로 반환
     *
     * @return byte[]
     */
    public static byte[] getByteArrayBigEndianFromCalendar() {
        return getByteArrayFromCalendar(Calendar.getInstance(), ByteOrder.BIG_ENDIAN);
    }

    /**
     * Calendar 객체를 byte 배열 Big Endian 으로 반환
     *
     * @param cal Calendar 객체
     * @return byte[]
     */
    public static byte[] getByteArrayBigEndianFromCalendar(Calendar cal) {
        return getByteArrayFromCalendar(cal, ByteOrder.BIG_ENDIAN);
    }

    /**
     * Calendar 객체를 byte 배열로 반환
     *
     * @param cal       Calendar 객체
     * @param byteOrder byte 배열 ByteOrder(BIG_ENDIAN, LITTLE_ENDIAN)
     * @return byte[]
     */
    public static byte[] getByteArrayFromCalendar(Calendar cal, ByteOrder byteOrder) {
        ByteBuffer buffer = ByteBuffer.allocate(7);

        buffer.order(byteOrder);
        buffer.putShort((short) cal.get(Calendar.YEAR));
        buffer.put((byte) (cal.get(Calendar.MONTH) + 1));
        buffer.put((byte) (cal.get(Calendar.DAY_OF_MONTH)));
        buffer.put((byte) (cal.get(Calendar.HOUR_OF_DAY)));
        buffer.put((byte) (cal.get(Calendar.MINUTE)));
        buffer.put((byte) (cal.get(Calendar.SECOND)));

        return buffer.array();
    }

    /**
     * 시작시간과 종료시간의 차이를 일로 반환한다.
     *
     * @param startTimeMillis 시작 시간
     * @param endTimeMillis   종료 시간
     * @return 시작시간과 종료시간의 차이를 일로 반환
     */
    public static long getElapsedTimeInDay(long startTimeMillis, long endTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, endTimeMillis) / 86400000;
    }

    public static long getElapsedTimeInDay(long startTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, 0) / 86400000;
    }

    /**
     * 시작시간과 종료시간의 차이를 시간으로 반환한다.
     *
     * @param startTimeMillis 시작 시간
     * @param endTimeMillis   종료 시간
     * @return 시작시간과 종료시간의 차이를 시간으로 반환
     */
    public static long getElapsedTimeInHour(long startTimeMillis, long endTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, endTimeMillis) / 3600000;
    }

    public static long getElapsedTimeInHour(long startTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, 0) / 3600000;
    }

    /**
     * 시작시간과 종료시간의 차이를 분으로 반환한다.
     *
     * @param startTimeMillis 시작 시간
     * @param endTimeMillis   종료 시간
     * @return 시작시간과 종료시간의 차이를 분으로 반환
     */
    public static long getElapsedTimeInMinute(long startTimeMillis, long endTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, endTimeMillis) / 60000;
    }

    public static long getElapsedTimeInMinute(long startTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, 0) / 60000;
    }

    /**
     * 시작시간과 종료시간의 차이를 초로 반환한다.
     *
     * @param startTimeMillis 시작 시간
     * @param endTimeMillis   종료 시간
     * @return 시작시간과 종료시간의 차이를 초로 반환
     */
    public static long getElapsedTimeInSeconds(long startTimeMillis, long endTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, endTimeMillis) / 1000;
    }

    public static long getElapsedTimeInSeconds(long startTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, 0) / 1000;
    }

    /**
     * 시작시간과 종료시간의 차이를 밀리초로 반환한다.
     *
     * @param startTimeMillis 시작 시간
     * @param endTimeMillis   종료 시간
     * @return 시작시간과 종료시간의 차이를 밀리초로 반환
     */
    public static long getElapsedTimeInMillis(long startTimeMillis, long endTimeMillis) {
        if (endTimeMillis <= 0) endTimeMillis = System.currentTimeMillis();

        long elapsedTimeMillis = endTimeMillis - startTimeMillis;
        return elapsedTimeMillis;
    }

    public static long getElapsedTimeInMillis(long startTimeMillis) {
        return getElapsedTimeInMillis(startTimeMillis, 0);
    }

    /**
     * 시작시간과 종료시간의 차이를 스트링으로 반환한다.
     *
     * @param startTimeMillis 시작 시간
     * @param endTimeMillis   종료 시간
     * @param locale          반환 언어 (KOREA, US)
     * @return 시작시간과 종료시간의 차이를 문자열로 반환 (ex. 1년 1개월 1시간)
     */
    public static String getElapsedTimeInString(long startTimeMillis, long endTimeMillis, Locale locale) {
        TagDateTime dateTime = getElapsedTimeInTagDateTime(startTimeMillis, endTimeMillis);

        StringBuilder sb = new StringBuilder();
        boolean isAppend = false;
        String str;

        boolean korea = false;
        if (locale.getLanguage().equalsIgnoreCase(Locale.KOREA.getLanguage())) {
            korea = true;
        }
        if (korea) str = "년";
        else str = "years";
        isAppend = append(sb, isAppend, dateTime.getYear(), str);

        if (korea) str = "개월";
        else str = "months";
        isAppend = append(sb, isAppend, dateTime.getMonth(), str);

        if (korea) str = "일";
        else str = "days";
        isAppend = append(sb, isAppend, dateTime.getDay(), str);

        if (korea) str = "시간";
        else str = "hours";
        isAppend = append(sb, isAppend, dateTime.getHour(), str);

        if (korea) str = "분";
        else str = "minutes";
        isAppend = append(sb, isAppend, dateTime.getMinute(), str);

        if (korea) str = "초";
        else str = "seconds";
        append(sb, isAppend, dateTime.getSecond(), str);

        return sb.toString();
    }

    public static String getElapsedTimeInString(long startTimeMillis, long endTimeMillis) {
        return getElapsedTimeInString(startTimeMillis, endTimeMillis, Locale.KOREA);
    }

    public static TagDateTime getElapsedTimeInTagDateTime(long startTimeMillis, long endTimeMillis) {
        long elapsedTimeSeconds = getElapsedTimeInSeconds(startTimeMillis, endTimeMillis);

        long year = (elapsedTimeSeconds / 31536000);
        elapsedTimeSeconds = elapsedTimeSeconds - (year * 31536000);
        long month = (elapsedTimeSeconds / 2592000);
        elapsedTimeSeconds = elapsedTimeSeconds - (month * 2592000);
        long day = (elapsedTimeSeconds / 86400);
        elapsedTimeSeconds = elapsedTimeSeconds - (day * 86400);
        long hour = (elapsedTimeSeconds / 3600);
        elapsedTimeSeconds = elapsedTimeSeconds - (hour * 3600);
        long minute = (elapsedTimeSeconds / 60);
        elapsedTimeSeconds = elapsedTimeSeconds - (minute * 60);
        long second = elapsedTimeSeconds;

        TagDateTime dateTime = new TagDateTime();
        if (year > 0) {
            dateTime.setYear((short) year);
        }
        if (month > 0) {
            dateTime.setMonth((byte) month);
        }
        if (day > 0) {
            dateTime.setDay((byte) day);
        }
        if (hour > 0) {
            dateTime.setHour((byte) hour);
        }
        if (minute > 0) {
            dateTime.setMinute((byte) minute);
        }
        if (second > 0) {
            dateTime.setSecond((byte) second);
        }

        return dateTime;
    }

    private static boolean append(StringBuilder sb, boolean isAppend, int intValue, String strValue) {
        if (intValue > 0) {
            if (isAppend) sb.append(" ");
            else isAppend = true;

            sb.append(intValue);
            sb.append(strValue);
        }

        return isAppend;
    }

    /**
     * 시작시간과 종료시간의 차이를 밀리초로 반환한다.
     *
     * @param startNanoTime 시작 시간
     * @param endNanoTime   종료 시간
     * @return 시작시간과 종료시간의 차이를 밀리초로 반환
     */
    public static long getElapsedNanoTimeInMillis(long startNanoTime, long endNanoTime) {
        if (endNanoTime <= 0) endNanoTime = System.nanoTime();

        long elapsedNanoTime = endNanoTime - startNanoTime;
        return elapsedNanoTime / 1000000;
    }

    public static long getElapsedNanoTimeInMillis(long startTimeMillis) {
        return getElapsedNanoTimeInMillis(startTimeMillis, 0);
    }

    /**
     * 초를 TagTime 객체로 반환
     *
     * @param seconds 초
     * @return 초를 TagTime 객체로 반환
     */
    public static TagTime TagTimeFromSecond(int seconds) {
        int MIN = 60;
        int HOUR = 60 * 60;
        int DAY = HOUR * 24;

        TagTime tagTime = null;

        if (seconds < 0) {
            tagTime = new TagTime(0xff, 0xff, 0xff);
        } else {
            int day = seconds / DAY;
            seconds = seconds - (DAY * day);
            int hour = seconds / HOUR;
            seconds = seconds - (HOUR * hour);
            int min = seconds / MIN;
            seconds = seconds - (MIN * min);
            int sec = seconds;
            if (day >= 1) {
                tagTime = new TagTime(24, 0, 0);
            } else {
                tagTime = new TagTime(hour, min, sec);
            }
        }

        return tagTime;
    }
}
