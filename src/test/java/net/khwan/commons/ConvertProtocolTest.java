//package net.khwan.commons;
//
//import net.khwan.commons.date.DateUtils;
//import net.khwan.commons.protocol.converter.ConvertProtocol;
//import net.khwan.commons.protocol.converter.DefaultConvertProtocol;
//import net.khwan.commons.protocol.model.ReceiveResult;
//import net.khwan.commons.utils.HexBin;
//import org.junit.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.nio.ByteOrder;
//import java.util.Queue;
//
///**
// * @author 김경환
// * @version 1.0
// * @since 2019-06-25
// */
//public class ConvertProtocolTest {
//    public static final Logger logger = LoggerFactory.getLogger(ConvertProtocolTest.class);
//
//    @Test
//    public void modbusResponseTest() throws Exception {
//        String[] packets = new String[]{
//                "01046E9D5A9D5B9D5C9D5D9D5E9D5F9D609D619D629D639D649D659D669D679D689D69001F00290033003D00000000000000000024002E00009D699D5A9D620047001F5F67D7B7CC590C8D0037004156CF000008AF00000BB900000BBA00000FA100000FA20000000B000B012D000000019AB1"
//                , "0103AE0BB90BB90BB90BB9001F001F000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BB9912C"
//                , "010214000000000000000000000000000000000000000073AB"
//                , "02046E9D5B9D5C9D5D9D5E9D5F9D609D619D629D639D649D659D669D679D689D699D6A0020002A0034003E00000000000000000025002F00009D6A9D5B9D63004800205F68D7B8CC5A0C8E0051005C56D0000008B000000BBA00000BBB00000FA200000FA30000000C000C012E00000001F4C9"
//                , "0203AE0BBA0BBA0BBA0BBA00200020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BBAC53C"
//                , "03046E9D5C9D5D9D5E9D5F9D609D619D629D639D649D659D669D679D689D699D6A9D6B0021002B0035003F00000000000000000026003000009D6B9D5C9D64004900215F69D7B9CC5B0C8F0013001456D1000008B100000BBB00000BBC00000FA300000FA40000000D000D012F000000015F79"
//                , "0303AE0BBB0BBB0BBB0BBB00210021000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000BBB2086"
//        };
//        ConvertProtocol convertProtocol = new DefaultConvertProtocol(2048);
//        convertProtocol.setDataLengthPosition(2, ConvertProtocol.DataLengthSize.UNSIGNED_BYTE);
//        convertProtocol.setReceiveTimeoutMillis(0);
//        convertProtocol.setDataValid(ConvertProtocol.DataValid.MODBUS_CRC16);
//        convertProtocol.setByteOrder(ByteOrder.LITTLE_ENDIAN);
//
//        long testStartTime = System.nanoTime();
//        for (int i = 0; i < 10000000; i++) {
//            for (String packet : packets) {
//                byte[] buffer = HexBin.decode(packet);
//                for (int j = 0; j < buffer.length; j++) {
//                    long startTime = System.nanoTime();
//                    Queue<ReceiveResult> resultQueue = convertProtocol.receivePacket(new byte[]{buffer[j]}, 1);
//                    while (!resultQueue.isEmpty()) {
//                        ReceiveResult receiveResult = resultQueue.remove();
//                        long elapsedTimeMillis = DateUtils.getElapsedNanoTimeInMillis(startTime);
//                        if (elapsedTimeMillis >= 50) {
//                            throw new Exception("Timeout (" + elapsedTimeMillis + ")");
//                        }
//                        if (!receiveResult.isResult()) {
//                            logger.warn("{}", receiveResult.toString());
//                            logger.warn("packet    : {}", HexBin.encode(receiveResult.getReceiveBuffer()));
//                        }
//                    }
//                }
//            }
//        }
//        logger.info("elapsedTimeMillis : {}", DateUtils.getElapsedNanoTimeInMillis(testStartTime));
//    }
//}
