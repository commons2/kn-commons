#!/bin/bash
EXEC_PATH=$(pwd)
SCRIPT_SRC=$(readlink -f "$0" || echo "$0")
SCRIPT_PATH=$(dirname "${SCRIPT_SRC}" || echo .)
JAVA_BIN=java
GRADLE_PATH=
###########################################
# args
############################################
RELEASE=$1
############################################

do_gradle() {
  # gradle run
  GRADLE_PARAM=
  if [ "$RELEASE" == "release" ]; then
    GRADLE_PARAM=-Prelease=true
  fi

  GRADLE_RESULT=0
  ./gradlew publish $GRADLE_PARAM || GRADLE_RESULT=$?
  if [ $GRADLE_RESULT == 0 ]; then
    ./gradlew clean
  else
    exit 1
  fi
}

# JAVA 설치 디렉토리 찾기
find_jdks() {
  for java_version in 8; do
    for jvmdir in /usr/lib/jvm/java-${java_version}-openjdk-* \
      /usr/lib/jvm/jdk-${java_version}-oracle-* \
      /usr/lib/jvm/jre-${java_version}-oracle-* \
      /usr/lib/jvm/java-${java_version}-oracle \
      /usr/lib/jvm/oracle-java${java_version}-jdk-* \
      /usr/lib/jvm/oracle-java${java_version}-jre-* \
      /usr/lib/jvm/zulu-jdk-${java_version} \
      /usr/lib/jvm/zulu-jdk-${java_version}; do
      if [ -d "${jvmdir}" ]; then
        JDK_DIRS="${JDK_DIRS} ${jvmdir}"
      fi
    done
  done
}

find_gradlew() {
  CUR_PATH=$(pwd)
  #echo "find gradle path : ${CUR_PATH}"

  if [ ! -e gradlew ]; then
    if [ ! $CUR_PATH == "/" ]; then
      cd ..
      find_gradlew
    fi
  else
    GRADLE_PATH=$CUR_PATH
    cd $EXEC_PATH
  fi
}

# The first existing directory is used for JAVA_HOME
JDK_DIRS=""
find_jdks

# Look for the right JVM to use
for jdir in $JDK_DIRS; do
  if [ -r "$jdir/bin/java" -a -z "${JAVA_HOME}" ]; then
    JAVA_HOME="$jdir"
    JAVA_BIN="$jdir/bin/java"
  fi
done

echo "java binary path \"$JAVA_BIN\""

find_gradlew

if [ $GRADLE_PATH == "/" ]; then
  echo "not found gradlew"
  exit 1
fi

export JAVA_HOME=$JAVA_HOME

# gradle path 로 이동 후 빌드 처리
cd $GRADLE_PATH

chmod 777 gradlew

do_gradle

cd $EXEC_PATH
